# Ruey
Ruey is a declarative GUI written completely in Rust, using
[Rusty object notation (RON)](https://github.com/ron-rs/ron) to declare the GUI
layout (basically another flavour of JSON, TOML, XML, etc). The core, and all
elements are written as a platform-agnostic library which only outputs draw
lists to the user. These draw lists can then be rendered using any graphics
API supporting indexed vertex buffers (or equivalent).

See
[example page on the wiki](https://gitlab.com/DavidLyhedDanielsson/ruey/wikis/examples)
for some code examples and videos.
## Target audience
Currently the target audience is for engines or other types of "non-menu"
GUIs. For instance, a main menu or options menu is currently not the target
for Ruey, though it should be possible to implement richer styling and
animation support. One inspiration for this library is
[Dear imgui](https://github.com/ocornut/imgui), and the target audience is
currently the same.

# Version 1.0
In version 1.0 you can expect...

## Docking and tabs
Out-of-the-box support for dockable and/or tabbed windows.

## A number of useful widgets
- Single or multi-line text boxes - with or without the ability to edit them
- Dropdown menus
- Sliders - vertical, horizontal, and with configurable step sizes
- Buttons - normal clickable ones, checkboxes, and radio buttons
- Color pickers - at least RGB and either HSB/HSV or HSL
- Menu bars

## Configurable styling and layout without recompiling
Both style and layout are controlled through external files, so there is
never a need to recompile your code for GUI changes.
**All elements are implemented in rust**, but the *layout* of them is defined in
an external file.

A goal for 1.0 or shortly thereafter is a diff-based reload, meaning only the
affected windows/widgets will be reloaded when the file has been modified.

### Why not statically typed/compiled?
Recompiling a sizable code base, rerunning the program, and then navigating
back to whichever GUI has been modified can take long enough that it becomes
bothersome to develop a GUI. Perhaps a button might look better a few pixels
off to the left, or perhaps the order of some buttons feels off; reloading
the GUI from an external file allows experimentation with instant feedback.

## Easy integration
There will be no need to tie your entire code base to this specific GUI
system. Providers allow you to put Ruey on top of your application and reuse
your data types and collections.
## Moddability by users
Theoretically it would be possible without much work to create an API to let
your users create their own GUIs using your engine. Providers allow the
creation of a sort of back-end API, and the external layout file lets users
modify them at will.
# Before 1.0
You can expect rough corners, unpolished features, bugs, and breaking
changes. But also the possibility to influence the library.

## Roadmap
A sort of roadmap can be found in the
[milestones](https://gitlab.com/DavidLyhedDanielsson/ruey/-/milestones/1)
section here on GitLab. Issues connected to the milestones will be added and
removed at any time, but the goals for the milestones should be largely the
same.

# Core principles
The core principles of Ruey are:
* Real-time capable - The library should always be real-time capable for a
  "complex but reasonable" UI.
* Fast design iteration - Designing a GUI is a very iterative process, and as
  such the library should work with the user and make iteration simple.
* Non-invasive - Implementing Ruey in an existing codebase should not require
  excessive modification or coupling of existing code.
* Dynamic - Dynamic data sources means a dynamic GUI is needed to visualize it.
* Stable and helpful - Invalid inputs to the library should not crash or corrupt
  it. If an error occurs a helpful error message is given.
* Extendable - GUI elements are self-contained and layouts work with the Element
  trait, making it easy to add or modify elements.

# How it works
The library and all elements are implemented in Rust, but uses
[RON](https://github.com/ron-rs/ron) as a declaration format. It can be
likened with HTML; you declare the layout of the GUI and hand over the file
to Ruey.

The glue between Rust and RON is what is called "providers", they source data
from Rust and provide it to the GUI. Each widget is coded to expect certain
types of data (strings, i32, etc), and the provider must provide that type.
Providers basically enable referencing (dynamic) Rust variables from the
(static) GUI file, and letting the GUI "front-end" and Rust "back-end"
communicate with each other.
## Parsing and building
The GUI is, at any point at runtime, created from a given RON file. The file
is parsed/deserialized and elements are created and stored internally, so
there is no need to hold onto a handle except for the GUI instance itself.

The GUI system is not yet usable though - it needs to be built first.
Building the system goes through all elements and positions them, making them
ready for rendering.

### Elements
An element is a widget or a layout.

### Widgets
A widget is something the user sees: a button, text field, slider etc.

### Layouts
A layout controls placement of gui elements (widgets or layouts). It is
possible to nest these to create more complext GUIs.

## Styling
Styling is done through the a RON file, and this file can be reloaded without
having to rebuild or reload the entire GUI.

# Controls
Left mouse works as usual (clicking, dragging, resizing). Use right click to
activate "docking mode", which will allow docking of windows to other windows
by clicking and dragging them. It is possible to undock windows by right
clicking the title bar and dragging the window away.

It is currently only possible to resize windows by draggin the bottom-right
corner. It is possible to resize docked windows by grabbing the border with
the left mouse button.

# Examples
## Code
Currently the repo contains an SDL/GL implementation, found in src/main.rs.
It will be moved somewhere else in the future, but it is good enough for now.
## Online
See [example page on the
wiki](https://gitlab.com/DavidLyhedDanielsson/ruey/wikis/examples) for some
examples with videos
# Documentation
The documentation is very lack-luster right now because I have been focusing
on making Ruey functional.

Check out the
[wiki]((https://gitlab.com/DavidLyhedDanielsson/ruey/wikis/examples)) for
documentation topics.
# Contribution
Any help is welcome. Pull requests, testing, suggestions, doubts. [Go to the
contribution page on the
wiki](https://gitlab.com/DavidLyhedDanielsson/ruey/wikis/contribution) to see
how to contribute.

This is my first rust project, and the code probably reflects that (and my
C/C++ background).
# License
## Ruey
Ruey is licensed under the MIT license. I would love to know about it if you
start using it though!
## CamingoCode
The font used in the project is CamingoCode, its license can be found at
https://www.fontsquirrel.com/license/camingocode
# Contact
I can be contacted either at my gmail, which is the same as my user name on
GitLab, as a single word, or on Reddit, where my username is Lyhed