#![feature(raw)]
extern crate cgmath;
extern crate ron;
extern crate serde;
#[macro_use]
extern crate log;
#[cfg(test)]
extern crate approx;
extern crate bitflags;
extern crate lazy_static;
extern crate rusttype;
#[cfg(test)]
extern crate uuid;

#[cfg(test)]
mod testutil;
pub mod color;
pub mod context;
pub mod drawlist;
pub mod element;
pub mod guierror;
pub mod guikey;
pub mod hsvcolor;
pub mod parsing;
pub mod rect;
pub mod style;
pub mod vector2;
pub mod vertex;
pub mod windowmanager;

mod font;

use context::Context;
use drawlist::*;
use element::Element;
use font::Font;
use guierror::{GuiError, ParseResult};
use guikey::GuiKey;
use parsing::parser::Parser;
use rect::Rect;
use ron::Value;
use std::collections::HashSet;
use std::fs::File;
use std::vec::Vec;
use windowmanager::WindowManager;

pub struct GuiVariables<'a> {
    font: &'a Font,
    mouse_x: i32,
    mouse_y: i32,
    draw_list: &'a mut DrawList,
    overlay_draw_list: &'a mut DrawList,
    context: &'a mut Context,
}

pub struct Gui {
    font: Font,
    area: Rect,
    keys_down: HashSet<GuiKey>,
    focused_element: Option<*mut dyn Element>,
    window_manager: WindowManager,
}

impl Gui {
    pub fn new(font_path: &std::path::Path) -> Result<Gui, GuiError> {
        Ok(Gui {
            font: Font::new(font_path)?,
            area: Rect::new_zero(),
            keys_down: HashSet::<GuiKey>::new(),
            focused_element: None,
            window_manager: Default::default(),
        })
    }

    pub fn load_file(&mut self, context: &mut Context, path: &std::path::Path) -> Result<ParseResult, GuiError> {
        let file = File::open(path)?;
        let value: Value = ron::de::from_reader(file)?;
        self.window_manager = Default::default(); // Must be recreated or cleared to avoid dangling pointers
        let mut parser = Parser::new(&mut self.window_manager);
        Ok(parser.parse_root(context, &value))
    }

    pub fn build(&mut self, area: Rect, context: &mut Context) {
        self.area = area;
        self.window_manager.build(area, context, &mut self.font);
    }

    pub fn rebuild(&mut self, context: &mut Context) {
        self.window_manager.rebuild(context, &mut self.font);
    }

    pub fn set_area(&mut self, area: Rect, context: &mut Context) {
        self.area = area;
        self.window_manager.rebuild(context, &mut self.font);
    }

    pub fn draw(&mut self, mouse_x: i32, mouse_y: i32, context: &mut Context) -> (DrawList, DrawList) {
        context.new_frame();

        let mut draw_list = DrawList::new();
        let mut overlay_draw_list = DrawList::new();
        draw_list.push_clip_rect(self.area);
        overlay_draw_list.push_clip_rect(self.area);
        {
            let mut gui_variables = GuiVariables {
                font: &self.font,
                mouse_x,
                mouse_y,
                draw_list: &mut draw_list,
                overlay_draw_list: &mut overlay_draw_list,
                context,
            };
            self.window_manager.draw(&mut gui_variables);
            self.focused_element = gui_variables.context.get_focused_element();
        }
        if !draw_list.commands.is_empty() {
            {
                let last = draw_list.commands.last_mut().unwrap();
                last.index_count = draw_list.indices.len() as u32 - last.index_offset;
            }
        }
        if !overlay_draw_list.commands.is_empty() {
            {
                let last = overlay_draw_list.commands.last_mut().unwrap();
                last.index_count = overlay_draw_list.indices.len() as u32 - last.index_offset;
            }
        }
        (draw_list, overlay_draw_list)
    }

    pub fn get_font_texture(&self) -> &Vec<u8> {
        &self.font.get_texture_data()
    }

    pub fn key_down(&mut self, context: &mut Context, key: GuiKey) {
        if !self.keys_down.contains(&key) {
            if let Some(focused) = self.focused_element {
                debug!("focused key_down {:?}", key);
                unsafe {
                    (*focused).key_down(context, key);
                }
            } else {
                debug!("key_down {:?}", key);
                self.window_manager.key_down(context, key)
            }
            self.keys_down.insert(key);
        } else if let Some(focused) = self.focused_element {
            debug!("focused key_repeat {:?}", key);
            unsafe {
                (*focused).key_repeat(context, key);
            }
        } else {
            debug!("key_repeat {:?}", key);
            self.window_manager.key_repeat(context, key)
        }
    }

    pub fn key_up(&mut self, context: &mut Context, key: GuiKey) {
        if self.keys_down.contains(&key) {
            self.keys_down.remove(&key);
            if let Some(focused) = self.focused_element {
                debug!("focused key_up {:?}", key);
                unsafe {
                    (*focused).key_up(context, key);
                }
            } else {
                debug!("key_up {:?}", key);
                self.window_manager.key_up(context, key);
            }
        }
    }

    pub fn text_input(&mut self, context: &mut Context, text: &str) {
        if let Some(focused) = self.focused_element {
            debug!("focused text_input {:?}", text);
            unsafe {
                (*focused).text_input(context, text);
            }
        } else {
            debug!("text_input {:?}", text);
            self.window_manager.text_input(context, text);
        }
    }

    pub fn mouse_down(&mut self, context: &mut Context, pos_x: i32, pos_y: i32, button: i32) {
        if let Some(focused) = self.focused_element {
            debug!("focused mouse_down {:?} at {:?};{:?}", button, pos_x, pos_y);
            unsafe {
                (*focused).mouse_down(context, &self.font, pos_x, pos_y, button);

                if context.get_focused_element().is_none() {
                    debug!("event released mouse_down {:?} at {:?};{:?}", button, pos_x, pos_y);
                    self.window_manager.mouse_down(context, &self.font, pos_x, pos_y, button);
                }
            }
        } else {
            debug!("mouse_down {:?} at {:?};{:?}", button, pos_x, pos_y);
            self.window_manager.mouse_down(context, &self.font, pos_x, pos_y, button);
        }
    }

    pub fn mouse_up(&mut self, context: &mut Context, pos_x: i32, pos_y: i32, button: i32) {
        if let Some(focused) = self.focused_element {
            debug!("focused mouse_up {:?} at {:?};{:?}", button, pos_x, pos_y);
            unsafe {
                (*focused).mouse_up(context, &self.font, pos_x, pos_y, button);
            }
        } else {
            debug!("mouse_up {:?} at {:?};{:?}", button, pos_x, pos_y);
            self.window_manager.mouse_up(context, &self.font, pos_x, pos_y, button);
        }
    }
}
