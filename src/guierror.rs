use std::convert::From;
use std::fmt;

use ron::Value;
use rusttype;

use element::Element;
use parsing::validationerror::ValidationError;

enum LastMessage {
    Error,
    Warning,
}

fn simple_property_to_string(value: &Value, indentation: usize) -> String {
    match value {
        Value::Bool(val) => val.to_string(),
        Value::Char(val) => val.to_string(),
        Value::Number(val) => val.get().to_string(),
        Value::String(val) => val.to_string(),
        Value::Seq(val) => format!("[Array with {} members]", val.len()),
        Value::Map(map) => {
            let mut val = "{\n".to_string();
            for pair in map {
                let name = simple_property_to_string(pair.0, indentation + 1);
                val = format!("{}{}{}: ...\n", val, "    ".repeat(indentation + 1), name);
            }
            val = format!("{}{}}}", val, "    ".repeat(indentation));
            val
        }
        _ => "<unknown type>".to_string(),
    }
}

fn value_to_string(value: &Value) -> String {
    let mut return_string = String::new();
    if let Value::Map(map) = value {
        for pair in map.iter() {
            let name = simple_property_to_string(pair.0, 1);
            let value = simple_property_to_string(pair.1, 1);
            return_string = format!("{} {}: {}\n", return_string, name, value);
        }
    }
    if !return_string.is_empty() {
        format!("(\n{})", return_string)
    } else {
        return_string
    }
}

pub struct ParseResult {
    pub errors: Vec<(String, Vec<ValidationError>)>,
    pub warnings: Vec<(String, Vec<ValidationError>)>,
    pub element: Option<Box<dyn Element>>,
}

impl std::fmt::Debug for ParseResult {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self.errors)?;
        write!(f, "{:?}", self.warnings)
    }
}

impl std::fmt::Display for ParseResult {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for (value_string, errors) in &self.errors {
            writeln!(f, "Error while parsing {}:", value_string)?;
            for error in errors {
                writeln!(f, "{}", error)?;
            }
        }
        for (value_string, warnings) in &self.warnings {
            writeln!(f, "Warning while parsing {}:", value_string)?;
            for warning in warnings {
                writeln!(f, "{}", warning)?;
            }
        }
        Ok(())
    }
}

#[derive(Default)]
pub struct ParseResultBuilder {
    value: String,
    errors: Vec<ValidationError>,
    warnings: Vec<ValidationError>,
    inherited_errors: Vec<(String, Vec<ValidationError>)>,
    inherited_warnings: Vec<(String, Vec<ValidationError>)>,
    element: Option<Box<dyn Element>>,
    last_message: Option<LastMessage>,
}

#[derive(Default, Debug)]
pub struct MessageBuilder {
    pub message: String,
}

impl MessageBuilder {
    pub fn new() -> MessageBuilder {
        MessageBuilder {
            message: Default::default(),
        }
    }

    pub fn with_message(mut self, message: &str) -> MessageBuilder{
        self.message = message.to_owned();
        self
    }

    pub fn with_message_expected_to_contain(mut self, field: &str) -> MessageBuilder{
        self.message = format!("Expected field \"{}\"", field);
        self
    }

    pub fn with_message_expected_to_contain_with_type(mut self, field: &str, field_type: &str) -> MessageBuilder {
        self.message = format!("Expected field \"{}\" of type {}", field, field_type);
        self
    }

    pub fn with_message_expected_field_to_contain(mut self, field: &str, expected_content: &str) -> MessageBuilder {
        self.message = format!("Expected field \"{}\" to contain {}", field, expected_content);
        self
    }

    pub fn with_message_expected_field_to_be_type(mut self, field: &str, field_type: &str) -> MessageBuilder {
        self.message = format!("Expected field \"{}\" to be of type {}", field, field_type);
        self
    }
}

impl ParseResultBuilder {
    pub fn new(value: &Value) -> ParseResultBuilder {
        ParseResultBuilder {
            value: value_to_string(value),
            errors: Default::default(),
            warnings: Default::default(),
            inherited_errors: Default::default(),
            inherited_warnings: Default::default(),
            element: Default::default(),
            last_message: None,
        }
    }

    pub fn build(self) -> ParseResult {
        let mut errors = self.inherited_errors;
        if !self.errors.is_empty() {
            errors.push((self.value.clone(), self.errors));
        }
        let mut warnings = self.inherited_warnings;
        if !self.warnings.is_empty() {
            warnings.push((self.value.clone(), self.warnings));
        }
        ParseResult {
            errors,
            warnings,
            element: self.element,
        }
    }

    pub fn consume_build(&mut self) -> ParseResult {
        let mut value_string = String::new();
        std::mem::swap(&mut value_string, &mut self.value);

        let mut temp_errors: Vec<ValidationError> = Vec::new();
        std::mem::swap(&mut temp_errors, &mut self.errors);
        let mut temp_warnings: Vec<ValidationError> = Vec::new();
        std::mem::swap(&mut temp_warnings, &mut self.warnings);

        let mut errors: Vec<(String, Vec<ValidationError>)> = Vec::new();
        errors.push((value_string.clone(), temp_errors));
        errors.append(&mut self.inherited_errors);

        let mut warnings: Vec<(String, Vec<ValidationError>)> = Vec::new();
        warnings.push((value_string, temp_warnings));
        errors.append(&mut self.inherited_warnings);

        ParseResult {
            errors,
            warnings,
            element: self.element.take(),
        }
    }

    pub fn with_messages_from(&mut self, mut parse_response: ParseResult) -> Option<Box<dyn Element>> {
        self.inherited_errors.append(&mut parse_response.errors);
        self.inherited_warnings.append(&mut parse_response.warnings);
        parse_response.element
    }

    pub fn with_errors(&mut self, mut error: Vec<ValidationError>) -> &mut Self {
        self.errors.append(&mut error);
        self
    }

    pub fn with_error(&mut self, error: ValidationError) -> &mut Self {
        self.last_message = Some(LastMessage::Error);
        self.errors.push(error);
        self
    }

    pub fn with_warning(&mut self, warning: ValidationError) -> &mut Self {
        self.last_message = Some(LastMessage::Warning);
        self.warnings.push(warning);
        self
    }

    pub fn promote_and_build(mut self) -> ParseResult {
        if let Some(LastMessage::Warning) = self.last_message {
            let last_warning = self.warnings.pop().unwrap();
            self.errors.push(last_warning);
        }
        self.build()
    }

    pub fn with_element<T: 'static + Element>(mut self, element: T) -> ParseResultBuilder{
        self.element = Some(Box::new(element));
        self
    }
    pub fn with_boxed_element(&mut self, element: Box<dyn Element>) {
        self.element = Some(element);
    }
}

#[derive(Debug)]
pub enum GuiError {
    LoadError(String),
    ParseError(String),
    BuildError(String),
    ExpectedError(String),
}

impl fmt::Display for GuiError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            GuiError::LoadError(text) => return write!(f, "{}", text),
            GuiError::ParseError(text) => return write!(f, "{}", text),
            GuiError::BuildError(text) => return write!(f, "{}", text),
            GuiError::ExpectedError(text) => return write!(f, "{}", text),
        }
    }
}

impl From<std::io::Error> for GuiError {
    fn from(err: std::io::Error) -> Self {
        GuiError::LoadError(format!("{}", err))
    }
}

impl From<ron::de::Error> for GuiError {
    fn from(err: ron::de::Error) -> Self {
        GuiError::LoadError(format!("{}", err))
    }
}

impl From<rusttype::Error> for GuiError {
    fn from(err: rusttype::Error) -> Self {
        GuiError::LoadError(format!("{}", err))
    }
}

impl GuiError {
    pub fn new_parse_error(value: &Value, message: &str) -> GuiError {
        GuiError::BuildError(format!("{} while parsing {:?}", message, value))
    }
    pub fn new_build_error(value: &Value, message: &str) -> GuiError {
        GuiError::BuildError(format!("{} while building {:?}", message, value))
    }
    pub fn new_expected_error(value: &Value, data_type: &str, member: &str) -> GuiError {
        GuiError::BuildError(format!(
            "expected field \"{}\" of type {} to exist in {:?}",
            member, data_type, value
        ))
    }
    pub fn new_expected_index_to_be_error(value: &Value, data_type: &str, index: usize) -> GuiError {
        GuiError::BuildError(format!(
            "expected index {} to be of type {} in {:?}",
            index, data_type, value
        ))
    }
    pub fn new_expected_to_contain(value: &Value, field: &str) -> GuiError {
        GuiError::BuildError(format!("expected {:?} to contain field \"{}\"", value, field))
    }
    pub fn new_expected_index_to_contain(value: &Value, data_type: &str, index: &str, description: &str) -> GuiError {
        GuiError::BuildError(format!(
            "expected index {} to be of type {} and contain {} in {:?}",
            index, data_type, description, value
        ))
    }
}
