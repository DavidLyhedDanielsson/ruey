use bitflags::*;
use element::*;
use parsing::provider::Provider;
use parsing::alignment::*;
use std::convert::From;

use font::{Font, HorizontalOrigin, VerticalOrigin};

bitflags! {
    struct InputMode: u32 {
        const ALL = 0b1111_1111;
        const ALPHA = 0b000_00001;
        const NUMERIC = 0b0000_0010;
        const HEX = 0b0000_0100;
        const CUSTOM = 0b0000_1000;
    }
}

#[derive(Clone, Copy)]
enum Command {
    LEFT,
    RIGHT,
    ENTER,
    BACKSPACE,
    CHARACTER(char),
    NONE,
}

const MAX_COMMANDS: usize = 16;

#[derive(Clone)]
pub struct InputText {
    base: ElementBase,
    vertical_align: VerticalAlignment,
    horizontal_align: HorizontalAlignment,
    provider: Provider,
    editing: bool,
    index: usize,
    click_position: Option<usize>,
    commands: [Command; MAX_COMMANDS as usize],
    command_count: usize,
    input_mode: InputMode,
}

fn validate_alignment(value: &Value) -> Result<(), ValidationError> {
    match value.get_alignment("align") {
        Ok(_) => Ok(()),
        Err(err) => if let ValidationError::NotFound(_) = err {
            Ok(())
        } else {
            Err(err)
        }
    }
}

fn validate_input_mode(value: &Value) -> Result<(), ValidationError> {
    match value.get_str("input_mode") {
        Ok(input_mode_str) => {
            for split in input_mode_str.split('|') {
                match split {
                    "alpha" | "numeric" | "hex" | "all" => { /*Do nothing*/ }
                    _ => return Err(ValidationError::InvalidValue("input_mode", "alpha, numeric, hex, all, or any combination using a pipe \"|\""))
                }
            }
            Ok(())
        },
        Err(_) => Ok(())
    }
}

impl Element for InputText {
    fn validate(value: &Value) -> FieldValidator {
        FieldValidator::new(value)
            .is_provider("provider")
            .is_valid(validate_alignment)
            .is_valid(validate_input_mode)
    }

    fn parse(_parser: &mut Parser, _context: &mut Context, value: &Value, base: ElementBase) -> ParseResult {
        let input_mode_text = value.optional_str("input_mode", "all");
        let mut input_mode = InputMode::empty();
        for split in input_mode_text.split('|') {
            match split {
                "alpha" => input_mode |= InputMode::ALPHA,
                "numeric" => input_mode |= InputMode::NUMERIC,
                "hex" => input_mode |= InputMode::HEX,
                "all" => input_mode |= InputMode::ALL,
                _ => { /* Do nothing */ }
            }
        }

        let provider = value.expect_provider("provider");
        let (vertical_align, horizontal_align) = value.optional_alignment("align", VerticalAlignment::Top, HorizontalAlignment::Left);

        ParseResultBuilder::new(value).with_element(InputText {
            base,
            vertical_align,
            horizontal_align,
            provider,
            editing: false,
            index: 0,
            click_position: None,
            commands: [Command::NONE; MAX_COMMANDS],
            command_count: 0,
            input_mode,
        }).build()
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        context: &mut Context,
        font: &Font,
    ) {
        self.base.build(position, width, height);
        if height.is_none() {
            self.base.size.y = font.get_font_height() as f32 + context.style.input_text.height_padding * 2.0;
        }
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        gui.draw_list.push_colored_quad(
            Rect::new_size_vector(self.base.position, self.base.size),
            gui.context.style.input_text.background_color,
        );

        if self.editing {
            gui.context.request_keyboard_focus();
            if !gui.context.request_element_focus(self as *mut dyn Element) {
                panic!("Could not request focus");
            }
        }

        let mut final_text: Option<String> = None;
        match self.provider {
            Provider::List(ref name) => {
                if let Some(provider) = gui.context.get_list_provider::<String>(&name) {
                    if let Some(text) = provider.get_next_mut() {
                        apply_commands(text, &mut self.index, &self.commands, &mut self.command_count);
                        final_text = Some(text.clone());
                    }
                }
            }
            Provider::Index { ref name, ref index } => {
                if let Some(provider) = gui.context.get_index_provider::<String>(&name) {
                    if let Some(text) = provider.get_mut(index) {
                        apply_commands(text, &mut self.index, &self.commands, &mut self.command_count);
                        final_text = Some(text.clone());
                    }
                }
            }
        }

        if let Some(text) = final_text {
            let start_vertex_offset = gui.draw_list.vertices.len();
            let text_position = self.get_text_position(gui.context);
            gui.font.create_text_positioned(
                &text,
                gui.context.style.input_text.text_color,
                &mut gui.draw_list.vertices,
                &mut gui.draw_list.indices,
                text_position,
                HorizontalOrigin::from(self.horizontal_align),
                VerticalOrigin::from(self.vertical_align),
                1.0,
            );

            let mut first_vertex_position = self.base.position;
            if let Some(first_vertex) = gui.draw_list.vertices.get(start_vertex_offset) {
                first_vertex_position = Vector2::new(first_vertex.x, first_vertex.y);
                if let Some(click_position) = self.click_position {
                    if click_position as f32 >= first_vertex.x {
                        self.index = gui
                            .font
                            .get_str_index(&text, click_position - (first_vertex.x as usize));
                    } else {
                        self.index = 0;
                    }
                    self.click_position = None;
                }
            }

            if self.editing {
                let text_center = gui.font.get_vertical_position(text_position, VerticalOrigin::from(self.vertical_align));

                let cursor_offset = gui.font.get_width_at_index(&text, self.index);
                let cursor_position = Vector2::new(first_vertex_position.x + cursor_offset as f32, text_center);

                if self.index > text.chars().count() {
                    self.index = text.chars().count();
                }

                gui.draw_list.push_colored_quad(
                    Rect::new_size_vector(cursor_position, Vector2::new(2.0, gui.font.get_line_height() as f32)),
                    gui.context.style.input_text.cursor_color,
                );
            }
        }
    }

    fn mouse_down(&mut self, context: &mut Context, _font: &Font, x: i32, y: i32, button: i32) -> bool {
        if button == 1 {
            if self.base.contains(x, y) {
                self.click_position = Some(x as usize);
                self.editing = true;
                true
            } else if self.editing {
                self.editing = false;
                context.release_element_focus(self as *mut dyn Element);
                true
            } else {
                false
            }
        } else {
            false
        }
    }

    fn key_down(&mut self, _context: &mut Context, key: GuiKey) -> bool {
        self.respond_to_key(key)
    }

    fn key_repeat(&mut self, _context: &mut Context, key: GuiKey) -> bool {
        self.respond_to_key(key)
    }

    fn text_input(&mut self, _context: &mut Context, text: &str) -> bool {
        if self.editing && self.command_count < MAX_COMMANDS && self.input_mode_allows(text) {
            if let Some(character) = text.chars().nth(0) {
                let command = Command::CHARACTER(character);

                self.commands[self.command_count] = command;
                self.command_count += 1;
                return true;
            }
        }
        false
    }

    fn get_providers(&self, context: &mut HashSet<String>) {
        match self.provider {
            Provider::List(ref name) => context.insert(name.clone()),
            Provider::Index { ref name, .. } => context.insert(name.clone()),
        };
    }

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}

impl InputText {
    fn respond_to_key(&mut self, key: GuiKey) -> bool {
        if self.editing && self.command_count < MAX_COMMANDS {
            if let Some(command) = self.translate_key(key) {
                self.commands[self.command_count] = command;
                self.command_count += 1;
                return true;
            }
        }
        false
    }

    fn translate_key(&self, key: GuiKey) -> Option<Command> {
        match key {
            GuiKey::LEFT => Some(Command::LEFT),
            GuiKey::RIGHT => Some(Command::RIGHT),
            GuiKey::ENTER => Some(Command::ENTER),
            GuiKey::BACKSPACE => Some(Command::BACKSPACE),
            _ => None,
        }
    }

    fn get_text_position(&self, context: &Context) -> Vector2 {
        let x_position = match self.horizontal_align {
            HorizontalAlignment::Left => self.base.position.x + context.style.input_text.height_padding,
            HorizontalAlignment::Right => {
                self.base.position.x + self.base.size.x - context.style.input_text.height_padding
            }
            HorizontalAlignment::Center => self.base.position.x + (self.base.size.x * 0.5).round(),
        };
        let y_position = match self.vertical_align {
            VerticalAlignment::Top => self.base.position.y,
            VerticalAlignment::Bottom => self.base.position.y + self.base.size.y,
            VerticalAlignment::Center => self.base.position.y + (self.base.size.y * 0.5).round(),
        };
        Vector2::new(x_position, y_position)
    }

    fn input_mode_allows(&self, text: &str) -> bool {
        if self.input_mode == InputMode::ALL {
            return true;
        }
        let mut passes = false;
        if self.input_mode.contains(InputMode::ALPHA) {
            let mut only_alpha = true;
            for character in text.chars() {
                if !character.is_alphabetic() {
                    only_alpha = false;
                    break;
                }
            }
            passes = passes || only_alpha;
        }
        if self.input_mode.contains(InputMode::NUMERIC) {
            let mut only_numeric = true;
            for character in text.chars() {
                if !character.is_numeric() {
                    only_numeric = false;
                    break;
                }
            }
            passes = passes || only_numeric;
        }
        if self.input_mode.contains(InputMode::HEX) {
            let mut only_hex = true;
            for character in text.chars() {
                if !character.is_ascii_hexdigit() {
                    only_hex = false;
                    break;
                }
            }
            passes = passes || only_hex;
        }

        passes
    }
}

#[allow(clippy::needless_range_loop)] // The range is most definitively needed
fn apply_commands(text: &mut String, index: &mut usize, commands: &[Command], command_count: &mut usize) {
    for i in 0..*command_count {
        let command = commands[i];
        match command {
            Command::LEFT => {
                if *index > 0 {
                    *index -= 1;
                }
            }
            Command::RIGHT => {
                if *index < text.chars().count() {
                    *index += 1;
                }
            }
            Command::BACKSPACE => {
                if *index > 0 {
                    if let Some((i, _)) = text.char_indices().nth(*index - 1) {
                        text.remove(i);
                        *index -= 1;
                    }
                }
            }
            Command::CHARACTER(character) => {
                if let Some((i, _)) = text.char_indices().nth(*index) {
                    text.insert(i, character);
                } else {
                    text.push(character);
                }
                *index += 1;
            }
            _ => {}
        }
    }
    *command_count = 0;
}

#[cfg(test)]
pub mod tests {
    use element::widgets::inputtext::*;
    use testutil::test_util::*;

    mod test_validation {
        use super::*;

        #[test]
        fn test_valid() {
            is_valid!(InputText, r#"(provider: "some_provider:some_index")"#);
            is_valid!(InputText, r#"(provider: "some_provider")"#);
            is_valid!(InputText, r#"(provider: "some_provider", align: "top|left")"#);
            is_valid!(InputText, r#"(provider: "some_provider", input_mode: "alpha|numeric|hex|all")"#);
        }

        #[test]
        fn test_invalid_missing_provider() {
            is_invalid!(InputText, r#"(align: "top|left")"#, ValidationError::NotFound("provider"));
        }

        #[test]
        fn test_invalid_input_mode() {
            is_invalid!(InputText, r#"(provider: "some_provider:some_index", input_mode: "abc")"#, ValidationError::InvalidValue("input_mode", "alpha, numeric, hex, all, or any combination using a pipe \"|\""));
        }
    }
}