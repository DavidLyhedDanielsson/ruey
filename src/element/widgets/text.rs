use element::*;
use parsing::provider::Provider;
use parsing::validationerror::ValidationError;
use parsing::alignment::*;
use std::convert::From;

use font::{Font, HorizontalOrigin, VerticalOrigin};

#[derive(Clone)]
pub struct Text {
    base: ElementBase,
    text: Option<String>,
    vertical_align: VerticalAlignment,
    horizontal_align: HorizontalAlignment,
    provider: Option<Provider>,
}

fn validate_text_or_provider(value: &Value) -> Result<(), ValidationError> {
    match (value.get_str("text"), value.get_str("provider")) {
        (Ok(_), Ok(_)) => Err(ValidationError::MutuallyExclusive("\"text\" and \"provider\"")),
        (Ok(_), Err(_)) => Ok(()),
        (Err(_), Ok(_)) => Ok(()),
        (Err(_), Err(_)) => Err(ValidationError::NotFoundEither("\"text\" or \"provider\"")),
    }
}

fn validate_alignment(value: &Value) -> Result<(), ValidationError> {
    match value.get_alignment("align") {
        Ok(_) => Ok(()),
        Err(err) => if let ValidationError::NotFound(_) = err {
            Ok(())
        } else {
            Err(err)
        }
    }
}

impl Element for Text {
    fn validate(value: &Value) -> FieldValidator {
        FieldValidator::new(value)
        .is_valid(validate_alignment)
        .is_valid(validate_text_or_provider)
    }

    fn parse(_parser: &mut Parser, _context: &mut Context, value: &Value, base: ElementBase) -> ParseResult {
        let (vertical_align, horizontal_align) = value.optional_alignment("align", VerticalAlignment::Top, HorizontalAlignment::Left);
        let (text, provider) = match value.get_str("text") {
            Ok(text_val) => (Some(text_val.to_string()), None),
            _ => (None, parse_provider(value.expect_str("provider")))
        };
        ParseResultBuilder::new(value).with_element(Text {
            base,
            text,
            vertical_align,
            horizontal_align,
            provider,
        }).build()
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        context: &mut Context,
        font: &Font,
    ) {
        self.base.position = position;
        if width.is_none() || height.is_none() {
            let mut new_width = 0.0;
            let mut new_height = 0.0;
            self.with_text(context, |text| {
                let mut size = font.measure_str(text);
                size.0 += 12;
                size.1 += 12;
                new_width = if let Some(width) = width {
                    width
                } else {
                    size.0 as f32
                };
                new_height = if let Some(height) = height {
                    height
                } else {
                    size.1 as f32
                };
            });

            self.base.size.x = new_width;
            self.base.size.y = new_height;
        } else {
            self.base.size = Vector2::new(width.unwrap(), height.unwrap());
        }
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        let color = gui.context.style.text.text_color;
        let draw_list = &mut gui.draw_list;
        let font = gui.font;
        self.with_text(gui.context, |text| {
            let mut draw_position = self.base.position;
            match self.vertical_align {
                VerticalAlignment::Top => { /* No need to do anything */ }
                VerticalAlignment::Bottom => draw_position.y += self.base.size.y,
                VerticalAlignment::Center => draw_position.y += (self.base.size.y * 0.5).round(),
            }
            match self.horizontal_align {
                HorizontalAlignment::Left => { /* No need to do anything */ }
                HorizontalAlignment::Right => draw_position.x += self.base.size.x,
                HorizontalAlignment::Center => draw_position.x += (self.base.size.x * 0.5).round(),
            }
            font.create_text_positioned(
                text,
                color,
                &mut draw_list.vertices,
                &mut draw_list.indices,
                draw_position,
                HorizontalOrigin::from(self.horizontal_align),
                VerticalOrigin::from(self.vertical_align),
                1.0,
            );
        });
    }

    fn get_providers(&self, context: &mut HashSet<String>) {
        if let Some(provider) = &self.provider {
            if let Provider::List(name) = provider {
                context.insert(name.to_string());
            }
        }
    }

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}

impl Text {
    fn with_text<F: FnMut(&str)>(&self, context: &mut Context, mut function: F) {
        if let Some(text) = &self.text {
            function(text);
        } else  if let Some(provider) = &self.provider {
            unsafe {
                if let Some(string) = provider.get_next_ref::<String>(context) {
                    function(string)
                } else if let Some(val) = provider.get_next_ref::<f32>(context) {
                    function(&val.to_string()) // TODO: Do this without dynamic allocation
                } else if let Some(val) = provider.get_next_ref::<i32>(context) {
                    function(&val.to_string())
                }
            }
        }
    }
}

#[cfg(test)]
pub mod tests {
    use element::widgets::text::*;
    use testutil::test_util::*;

    mod test_validation {
        use super::*;

        #[test]
        fn test_valid() {
            is_valid!(Text, r#"(text: "some_text", align: "top|left")"#);
        }
        #[test]
        fn test_valid_text() {
            is_valid!(Text, r#"(text: "some_text")"#);
        }
        #[test]
        fn test_valid_provider() {
            is_valid!(Text, r#"(provider: "some_provider")"#);
        }
        #[test]
        fn test_valid_align() {
            // Vertical|horizontal
            is_valid!(Text, r#"(text: "some_text", align: "top|left")"#);
            is_valid!(Text, r#"(text: "some_text", align: "center|left")"#);
            is_valid!(Text, r#"(text: "some_text", align: "bottom|left")"#);
            is_valid!(Text, r#"(text: "some_text", align: "top|center")"#);
            is_valid!(Text, r#"(text: "some_text", align: "top|right")"#);
            // Horizontal|vertical
            is_valid!(Text, r#"(text: "some_text", align: "left|top")"#);
            is_valid!(Text, r#"(text: "some_text", align: "left|center")"#);
            is_valid!(Text, r#"(text: "some_text", align: "left|bottom")"#);
            is_valid!(Text, r#"(text: "some_text", align: "center|top")"#);
            is_valid!(Text, r#"(text: "some_text", align: "right|top")"#);
        }
        #[test]
        fn test_text_and_provider() {
            is_invalid!(Text, r#"(text: "some_text", provider: "some_provider")"#, ValidationError::MutuallyExclusive(r#""text" and "provider""#));
        }
        #[test]
        fn test_missing_text_and_provider() {
            is_invalid!(Text, r#"(type: "text")"#, ValidationError::NotFoundEither(r#""text" or "provider""#));
        }
    }
}