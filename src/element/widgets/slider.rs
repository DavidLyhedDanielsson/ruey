use element::*;
use parsing::provider::Provider;

use font::{Font, HorizontalOrigin, VerticalOrigin};

#[derive(Clone)]
pub struct Slider {
    base: ElementBase,
    value_increment: f32,
    max_value: f32,
    min_value: f32,
    decimal_places: usize,
    mouse_down: bool,
    provider: Provider,
    grab_offset: f32,
    slider_area: Rect,
}

impl Element for Slider {
    fn validate(value: &Value) -> FieldValidator {
        FieldValidator::new(value)
            .is_provider("provider")
            .is_f32("max_value")
    }

    fn parse(_parser: &mut Parser, _context: &mut Context, value: &Value, base: ElementBase) -> ParseResult {
        ParseResultBuilder::new(value)
            .with_element(Slider {
                base,
                value_increment: value.optional_f32("value_increment", 1.0),
                max_value: value.expect_f32("max_value"),
                min_value: value.optional_f32("min_value", 0.0),
                decimal_places: value.optional_u32("decimal_places", 0) as usize + 1, // Include dot
                mouse_down: false,
                provider: parse_provider(value.expect_str("provider")).unwrap(),
                grab_offset: 0.0,
                slider_area: Rect::new_zero(),
            }).build()
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        context: &mut Context,
        font: &Font,
    ) {
        self.base.build(position, width, height);

        let min_value_width = Slider::value_text_width(self.min_value, self.decimal_places, font);
        let max_value_width = Slider::value_text_width(self.max_value, self.decimal_places, font);

        self.slider_area = Rect::new(
            self.base.position.x + min_value_width,
            self.base.position.y,
            self.base.position.x + self.base.size.x - max_value_width,
            self.base.position.y + self.base.size.y,
        );

        // An out-of-bounds value will cause the thumb to be outside of
        // self.area, so it should be clamped.
        match &self.provider {
            Provider::List(name) => {
                if let Some(provider) = context.get_list_provider::<f32>(&name) {
                    let val = provider.get_next_mut().unwrap();
                    *val = (*val).min(self.max_value).max(self.min_value);
                }
            }
            Provider::Index { name, index } => {
                if let Some(provider) = context.get_index_provider::<f32>(&name) {
                    let val = provider.get_mut(index).unwrap();
                    *val = (*val).min(self.max_value).max(self.min_value);
                }
            }
        };
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        if self.mouse_down {
            self.update_value(gui.mouse_x as f32, gui.context);
        }

        let value: f32 = match &self.provider {
            Provider::List(name) => {
                if let Some(provider) = gui.context.get_list_provider::<f32>(&name) {
                    *provider.get_next().unwrap_or(&self.min_value)
                } else {
                    self.min_value
                }
            }
            Provider::Index { name, index } => {
                if let Some(provider) = gui.context.get_index_provider::<f32>(&name) {
                    *provider.get(&index).unwrap_or(&self.min_value)
                } else {
                    self.min_value
                }
            }
        };

        gui.draw_list
            .push_colored_quad(self.slider_area, gui.context.style.slider.background_color);
        let thumb_area = self.thumb_area(value);
        if (thumb_area.size().x - self.slider_area.size().y).abs() > 0.01 {
            let step_size = self.slider_area.size().x / self.steps();
            for i in 0..(self.steps() as usize / 2) {
                gui.draw_list.push_colored_quad(
                    Rect::new_size_vector(
                        self.slider_area.min + Vector2::new(step_size * (i * 2) as f32, 0.0),
                        Vector2::new(step_size, thumb_area.size().y),
                    ),
                    gui.context.style.slider.step_background_color,
                );
            }
        }

        let thumb_color = if self.mouse_down {
            gui.context.style.slider.thumb_color_clicked
        } else if thumb_area.contains(gui.mouse_x as f32, gui.mouse_y as f32) {
            gui.context.style.slider.thumb_color_hovered
        } else {
            gui.context.style.slider.thumb_color
        };
        gui.draw_list.push_colored_quad(thumb_area, thumb_color);

        let (min_text, max_text, value_text) = self.min_max_value_text(value);
        gui.font.create_text_positioned(
            &min_text,
            gui.context.style.slider.min_max_text_color,
            &mut gui.draw_list.vertices,
            &mut gui.draw_list.indices,
            Vector2::new(self.slider_area.min.x, self.base.get_area().center().y),
            HorizontalOrigin::Right,
            VerticalOrigin::Center,
            1.0,
        );
        gui.font.create_text_positioned(
            &max_text,
            gui.context.style.slider.min_max_text_color,
            &mut gui.draw_list.vertices,
            &mut gui.draw_list.indices,
            Vector2::new(self.slider_area.max.x, self.base.get_area().center().y),
            HorizontalOrigin::Left,
            VerticalOrigin::Center,
            1.0,
        );

        let (value_text_position, value_text_horizontal_origin) =
            Slider::value_text_position(gui.font, &value_text, thumb_area, self.slider_area);
        gui.font.create_text_positioned(
            &value_text,
            gui.context.style.slider.value_text_color,
            &mut gui.draw_list.vertices,
            &mut gui.draw_list.indices,
            value_text_position,
            value_text_horizontal_origin,
            VerticalOrigin::Center,
            1.0,
        );
    }

    fn mouse_down(&mut self, context: &mut Context, _font: &Font, x: i32, y: i32, _button: i32) -> bool {
        let value: f32 = match &self.provider {
            Provider::List(name) => {
                if let Some(provider) = context.get_list_provider::<f32>(&name) {
                    *provider.get_next().unwrap_or(&self.min_value)
                } else {
                    self.min_value
                }
            }
            Provider::Index { name, index } => {
                if let Some(provider) = context.get_index_provider::<f32>(&name) {
                    *provider.get(&index).unwrap_or(&self.min_value)
                } else {
                    self.min_value
                }
            }
        };
        let thumb_area = self.thumb_area(value);
        if thumb_area.contains(x as f32, y as f32) {
            self.mouse_down = true;
            self.grab_offset = (x as f32 - thumb_area.min.x) + self.slider_area.min.x;
            true
        } else {
            false
        }
    }

    fn mouse_up(&mut self, _context: &mut Context, _font: &Font, _x: i32, _y: i32, _button: i32) -> bool {
        self.mouse_down = false;
        self.grab_offset = 0.0;
        false
    }

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }

    fn allow_unsized() -> bool {
        false
    }
}

impl Slider {
    fn value_text_width(value: f32, decimal_places_including_dot: usize, font: &Font) -> f32 {
        let mut value_length = if value <= -1.0 || value >= 1.0 {
            // log_10(< 10.0) is truncated to 0, so add 1
            value.abs().log10() as usize + decimal_places_including_dot + 1
        } else {
            // decimal_places includes the dot, but not the 0 before the dot
            decimal_places_including_dot + 1
        };
        if value < 0.0 {
            value_length += 1;
        }
        font.measure_str(&str::repeat("X", value_length)).0 as f32
    }

    fn min_max_value_text(&self, value: f32) -> (String, String, String) {
        (
            format!("{:.*}", self.decimal_places - 1, self.min_value),
            format!("{:.*}", self.decimal_places - 1, self.max_value),
            format!("{:.*}", self.decimal_places - 1, value),
        )
    }

    fn value_text_position(
        font: &Font,
        value_text: &str,
        thumb_area: Rect,
        slider_area: Rect,
    ) -> (Vector2, HorizontalOrigin) {
        let value_text_size = font.measure_str(&value_text);
        if thumb_area.center().x - value_text_size.0 as f32 * 0.5 < slider_area.min.x {
            (
                Vector2::new(slider_area.min.x, slider_area.center().y),
                HorizontalOrigin::Left,
            )
        } else if thumb_area.center().x + value_text_size.0 as f32 * 0.5 > slider_area.max.x {
            (
                Vector2::new(slider_area.max.x, slider_area.center().y),
                HorizontalOrigin::Right,
            )
        } else {
            (thumb_area.center(), HorizontalOrigin::Center)
        }
    }

    fn steps(&self) -> f32 {
        ((self.max_value - self.min_value) / self.value_increment) + 1.0
    }
    fn thumb_width(&self) -> f32 {
        (self.slider_area.size().x / self.steps()).max(self.slider_area.size().y)
    }

    fn thumb_area(&self, value: f32) -> Rect {
        let value_range = self.max_value - self.min_value;
        let thumb_position_x =
            (value - self.min_value) / value_range * (self.slider_area.size().x - self.thumb_width());
        Rect::new_size_vector(
            Vector2::new(self.slider_area.min.x + thumb_position_x, self.slider_area.min.y),
            Vector2::new(self.thumb_width(), self.slider_area.size().y),
        )
    }

    #[allow(clippy::float_cmp)] // thumb_width is set to exactly base.size.y, so it's fine to compare it
    fn update_value(&self, mouse_x: f32, context: &mut Context) {
        let thumb_width = self.thumb_width();
        let (x_pos, step_size) = if thumb_width == self.slider_area.size().y {
            (
                mouse_x - self.grab_offset,
                (self.slider_area.size().x - thumb_width) / self.steps(),
            )
        } else {
            (
                mouse_x - self.slider_area.min.x,
                self.slider_area.size().x / self.steps(),
            )
        };
        let rounded_value = (x_pos / step_size).floor();
        let new_value = (self.min_value + (rounded_value * self.value_increment))
            .min(self.max_value)
            .max(self.min_value);
        match &self.provider {
            Provider::List(name) => {
                if let Some(provider) = context.get_list_provider::<f32>(&name) {
                    *provider.get_next_mut().unwrap() = new_value;
                }
            }
            Provider::Index { name, index } => {
                if let Some(provider) = context.get_index_provider::<f32>(&name) {
                    *provider.get_mut(index).unwrap() = new_value;
                }
            }
        };
    }
}

#[cfg(test)]
pub mod tests {
    use element::widgets::slider::*;
    use testutil::test_util::*;

    mod test_validation {
        use super::*;

        #[test]
        fn test_valid() {
            is_valid!(Slider, r#"(provider: "some_provider:some_index", max_value: 123.45)"#);
            is_valid!(Slider, r#"(provider: "some_provider", max_value: 123.45)"#);
        }

        #[test]
        fn test_invalid_missing_provider() {
            is_invalid!(Slider, r#"(max_value: 123.45)"#, ValidationError::NotFound("provider"));
        }

        #[test]
        fn test_invalid_missing_max_value() {
            is_invalid!(Slider, r#"(provider: "some_provider")"#, ValidationError::NotFound("max_value"));
        }
    }
}