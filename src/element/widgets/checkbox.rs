use element::*;
use parsing::provider::Provider;

use font::{Font, HorizontalOrigin, VerticalOrigin};

#[derive(Clone)]
pub struct Checkbox {
    base: ElementBase,
    text: String,
    mouse_down: bool,
    provider: Provider,
}

impl Element for Checkbox {
    fn validate(value: &Value) -> FieldValidator {
        FieldValidator::new(value)
            .is_str("text")
            .is_provider("provider")
    }

    fn parse(_parser: &mut Parser, _context: &mut Context, value: &Value, base: ElementBase) -> ParseResult {
        let text = value.expect_str("text").to_string();
        let provider = value.expect_provider("provider");
        ParseResultBuilder::new(value).with_element(Checkbox {
            base,
            text,
            mouse_down: false,
            provider,
        }).build()
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        _context: &mut Context,
        font: &Font,
    ) {
        if width.is_none() || height.is_none() {
            let mut size = font.measure_str(&self.text);
            size.0 += 12;
            size.1 += 12;
            if let Some(height) = height {
                self.base.size.y = height;
            } else {
                self.base.size.y = size.1 as f32;
            }
            if let Some(width) = width {
                self.base.size.x = width;
            } else {
                self.base.size.x = size.0 as f32 + size.1 as f32;
            }
        } else {
            self.base.size.x = width.unwrap();
            self.base.size.y = height.unwrap();
        }
        self.base.position = position;
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        let value = self.provider.get_next_optional(gui.context, false);

        let background_color = if self.mouse_down {
            gui.context.style.checkbox.background_color_clicked
        } else if self.base.contains(gui.mouse_x, gui.mouse_y) {
            gui.context.style.checkbox.background_color_hovered
        } else {
            gui.context.style.checkbox.background_color
        };
        gui.draw_list.push_colored_quad(
            Rect::new_size_vector(self.base.position, self.base.size),
            background_color,
        );

        let check_area = gui.context.style.checkbox.check_area(self.base.get_area());
        gui.draw_list.push_outline_quad(
            check_area,
            gui.context.style.checkbox.check_box_border_thickness,
            gui.context.style.checkbox.text_color,
        );

        if value {
            let area = gui.context.style.checkbox.bordered_check_area(self.base.get_area());
            let min = area.min + Vector2::new_replicate(gui.context.style.checkbox.check_padding);
            let max = area.max - Vector2::new_replicate(gui.context.style.checkbox.check_padding);
            let t = gui.context.style.checkbox.check_thickness;
            gui.draw_list.push_irregular_quad(
                (min.x + t, min.y),
                (min.x, min.y + t),
                (max.x - t, max.y),
                (max.x, max.y - t),
                gui.context.style.checkbox.text_color,
            );
            gui.draw_list.push_irregular_quad(
                (min.x + t, max.y),
                (min.x, max.y - t),
                (max.x - t, min.y),
                (max.x, min.y + t),
                gui.context.style.checkbox.text_color,
            );
        }

        gui.font.create_text_positioned(
            &self.text,
            gui.context.style.button.text_color,
            &mut gui.draw_list.vertices,
            &mut gui.draw_list.indices,
            self.base.position
                + Vector2::new(
                    self.base.size.y + gui.context.style.checkbox.check_box_text_padding,
                    self.base.size.y * 0.5 + gui.context.style.checkbox.text_vertical_offset,
                ),
            HorizontalOrigin::Left,
            VerticalOrigin::Center,
            1.0,
        );
    }

    fn mouse_down(&mut self, _context: &mut Context, _font: &Font, x: i32, y: i32, _button: i32) -> bool {
        if self.base.contains(x, y) {
            self.mouse_down = true;
            true
        } else {
            false
        }
    }

    fn mouse_up(&mut self, context: &mut Context, _font: &Font, x: i32, y: i32, _button: i32) -> bool {
        let mouse_down = self.mouse_down;
        self.mouse_down = false;
        if mouse_down && self.base.contains(x, y) {
            unsafe {
                if let Some(value) = self.provider.get_next_ref_mut::<bool>(context) {
                    *value = !*value;
                }
            }
            true
        } else {
            false
        }
    }

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}

#[cfg(test)]
pub mod tests {
    use element::widgets::checkbox::*;
    use testutil::test_util::*;

    mod test_validation {
        use super::*;

        #[test]
        fn test_valid() {
            is_valid!(Checkbox, r#"(text: "some_text", provider: "some_provider:some_index")"#);
            is_valid!(Checkbox, r#"(text: "some_text", provider: "some_provider")"#);
        }

        #[test]
        fn test_invalid_missing_text() {
            is_invalid!(Checkbox, r#"(provider: "some_provider")"#, ValidationError::NotFound("text"));
        }

        #[test]
        fn test_invalid_missing_provider() {
            is_invalid!(Checkbox, r#"(text: "some_text")"#, ValidationError::NotFound("provider"));
        }
    }
}