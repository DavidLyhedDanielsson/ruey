use element::*;

use color::Color as rawcolor;

use std::str;
use std::str::from_utf8_unchecked;

#[derive(Clone)]
pub struct Color {
    base: ElementBase,
    color: rawcolor,
}

fn parse_u8<'a, I>(iter: &mut I) -> Result<u8, ()>
where
    I: Iterator<Item = &'a [u8]>,
{
    if let Some(str_val) = iter.next() {
        let val;
        unsafe {
            val = u8::from_str_radix(from_utf8_unchecked(str_val), 16);
        }
        match val {
            Ok(val) => Ok(val),
            _ => Err(()),
        }
    } else {
        Err(())
    }
}

fn parse_color_string(value: &str) -> Result<(u8, u8, u8, u8), ()> {
    let mut iter = value.as_bytes().chunks(2);
    if let (Ok(r), Ok(g), Ok(b), Ok(a)) = (
        parse_u8(&mut iter),
        parse_u8(&mut iter),
        parse_u8(&mut iter),
        parse_u8(&mut iter),
    ) {
        Ok((r, g, b, a))
    } else {
        Err(())
    }
}

impl Element for Color {
    fn validate(value: &Value) -> FieldValidator {
        FieldValidator::new(value)
            .is_valid(|val| {
                match parse_color_string(val.get_str("color")?) {
                    Ok(_) => Ok(()),
                    Err(_) => Err(ValidationError::InvalidValue("color", "a hex string with 4 colors e.g. ABCDEFFF"))
                }
            })
    }

    fn parse(_parser: &mut Parser, _context: &mut Context, value: &Value, base: ElementBase) -> ParseResult {
        let mut parse_response = ParseResultBuilder::new(value);
        let color = value.expect_str("color");
        let (r, g, b, a) = match parse_color_string(color) {
            Ok(values) => values,
            Err(_) => {
                parse_response.with_error(ValidationError::InvalidValue("color", "Could not parse color"));
                return parse_response.build();
            }
        };
        parse_response.with_element(Color {
            base,
            color: rawcolor::newb(r, g, b, a),
        }).build()
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        _context: &mut Context,
        _font: &Font,
    ) {
        self.base.size.x = width.expect("A size was not given to color widget");
        self.base.size.y = height.expect("A size was not given to color widget");
        self.base.position = position;
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        gui.draw_list
            .push_colored_quad(Rect::new_size_vector(self.base.position, self.base.size), self.color);
    }

    fn get_providers(&self, _context: &mut HashSet<String>) {}
    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn allow_unsized() -> bool {
        false
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}

#[cfg(test)]
pub mod tests {
    use element::widgets::color::*;
    use testutil::test_util::*;

    // This element is probably only used for debugging, so only minimal text coverage
    mod test_validation {
        use super::*;

        #[test]
        fn test_valid() {
            is_valid!(Color, r#"(color: "ABCDEFFF")"#);
        }

        #[test]
        fn test_invalid_missing_color() {
            is_invalid!(Color, r#"(type: "color")"#, ValidationError::NotFound("color"));
        }
    }
}