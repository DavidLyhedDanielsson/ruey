use element::*;
use parsing::provider::Provider;

use font::{Font, HorizontalOrigin, VerticalOrigin};

#[derive(Clone)]
pub struct Button {
    base: ElementBase,
    text: String,
    provider: Provider,
    mouse_down: bool,
    button_clicked: bool,
}

impl Element for Button {
    fn validate(value: &Value) -> FieldValidator {
        FieldValidator::new(value)
            .is_str("text")
            .is_provider("provider")
    }

    fn parse(_parser: &mut Parser, _context: &mut Context, value: &Value, base: ElementBase) -> ParseResult {
        let text = value.expect_str("text").to_string();
        let provider = value.expect_provider("provider");
        ParseResultBuilder::new(value).with_element(Button {
            base,
            text,
            provider,
            mouse_down: false,
            button_clicked: false,
        }).build()
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        _context: &mut Context,
        font: &Font,
    ) {
        if width.is_none() || height.is_none() {
            let mut size = font.measure_str(&self.text);
            size.0 += 12;
            size.1 += 12;
            if let Some(width) = width {
                self.base.size.x = width;
            } else {
                self.base.size.x = size.0 as f32;
            }
            if let Some(height) = height {
                self.base.size.y = height;
            } else {
                self.base.size.y = size.1 as f32;
            }
        } else {
            self.base.size.x = width.unwrap();
            self.base.size.y = height.unwrap();
        }
        self.base.position = position;
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        let provider_value = if self.button_clicked {
            self.button_clicked = false;
            true
        } else {
            false
        };

        if let Some(value_ref) = unsafe{ self.provider.get_next_ref_mut(gui.context) } {
            *value_ref = provider_value;
        }

        if self.mouse_down {
            gui.draw_list.push_colored_quad(
                Rect::new_size_vector(self.base.position, self.base.size),
                gui.context.style.button.background_color,
            );
        } else if self.base.contains(gui.mouse_x, gui.mouse_y) {
            gui.draw_list.push_colored_quad(
                Rect::new_size_vector(self.base.position, self.base.size),
                gui.context.style.button.background_color_hovered,
            );
        } else {
            gui.draw_list.push_colored_quad(
                Rect::new_size_vector(self.base.position, self.base.size),
                gui.context.style.button.background_color_clicked,
            );
        }
        gui.font.create_text_positioned(
            &self.text,
            gui.context.style.button.text_color,
            &mut gui.draw_list.vertices,
            &mut gui.draw_list.indices,
            self.base.position + self.base.size * 0.5,
            HorizontalOrigin::Center,
            VerticalOrigin::Center,
            1.0,
        );
    }

    fn mouse_down(&mut self, _context: &mut Context, _font: &Font, x: i32, y: i32, _button: i32) -> bool {
        if self.base.contains(x, y) {
            self.mouse_down = true;
            true
        } else {
            false
        }
    }

    fn mouse_up(&mut self, _context: &mut Context, _font: &Font, x: i32, y: i32, _button: i32) -> bool {
        self.mouse_down = false;
        if self.base.contains(x, y) {
            self.button_clicked = true;
            true
        } else {
            false
        }
    }

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}

#[cfg(test)]
pub mod tests {
    use element::widgets::button::*;
    use testutil::test_util::*;

    mod test_validation {
        use super::*;

        #[test]
        fn test_valid_index_provider() {
            is_valid!(Button, r#"(text: "some_text", provider: "some_provider:some_index")"#);
        }

        #[test]
        fn test_valid_list_provider() {
            is_valid!(Button, r#"(text: "some_text", provider: "some_provider")"#);
        }

        #[test]
        fn test_invalid_missing_text() {
            is_invalid!(Button, r#"(provider: "some_provider:some_index")"#, ValidationError::NotFound("text"));
        }

        #[test]
        fn test_invalid_missing_provider() {
            is_invalid!(Button, r#"(text: "some_text")"#, ValidationError::NotFound("provider"));
        }
    }
}