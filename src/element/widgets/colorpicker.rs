use element::*;
use color::Color;
use hsvcolor::HSVColor;
use parsing::provider::Provider;

#[derive(Clone, Copy)]
enum ChangeMode {
    Hue,
    SaturationValue,
    Alpha,
}

#[derive(Clone)]
pub struct ColorPicker {
    base: ElementBase,
    provider: Provider,
    selected_color: Color,
    open: bool,
    change_mode: Option<ChangeMode>,
    allow_alpha: bool,
    hue: f32,
    saturation: f32,
    value: f32,
    alpha: f32,
}

impl Element for ColorPicker {
    fn validate(value: &Value) -> FieldValidator {
        FieldValidator::new(value)
            .is_provider("provider")
    }

    fn parse(_parser: &mut Parser, _context: &mut Context, value: &Value, base: ElementBase) -> ParseResult {
        let provider = value.expect_provider("provider");
        ParseResultBuilder::new(value).with_element(ColorPicker {
            base,
            provider,
            selected_color: Color::newb(255, 0, 0, 255),
            open: false,
            change_mode: None,
            allow_alpha: value.optional_bool("alpha", false),
            hue: 0.0,
            saturation: 1.0,
            value: 1.0,
            alpha: 1.0,
        }).build()
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        context: &mut Context,
        _font: &Font,
    ) {
        self.base.build(position, width, height);

        match &self.provider {
            Provider::List(name) => {
                if let Some(provider) = context.get_list_provider::<Color>(&name) {
                    if let Some(value) = provider.get_next_mut() {
                        self.selected_color = *value;
                    }
                }
            }
            Provider::Index { name, index } => {
                if let Some(provider) = context.get_index_provider::<Color>(&name) {
                    if let Some(value) = provider.get_mut(&index) {
                        self.selected_color = *value;
                    }
                }
            }
        }

        let hsv_color = HSVColor::new_rgb(self.selected_color.r, self.selected_color.g, self.selected_color.b);
        self.hue = hsv_color.hue;
        self.saturation = hsv_color.saturation;
        self.value = hsv_color.value;
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        if let Some(color) = unsafe { self.provider.peek_next::<Color>(gui.context) } {
            self.selected_color = color;
        }
        self.draw_closed(gui);
        if self.open {
            self.draw_open(gui);
        } else {
            // The color needs to be "consumed" so that the provider acts as expected
            unsafe{ self.provider.get_next_ref_mut::<Color>(gui.context) };
        }
    }

    fn mouse_down(&mut self, context: &mut Context, _font: &Font, x: i32, y: i32, _button: i32) -> bool {
        if self.open {
            let hue_area = Rect::new_size(self.base.position.x, self.base.position.y + self.base.size.y, 24.0, self.base.size.x);
            let alpha_area = if self.allow_alpha {
                Rect::new_size(
                    self.base.position.x + self.base.size.x - 24.0,
                    self.base.position.y,
                    24.0,
                    self.base.size.x,
                )
            } else {
                Rect::new_zero()
            };
            let matrix_area = Rect::new_size(
                hue_area.max.x,
                hue_area.min.y,
                self.base.size.x - hue_area.size().x - alpha_area.size().x,
                hue_area.size().y,
            );
            if hue_area.contains(x as f32, y as f32) {
                self.change_mode = Some(ChangeMode::Hue);
                true
            } else if matrix_area.contains(x as f32, y as f32) {
                self.change_mode = Some(ChangeMode::SaturationValue);
                true
            } else if alpha_area.contains(x as f32, y as f32) {
                self.change_mode = Some(ChangeMode::Alpha);
                true
            } else {
                self.change_mode = None;
                self.open = false;
                context.release_element_focus(self as *mut dyn Element);
                false
            }
        } else if self.base.contains(x, y) {
            self.open = true;
            context.request_element_focus(self as *mut dyn Element);
            true
        } else {
            false
        }
    }

    fn mouse_up(&mut self, _context: &mut Context, _font: &Font, _x: i32, _y: i32, _button: i32) -> bool {
        self.change_mode = None;
        self.open
    }

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }

    fn get_providers(&self, context: &mut HashSet<String>) {
        match self.provider {
            Provider::List(ref name) => context.insert(name.clone()),
            Provider::Index { ref name, .. } => context.insert(name.clone()),
        };
}
}

impl ColorPicker {
    fn draw_checkers_vertical(&self, gui: &mut GuiVariables, area: Rect, tiles: i32) {
        gui.overlay_draw_list
            .push_colored_quad(area, Color::newb(200, 200, 200, 255));
        let tile_width = area.size().x * 0.5;
        let tile_height = area.size().y / tiles as f32;
        for i in 0..10 {
            gui.overlay_draw_list.push_colored_quad(
                Rect::new_size(
                    area.min.x + (tile_width * (i % 2) as f32),
                    area.min.y + tile_height * i as f32,
                    tile_width,
                    tile_height,
                ),
                Color::newb(150, 150, 150, 255),
            );
        }
    }
    fn draw_checkers_horizontal(&self, gui: &mut GuiVariables, area: Rect, tiles: i32) {
        gui.draw_list.push_colored_quad(area, Color::newb(200, 200, 200, 255));
        let tile_width = area.size().x / tiles as f32;
        let tile_height = area.size().y * 0.5;
        for i in 0..10 {
            gui.draw_list.push_colored_quad(
                Rect::new_size(
                    area.min.x + tile_width * i as f32,
                    area.min.y + (tile_height * (i % 2) as f32),
                    tile_width,
                    tile_height,
                ),
                Color::newb(150, 150, 150, 255),
            );
        }
    }

    fn draw_closed(&mut self, gui: &mut GuiVariables) {
        let color_area = Rect::new_size(
            self.base.position.x + 1.0,
            self.base.position.y + 1.0,
            self.base.size.x - 2.0,
            self.base.size.y - 2.0,
        );

        gui.draw_list
            .push_colored_quad(self.base.get_area(), gui.context.style.window.split_border_color);
        if self.allow_alpha || self.selected_color.a < 1.0 {
            self.draw_checkers_horizontal(gui, color_area, 10);
        }
        gui.draw_list.push_colored_quad(color_area, self.selected_color);

    }
    fn draw_open(&mut self, gui: &mut GuiVariables) {
        let picker_area = Rect::new_size(
            self.base.position.x,
            self.base.position.y + self.base.size.y,
            self.base.size.x,
            self.base.size.x,
        );
        let hue_area = Rect::new_size(picker_area.min.x, picker_area.min.y, 24.0, picker_area.size().y);
        let alpha_area = if self.allow_alpha {
            Rect::new_size(
                picker_area.min.x + picker_area.size().x - 24.0,
                picker_area.min.y,
                24.0,
                picker_area.size().x,
            )
        } else {
            Rect::new_zero()
        };
        let matrix_area = Rect::new_size(
            hue_area.min.x + 24.0,
            hue_area.min.y,
            picker_area.size().x - hue_area.size().x - alpha_area.size().x,
            hue_area.size().y,
        );
        let color_ref = unsafe{ self.provider.get_next_ref_mut::<Color>(gui.context) };
        if let Some(change_mode) = self.change_mode {
            match change_mode {
                ChangeMode::Hue => {
                    self.hue = ((gui.mouse_y as f32 - hue_area.min.y) / hue_area.size().y * 360.0)
                        .min(360.0)
                        .max(0.0);
                }
                ChangeMode::SaturationValue => {
                    self.saturation = ((gui.mouse_x as f32 - matrix_area.min.x) / matrix_area.size().x)
                        .min(1.0)
                        .max(0.0);
                    self.value = 1.0
                        - ((gui.mouse_y as f32 - matrix_area.min.y) / matrix_area.size().y)
                            .min(1.0)
                            .max(0.0);
                }
                ChangeMode::Alpha => {
                    self.selected_color.a = 1.0
                        - ((gui.mouse_y as f32 - hue_area.min.y) / hue_area.size().y)
                            .min(1.0)
                            .max(0.0);
                }
            }
            self.selected_color = HSVColor::new(self.hue, self.saturation, self.value).to_rgba(self.selected_color.a);
            if let Some(color) = color_ref {
                *color = self.selected_color;
            }
        }

        let section_height = picker_area.size().x / 6.0;
        let mut section = Rect::new_size(picker_area.min.x, picker_area.min.y, 24.0, section_height);

        let section_colors = [
            Color::newb(255, 0, 0, 255),
            Color::newb(255, 255, 0, 255),
            Color::newb(0, 255, 0, 255),
            Color::newb(0, 255, 255, 255),
            Color::newb(0, 0, 255, 255),
            Color::newb(255, 0, 255, 255),
            Color::newb(255, 0, 0, 255),
        ];

        for i in 0..6 {
            gui.overlay_draw_list.push_multicolored_quad(
                section,
                section_colors[i],
                section_colors[i],
                section_colors[i + 1],
                section_colors[i + 1],
            );

            section.min.y += section_height;
            section.max.y += section_height;
        }

        let hue_color = HSVColor::new(self.hue, 1.0, 1.0);
        {
            let selection_position =
                Vector2::new(hue_area.min.x, hue_area.min.y + hue_area.size().y * self.hue / 360.0);
            let selection_area = Rect::new_size_vector(selection_position, Vector2::new(hue_area.size().x, 3.0));
            gui.overlay_draw_list
                .push_outline_quad(selection_area, 1.0, Color::white())
        }

        if self.allow_alpha {
            self.draw_checkers_vertical(gui, alpha_area, 10);
            let opaque = Color::newb(255, 255, 255, 255);
            let transparent = Color::newb(255, 255, 255, 0);
            gui.overlay_draw_list
                .push_multicolored_quad(alpha_area, opaque, opaque, transparent, transparent);

            let selection_position = Vector2::new(
                alpha_area.min.x,
                alpha_area.min.y + alpha_area.size().y * (1.0 - self.selected_color.a) - 1.0,
            );
            let selection_area = Rect::new_size_vector(selection_position, Vector2::new(alpha_area.size().x, 3.0));
            gui.overlay_draw_list
                .push_outline_quad(selection_area, 1.0, Color::white())
        }

        // Smart rendering solution by https://github.com/ocornut/imgui/issues/346#issuecomment-172087814 !
        // No need to implement HSV/HSL rendering and complicated draw lists :)
        gui.overlay_draw_list.push_multicolored_quad(
            matrix_area,
            Color::newb(255, 255, 255, 255),
            hue_color.to_rgb(),
            Color::newb(255, 255, 255, 255),
            hue_color.to_rgb(),
        );
        gui.overlay_draw_list.push_multicolored_quad(
            matrix_area,
            Color::newb(0, 0, 0, 0),
            Color::newb(0, 0, 0, 0),
            Color::newb(0, 0, 0, 255),
            Color::newb(0, 0, 0, 255),
        );

        let selection_position = Vector2::new(
            matrix_area.min.x + matrix_area.size().x * self.saturation,
            matrix_area.min.y + matrix_area.size().y * (1.0 - self.value),
        );
        let selection_area = Rect::new_size_vector(
            selection_position - Vector2::new_replicate(5.0),
            Vector2::new_replicate(10.0),
        );
        gui.overlay_draw_list
            .push_outline_quad(selection_area, 1.0, Color::newb(255, 255, 255, 255));
    }
}

#[cfg(test)]
pub mod tests {
    use element::widgets::colorpicker::*;
    use testutil::test_util::*;

    mod test_validation {
        use super::*;

        #[test]
        fn test_valid() {
            is_valid!(ColorPicker, r#"(provider: "some_provider:some_index")"#);
            is_valid!(ColorPicker, r#"(provider: "some_provider")"#);
        }

        #[test]
        fn test_invalid_missing_provider() {
            is_invalid!(ColorPicker, r#"(type: "colorpicker")"#, ValidationError::NotFound("provider"));
        }
    }
}