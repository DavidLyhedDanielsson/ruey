use element::*;
use font::{Font, HorizontalOrigin, VerticalOrigin};
use parsing::provider::Provider;

#[derive(Clone)]
enum ProviderOrArray {
    Provider(String),
    Array(Vec<String>),
}

#[derive(Clone)]
pub struct RadioButton {
    base: ElementBase,
    selected_index: usize,
    mouse_down: bool,
    new_index: usize,
    provider: Provider,
    options: ProviderOrArray,
}

fn validate_options(value: &Value) -> Result<(), ValidationError> {
    let options_provider = value.get_str("options_provider");
    let options = value.get_array("options");

    match (options_provider, options) {
        (Ok(options_provider), Err(_)) => {
            match parse_provider(options_provider) {
                Some(Provider::List(_)) => Ok(()),
                Some(Provider::Index{..}) => Err(ValidationError::InvalidValue("options_provider", "a list provider")),
                None => Err(ValidationError::InvalidValue("options_provider", "a list provider")),
            }
        }
        (Err(_), Ok(options)) => {
            for option in options {
                match option {
                    Value::String(_) => { /* Do nothing */ }
                    _ => return Err(ValidationError::WrongType("options", "an array of strings"))
                }
            }
            Ok(())
        }
        (Ok(_), Ok(_)) => Err(ValidationError::MutuallyExclusive(r#""options_provider" and "options""#)),
        (Err(_), Err(_)) => Err(ValidationError::NotFoundEither(r#""options_provider" or "options""#)),
    }
}

impl Element for RadioButton {
    fn validate(value: &Value) -> FieldValidator {
        FieldValidator::new(value)
            .is_provider("provider")
            .is_valid(validate_options)
    }

    fn parse(_parser: &mut Parser, _context: &mut Context, value: &Value, base: ElementBase) -> ParseResult {
        let options = if let Ok(provider) = value.get_str("options_provider") {
            ProviderOrArray::Provider(provider.to_string())
        } else {
            let values = value.expect_array("options");
            ProviderOrArray::Array(values.iter().map(|val| match val {
                ron::Value::String(text) => (*text).clone(),
                _ => panic!("Invalid value in options array")
            }).collect())
        };
        ParseResultBuilder::new(value)
            .with_element(RadioButton {
                base,
                provider: value.expect_provider("provider"),
                options,
                selected_index: 0,
                mouse_down: false,
                new_index: 0,
            })
            .build()
    }

    fn build(
        &mut self,
        position: Vector2,
        _width: Option<f32>,
        _height: Option<f32>,
        context: &mut Context,
        font: &Font,
    ) {
        let height = font.get_font_height() as f32 + context.style.radio_button.line_thickness;
        let mut width = 0.0;
        self.for_each_option(context, |context, _, option| {
            let size = font.measure_str(option);
            width += size.0 as f32 + context.style.radio_button.option_padding * 2.0;
        });
        self.base.build(position, Some(width), Some(height));
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        let padding = gui.context.style.radio_button.option_padding;
        let mut draw_position = self.base.position;
        let draw_list = &mut gui.draw_list;
        let font = gui.font;
        let selected_index = self.selected_index;
        let new_index = self.new_index;
        let mouse_down = self.mouse_down;
        self.for_each_option(gui.context, |context, i, option| {
            let size = font.measure_str(option);
            let (background_color, text_color) = if i == selected_index {
                (context.style.radio_button.option_background_color_active,
                context.style.radio_button.option_text_color_active)
            } else if mouse_down && i == new_index {
                (context.style.radio_button.option_background_color_clicked,
                context.style.radio_button.option_text_color_clicked)
            } else {
                (context.style.radio_button.option_background_color_inactive,
                context.style.radio_button.option_text_color_inactive)
            };
            let button_area = Rect::new_size(
                draw_position.x,
                draw_position.y,
                size.0 as f32 + padding * 2.0,
                font.get_font_height() as f32 + context.style.radio_button.line_thickness,
            );
            draw_list.push_colored_quad(button_area, background_color);
            if i == selected_index {
                draw_list.push_colored_quad(
                    Rect::new_size(
                        button_area.min.x + padding,
                        button_area.max.y - context.style.radio_button.line_thickness - context.style.radio_button.line_bottom_offset,
                        size.0 as f32,
                        context.style.radio_button.line_thickness,
                    ),
                    text_color,
                );
                font.create_text_positioned(
                    option,
                    text_color,
                    &mut draw_list.vertices,
                    &mut draw_list.indices,
                    Vector2::new(button_area.center().x, button_area.max.y - context.style.radio_button.line_thickness - context.style.radio_button.line_bottom_offset),
                    HorizontalOrigin::Center,
                    VerticalOrigin::Bottom,
                    1.0,
                );
            } else {
                font.create_text_positioned(
                    option,
                    text_color,
                    &mut draw_list.vertices,
                    &mut draw_list.indices,
                    button_area.center(),
                    HorizontalOrigin::Center,
                    VerticalOrigin::Center,
                    1.0,
                );
            };
            draw_position.x += button_area.size().x;
        });
    }

    fn mouse_down(&mut self, context: &mut Context, font: &Font, x: i32, y: i32, _button: i32) -> bool {
        if let Some(i) = self.button_index(context, font, Vector2::new(x as f32, y as f32)) {
            self.mouse_down = true;
            self.new_index = i;
            true
        } else {
            false
        }
    }

    fn mouse_up(&mut self, context: &mut Context, font: &Font, x: i32, y: i32, _button: i32) -> bool {
        if let Some(i) = self.button_index(context, font, Vector2::new(x as f32, y as f32)) {
            if i == self.new_index {
                self.selected_index = self.new_index;
                unsafe {
                    if let Some(val) = self.provider.get_next_ref_mut::<i32>(context) {
                        *val = self.new_index as i32;
                    }
                }
            }
            self.new_index = 0;
        }
        let return_value = self.mouse_down;
        self.mouse_down = false;
        return_value
    }

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}

impl RadioButton {
    fn button_index(&mut self, context: &mut Context, font: &Font, position: Vector2) -> Option<usize> {
        let mut draw_position = self.base.position;
        let mut new_index = None;
        self.for_each_option(context, |context, i, option| {
            let size = font.measure_str(option);
            let button_area = Rect::new_size(
                draw_position.x,
                draw_position.y,
                size.0 as f32 + context.style.radio_button.option_padding * 2.0,
                font.get_font_height() as f32 + context.style.radio_button.line_thickness,
            );
            if button_area.contains(position.x, position.y) {
                new_index = Some(i);
            }
            draw_position.x += button_area.size().x;
        });
        new_index
    }

    fn for_each_option<F: FnMut(&Context, usize, &String)>(&mut self, context: &mut Context, mut function: F) {
        match &self.options {
            ProviderOrArray::Array(arr) => Some(arr.iter().enumerate().for_each(|(i, option)| function(context, i, option))),
            ProviderOrArray::Provider(provider) => {
                if let Some(provider) = context.get_list_provider::<String>(&provider) {
                    Some(provider.iter().enumerate().for_each(|(i, option)| function(context, i, option)))
                } else {
                    None
                }
            }
        };
    }
}

#[cfg(test)]
pub mod tests {
    use element::widgets::radiobutton::*;
    use testutil::test_util::*;

    mod test_validation {
        use super::*;

        #[test]
        fn test_valid() {
            is_valid!(RadioButton, r#"(provider: "some_provider", options: ["one", "two", "three"])"#);
            is_valid!(RadioButton, r#"(provider: "some_provider:some_index", options: ["one", "two", "three"])"#);
            is_valid!(RadioButton, r#"(provider: "some_provider:some_index", options_provider: "some_provider")"#);
        }

        #[test]
        fn test_invalid_index_provider() {
            is_invalid!(RadioButton, r#"(provider: "some_provider", options_provider: "some_provider:some_index")"#, ValidationError::InvalidValue("options_provider", "a list provider"));
        }

        #[test]
        fn test_invalid_missing_provider() {
            is_invalid!(RadioButton, r#"(options: ["one", "two", "three"])"#, ValidationError::NotFound("provider"));
        }

        #[test]
        fn test_invalid_missing_options() {
            is_invalid!(RadioButton, r#"(provider: "some_provider:some_index")"#, ValidationError::NotFoundEither(r#""options_provider" or "options""#));
        }

        #[test]
        fn test_invalid_options_and_options_provider() {
            is_invalid!(RadioButton, r#"(provider: "some_provider", options: ["one", "two", "three"], options_provider: "some_other_provider")"#, ValidationError::MutuallyExclusive(r#""options_provider" and "options""#));
        }
    }
}