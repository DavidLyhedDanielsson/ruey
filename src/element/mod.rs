pub mod docking;
pub mod layouts;
pub mod utility;
pub mod widgets;

pub use context::Context;
pub use font::Font;
pub use guierror::GuiError;
pub use rect::Rect;
pub use ron::Value;
pub use std::collections::HashSet;
pub use vector2::Vector2;
pub use vertex::Vertex;
pub use GuiVariables;
pub use guierror::{ParseResult, ParseResultBuilder, MessageBuilder};
pub use parsing::{parser::Parser, parsehelper::*, ronparser::*, validationerror::ValidationError};

use guikey::GuiKey;

pub trait Element {
    fn validate(value: &Value) -> FieldValidator
    where
        Self: Sized,
    {
        FieldValidator::new(value)
    }
    fn parse(parser: &mut Parser, context: &mut Context, value: &Value, base: ElementBase) -> ParseResult
    where
        Self: Sized;
    fn build(&mut self, position: Vector2, width: Option<f32>, height: Option<f32>, context: &mut Context, _font: &Font);
    fn draw(&mut self, gui: &mut GuiVariables);
    fn get_providers(&self, _context: &mut HashSet<String>) {}
    fn box_clone(&self) -> Box<dyn Element>;
    fn key_down(&mut self, _context: &mut Context, _key: GuiKey) -> bool {
        false
    }
    fn key_up(&mut self, _context: &mut Context, _key: GuiKey) -> bool {
        false
    }
    fn key_repeat(&mut self, _context: &mut Context, _key: GuiKey) -> bool {
        false
    }
    fn mouse_down(&mut self, _context: &mut Context, _font: &Font, _x: i32, _y: i32, _button: i32) -> bool {
        false
    }
    fn mouse_up(&mut self, _context: &mut Context, _font: &Font, _x: i32, _y: i32, _button: i32) -> bool {
        false
    }
    fn text_input(&mut self, _context: &mut Context, _text: &str) -> bool {
        false
    }
    fn allow_unsized() -> bool
    where
        Self: Sized,
    {
        true
    }
    fn get_base(&self) -> &ElementBase;
    fn get_base_mut(&mut self) -> &mut ElementBase;
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Unit {
    Percent(f32),
    Units(f32),
    Undecided,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Size {
    pub width: Unit,
    pub height: Unit,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct ElementBase {
    pub position: Vector2,
    pub size: Vector2,
    pub parsed_size: Size,
}

impl Clone for Box<dyn Element> {
    fn clone(&self) -> Box<dyn Element> {
        self.box_clone()
    }
}

impl ElementBase {
    pub fn build(&mut self, position: Vector2, width: Option<f32>, height: Option<f32>) {
        self.position = position;
        if let Some(width) = width {
            self.size.x = width;
        }
        if let Some(height) = height {
            self.size.y = height;
        }
    }

    pub fn contains(&self, x: i32, y: i32) -> bool {
        x >= self.position.x as i32
            && x <= (self.position.x + self.size.x) as i32
            && y >= self.position.y as i32
            && y <= (self.position.y + self.size.y) as i32
    }

    pub fn get_area(&self) -> Rect {
        Rect::new_size_vector(self.position, self.size)
    }
}
