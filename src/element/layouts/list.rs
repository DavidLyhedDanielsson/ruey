use element::*;

use element::utility::scrollbar::Scrollbar;
use std::{cmp::max, collections::HashSet, vec::Vec};

extern crate stopwatch;

#[derive(Clone)]
pub struct List {
    base: ElementBase,
    children: Vec<Box<dyn Element>>,
    child_element: Box<dyn Element>,
    child_providers: HashSet<String>,
    scrollbar: Scrollbar,
    mouse_down_child: Option<usize>,
}

impl Element for List {
    fn validate(value: &Value) -> FieldValidator {
        FieldValidator::new(value).is_class("child_object")
    }

    fn parse(parser: &mut Parser, context: &mut Context, value: &Value, base: ElementBase) -> ParseResult {
        let mut parse_response = ParseResultBuilder::new(value);

        let child_value = value.expect("child_object");
        let child_response = parser.parse_element(context, child_value);
        let child_element = if let Some(child_element) = parse_response.with_messages_from(child_response) {
            child_element
        } else {
            return parse_response.build();
        };
        let mut child_providers = HashSet::<String>::new();
        child_element.get_providers(&mut child_providers);
        parse_response.with_element(List {
            base,
            children: Vec::<Box<dyn Element>>::new(),
            child_element,
            child_providers,
            scrollbar: Scrollbar::new(true),
            mouse_down_child: None,
        }).build()
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        _context: &mut Context,
        _font: &Font,
    ) {
        self.base.position = position;
        self.base.size.x = width.expect("A width was not given to a list layout");
        self.base.size.y = height.expect("A height was not given to a list layout");
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        gui.draw_list
            .push_clip_rect(Rect::new_size_vector(self.base.position, self.base.size));
        self.build_children(gui.context, gui.font);
        for child in &mut self.children {
            child.draw(gui);
        }
        self.scrollbar.draw(gui);
        gui.draw_list.pop_clip_rect();
    }

    fn get_providers(&self, context: &mut HashSet<String>) {
        for element in &self.children {
            element.get_providers(context);
        }
    }

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn mouse_down(&mut self, context: &mut Context, font: &Font, x: i32, y: i32, button: i32) -> bool {
        if !self.scrollbar.mouse_down(context, x, y, button) {
           for (i, ref mut child) in self.children.iter_mut().enumerate() {
               if child.get_base().contains(x, y) {
                   self.mouse_down_child = Some(i);
                   return child.mouse_down(context, font, x, y, button)
               }
           } 
            false
        } else {
            true
        }
    }

    fn mouse_up(&mut self, context: &mut Context, font: &Font, x: i32, y: i32, button: i32) -> bool {
        if let Some(index) = self.mouse_down_child {
            self.mouse_down_child = None;
            if let Some(child) = self.children.get_mut(index) {
                return child.mouse_up(context, font, x, y, button);
            }
            false
        } else {
            self.scrollbar.mouse_up(context, x, y, button)
        }
    }

    fn allow_unsized() -> bool {
        false
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}

impl List {
    fn build_children(&mut self, context: &mut Context, font: &Font) {
        let mut child_count = 0;
        for provider_name in &self.child_providers {
            context.reset_provider(provider_name);
            child_count = max(child_count, context.provider_len(provider_name));
        }

        self.children.resize(child_count, self.child_element.clone());
        let mut child_pos = self.base.position;
        child_pos.y -= self.scrollbar.get_min_range();
        let mut total_height: f32 = 0.0;

        for child in &mut self.children {
            let child_width;
            match child.get_base().parsed_size.width {
                Unit::Percent(percent) => child_width = Some(self.base.size.x * percent * 0.01),
                Unit::Units(unit) => child_width = Some(unit),
                Unit::Undecided => child_width = None,
            }
            let child_height;
            match child.get_base().parsed_size.height {
                Unit::Percent(percent) => child_height = Some(self.base.size.y * percent * 0.01),
                Unit::Units(unit) => child_height = Some(unit),
                Unit::Undecided => child_height = None,
            }
            child.build(child_pos, child_width, child_height, context, font);
            child_pos.y += child.get_base().size.y;
            total_height += child.get_base().size.y;
        }

        let mut scrollbar_position = self.base.position;
        scrollbar_position.x += self.base.size.x - 24.0;
        self.scrollbar.build(
            Rect::new_size(scrollbar_position.x, scrollbar_position.y, 24.0, self.base.size.y),
            total_height,
            self.base.size.y,
        );
    }
}
