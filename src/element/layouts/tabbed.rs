use element::docking::tab;
use element::layouts::switcher::Switcher;
use element::*;

#[derive(Clone)]
pub struct Tabbed {
    base: ElementBase,
    tab_names: Vec<String>,
    switcher: Switcher,
    mouse_down: bool,
}

impl Element for Tabbed {
    fn validate(value: &Value) -> FieldValidator {
        FieldValidator::new(value).is_valid(|value: &Value| {
            let child_values = match value.get_array("children") {
                Ok(child_values) => child_values,
                Err(err) => return Err(err)
            };
            for child in child_values {
                match child {
                    Value::Seq(seq) => {
                        match seq.get(0) {
                            Some(Value::String(_)) => {}
                            Some(_) | None => return Err(ValidationError::InvalidValue("First member in \"children\" array tuple", "string"))
                        };

                        match seq.get(1) {
                            Some(_) => {},
                            None => return Err(ValidationError::InvalidValue("Second member in \"children;\" array tuple", "string"))
                        };
                    }
                    _ => return Err(ValidationError::WrongType("children", "(name, element) tuples"))
                }
            }
            Ok(())
        })
    }

    fn parse(parser: &mut Parser, context: &mut Context, value: &Value, base: ElementBase) -> ParseResult {
        let mut parse_response = ParseResultBuilder::new(value);
        let mut tab_names = Vec::<String>::new();
        let mut switcher_tabs = Vec::<Box<dyn Element>>::new();
        let child_values = value.expect_array("children");
        for child in child_values {
            match child {
                Value::Seq(seq) => {
                    let name = match seq.get(0) {
                        Some(Value::String(text)) => {
                            text.to_string()
                        }
                        Some(_) | None => continue
                    };

                    let element = match seq.get(1) {
                        Some(element) => element,
                        None => continue
                    };

                    let child_response = parser.parse_element(context, element);
                    let child = match parse_response.with_messages_from(child_response) {
                        Some(child) => child,
                        None => continue,
                    };

                    tab_names.push(name);
                    switcher_tabs.push(child);
                }
                _ => {
                    parse_response.with_error(ValidationError::WrongType("children", "(name, element) tuples"));
                }
            }
        }

        parse_response.with_element(Tabbed {
            base,
            tab_names,
            // It doesn't really matter what the given ElementBase is, since the base isn't actually used anywhere
            switcher: Switcher::new_switcher(base, switcher_tabs, 0, None),
            mouse_down: false,
        }).build()
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        context: &mut Context,
        font: &Font,
    ) {
        self.base.build(position, width, height);
        let area = context.style.window.tab_element_area(self.base.get_area());
        self.switcher.build(area.min, Some(area.size().x), Some(area.size().y), context, font);
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        tab::draw_tabs(
            gui,
            self.base.get_area(),
            self.switcher.get_selected_index(),
            self.switcher.len(),
            |index| self.tab_names.get(index).unwrap(),
        );
        self.switcher.draw(gui);
    }

    fn get_providers(&self, context: &mut HashSet<String>) {
        self.switcher.get_providers(context);
    }

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn mouse_down(&mut self, context: &mut Context, font: &Font, x: i32, y: i32, button: i32) -> bool {
        self.mouse_down = false;
        if tab::tabs_area_contains(context, self.base.get_area(), x as f32, y as f32) {
            for (i, area) in tab::iter_tab_areas(context, self.base.get_area(), self.switcher.len()).enumerate() {
                if area.contains(x as f32, y as f32) {
                    self.switcher.set_selected_index(i);
                    return true;
                }
            }
            false
        } else {
            self.mouse_down = true;
            self.switcher.mouse_down(context, font, x, y, button)
        }
    }

    fn mouse_up(&mut self, context: &mut Context, font: &Font, x: i32, y: i32, button: i32) -> bool {
        if self.mouse_down {
            self.switcher.mouse_up(context, font, x, y, button)
        } else {
            false
        }
    }

    fn key_down(&mut self, context: &mut Context, key: GuiKey) -> bool {
        self.switcher.key_down(context, key)
    }

    fn key_up(&mut self, context: &mut Context, key: GuiKey) -> bool {
        self.switcher.key_up(context, key)
    }

    fn key_repeat(&mut self, context: &mut Context, key: GuiKey) -> bool {
        self.switcher.key_repeat(context, key)
    }

    fn text_input(&mut self, context: &mut Context, text: &str) -> bool {
        self.switcher.text_input(context, text)
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}
