use element::*;
use parsing::provider::Provider;

#[derive(Clone)]
pub struct Switcher {
    base: ElementBase,
    children: Vec<Box<dyn Element>>,
    selected_index: usize,
    provider: Option<Provider>,
    change_index: bool,
}

impl Element for Switcher {
    fn validate(value: &Value) -> FieldValidator {
        FieldValidator::new(value).is_valid(|value: &Value| {
            let child_values = match value.get_array("children") {
                Ok(child_values) => child_values,
                Err(err) => return Err(err)
            };
            for child in child_values {
                match child {
                    Value::Map(_) => {}
                    _ => return Err(ValidationError::WrongType("children", "an array of gui elements"))
                }
            }
            Ok(())
        }).is_provider("provider")
    }

    fn parse(parser: &mut Parser, context: &mut Context, value: &Value, base: ElementBase) -> ParseResult {
        let mut parse_response = ParseResultBuilder::new(value);
        let mut children = Vec::<Box<dyn Element>>::new();
        let child_values = value.expect_array("children");
        for child_value in child_values {
            let child_response = parser.parse_element(context, child_value);
            if let Some(child) = parse_response.with_messages_from(child_response) {
                children.push(child);
            }
        }

        let provider = if let Ok(provider) = value.get_provider("provider") {
            Some(provider)
        } else {
            None
        };

        parse_response.with_element(Switcher {
            base,
            children,
            selected_index: 0,
            provider,
            change_index: false,
        }).build()
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        context: &mut Context,
        font: &Font,
    ) {
        self.base.build(position, width, height);
        for child in &mut self.children {
            let child_width;
            match child.get_base().parsed_size.width {
                Unit::Percent(percent) => child_width = Some(self.base.size.x * percent * 0.01),
                Unit::Units(unit) => child_width = Some(unit),
                Unit::Undecided => child_width = None,
            }
            let child_height;
            match child.get_base().parsed_size.height {
                Unit::Percent(percent) => child_height = Some(self.base.size.y * percent * 0.01),
                Unit::Units(unit) => child_height = Some(unit),
                Unit::Undecided => child_height = None,
            }
            child.build(position, child_width, child_height, context, font);
        }
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        if let Some(provider) = &self.provider {
            unsafe {
                if let Some(provider_value) = provider.get_next_ref_mut::<i32>(gui.context) {
                    if self.change_index {
                        *provider_value = self.selected_index as i32;
                        self.change_index = false;
                    } else if *provider_value >= 0 {
                        self.selected_index = (*provider_value) as usize;
                    }
                }
            }
        }
        self.for_selected(|element| { element.draw(gui); false });
    }

    fn get_providers(&self, context: &mut HashSet<String>) {
        for element in &self.children {
            element.get_providers(context);
        }
    }

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn mouse_down(&mut self, context: &mut Context, font: &Font, x: i32, y: i32, button: i32) -> bool {
        self.for_selected(|element| element.mouse_down(context, font, x, y, button))
    }

    fn mouse_up(&mut self, context: &mut Context, font: &Font, x: i32, y: i32, button: i32) -> bool {
        self.for_selected(|element| element.mouse_up(context, font, x, y, button))
    }

    fn key_down(&mut self, context: &mut Context, key: GuiKey) -> bool {
        self.for_selected(|element| element.key_down(context, key))
    }

    fn key_up(&mut self, context: &mut Context, key: GuiKey) -> bool {
        self.for_selected(|element| element.key_up(context, key))
    }

    fn key_repeat(&mut self, context: &mut Context, key: GuiKey) -> bool {
        self.for_selected(|element| element.key_repeat(context, key))
    }

    fn text_input(&mut self, context: &mut Context, text: &str) -> bool {
        self.for_selected(|element| element.text_input(context, text))
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}

#[allow(clippy::len_without_is_empty)]
impl Switcher {
    pub fn new_switcher(base: ElementBase, children: Vec<Box<dyn Element>>, selected_index: usize, provider: Option<Provider>) -> Switcher {
        Switcher {
            base,
            children,
            selected_index,
            provider,
            change_index: false,
        }
    }

    pub fn set_selected_index(&mut self, new_index: usize) {
        self.selected_index = new_index;
        self.change_index = true;
    }

    pub fn get_selected_index(&self) -> usize {
        self.selected_index
    }

    pub fn len(&self) -> usize {
        self.children.len()
    }

    fn for_selected<F: FnMut(&mut Box<dyn Element>) -> bool>(&mut self, mut function: F) -> bool {
        if let Some(child) = self.children.get_mut(self.selected_index) {
            function(&mut *child)
        } else {
            false
        }
    }
}
