use element::*;

#[derive(Clone)]
pub enum Direction {
    Vertical,
    Horizontal,
}

#[derive(Clone)]
pub struct Linear {
    base: ElementBase,
    direction: Direction,
    children: Vec<Box<dyn Element>>,
    element_padding: i32,
}

fn validate_direction(value: &Value) -> Result<(), ValidationError> {
    match value.get_str("direction")? {
        "vertical" => Ok(()),
        "horizontal" =>  Ok(()),
        _ => Err(ValidationError::InvalidValue("direction", "either \"vertical\" or \"horizontal\"")),
    }
}

impl Element for Linear {
    fn validate(value: &Value) -> FieldValidator {
        FieldValidator::new(value)
            .is_valid(validate_direction)
            .is_array("children")
    }

    fn parse(parser: &mut Parser, context: &mut Context, value: &Value, base: ElementBase) -> ParseResult {
        let mut parse_response = ParseResultBuilder::new(value);
        let direction;
        match value.expect_str("direction") {
            "vertical" => direction = Direction::Vertical,
            "horizontal" => direction = Direction::Horizontal,
            _ => direction = Direction::Vertical, // Should not happen since it is validated
        }

        let empty_vec = Vec::<Value>::new();
        let arr = value.optional_array("children", &empty_vec);

        let mut children = Vec::<Box<dyn Element>>::with_capacity(arr.len());
        for element in arr {
            let child_response = parser.parse_element(context, &element);
            if let Some(child) = parse_response.with_messages_from(child_response) {
                children.push(child);
            }
        }

        parse_response.with_element(Linear {
            base,
            direction,
            children,
            element_padding: value.optional_i32("element_padding", 12),
        }).build()
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        context: &mut Context,
        font: &Font,
    ) {
        self.base.build(position, width, height);
        let mut max_width = 0.0f32;
        let mut max_height = 0.0f32;
        let mut child_pos = self.base.position;
        for child in &mut self.children {
            let child_width;
            match child.get_base().parsed_size.width {
                Unit::Percent(percent) => child_width = Some(self.base.size.x * percent * 0.01),
                Unit::Units(unit) => child_width = Some(unit),
                Unit::Undecided => child_width = None,
            }
            let child_height;
            match child.get_base().parsed_size.height {
                Unit::Percent(percent) => child_height = Some(self.base.size.y * percent * 0.01),
                Unit::Units(unit) => child_height = Some(unit),
                Unit::Undecided => child_height = None,
            }
            child.build(child_pos, child_width, child_height, context, font);
            match self.direction {
                Direction::Horizontal => child_pos.x += child.get_base().size.x + self.element_padding as f32,
                Direction::Vertical => child_pos.y += child.get_base().size.y + self.element_padding as f32,
            }
            max_width = max_width.max(child.get_base().size.x);
            max_height = max_height.max(child.get_base().size.y);
        }

        if width.is_none() {
            self.base.size.x = max_width;
        }
        if height.is_none() {
            self.base.size.y = max_height;
        }
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        for child in &mut self.children {
            child.draw(gui);
        }
    }

    fn get_providers(&self, context: &mut HashSet<String>) {
        for element in &self.children {
            element.get_providers(context);
        }
    }

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn mouse_down(&mut self, context: &mut Context, font: &Font, x: i32, y: i32, button: i32) -> bool {
        for child in &mut self.children {
            if child.mouse_down(context, font, x, y, button) {
                return true;
            }
        }
        false
    }

    fn mouse_up(&mut self, context: &mut Context, font: &Font, x: i32, y: i32, button: i32) -> bool {
        for child in &mut self.children {
            if child.mouse_up(context, font, x, y, button) {
                return true;
            }
        }
        false
    }

    fn key_down(&mut self, context: &mut Context, key: GuiKey) -> bool {
        for child in &mut self.children {
            if child.key_down(context, key) {
                return true;
            }
        }
        false
    }

    fn key_up(&mut self, context: &mut Context, key: GuiKey) -> bool {
        for child in &mut self.children {
            if child.key_up(context, key) {
                return true;
            }
        }
        false
    }

    fn key_repeat(&mut self, context: &mut Context, key: GuiKey) -> bool {
        for child in &mut self.children {
            if child.key_repeat(context, key) {
                return true;
            }
        }
        false
    }

    fn text_input(&mut self, context: &mut Context, text: &str) -> bool {
        for child in &mut self.children {
            if child.text_input(context, text) {
                return true;
            }
        }
        false
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}

impl Linear {
    pub fn take_child(&mut self, i: usize) -> Box<dyn Element> {
        self.children.remove(i)
    }

    pub fn push(&mut self, child: Box<dyn Element>) {
        self.children.push(child);
    }

    pub fn set_direction(&mut self, direction: Direction) {
        self.direction = direction;
    }
}
