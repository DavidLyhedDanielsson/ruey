use element::docking::{DockedElement, Zone};
use element::layouts::dockable::Dockable;
use element::*;
use parsing::provider::Provider;

#[derive(Clone)]
pub enum ParsedSize {
    Unit(f32),
    Provider(Provider),
    None
}

#[derive(Clone)]
pub struct Window {
    pub position: Vector2,
    pub size: Vector2,
    pub min_size: Vector2,
    pub max_size: Vector2,
    pub layout: Dockable,
    pub allow_resize: bool,
    pub allow_move: bool,
    pub parsed_width: ParsedSize,
    pub parsed_height: ParsedSize,
}

fn parse_window_sizing(value: &Value) -> (ParsedSize, ParsedSize) {
    let parsed_width = if let Ok(provider) = value.get_provider("width") {
        ParsedSize::Provider(provider)
    } else if let Ok(width_value) = value.get_i32("width") {
        ParsedSize::Unit(width_value as f32)
    } else {
        ParsedSize::None
    };

    let parsed_height = if let Ok(provider) = value.get_provider("height") {
        ParsedSize::Provider(provider)
    } else if let Ok(height_value) = value.get_i32("height") {
        ParsedSize::Unit(height_value as f32)
    } else {
        ParsedSize::None
    };

    (parsed_width, parsed_height)
}

impl Window {
    pub fn get_area(&self) -> Rect {
        Rect::new_size_vector(self.position, self.size)
    }

    pub fn validate(value: &Value) -> FieldValidator {
        FieldValidator::new(value)
            .is_class("child")
            .is_str("title")
    }

    pub fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        context: &mut Context,
        font: &Font,
    ) {
        let width = match &self.parsed_width {
            ParsedSize::Unit(width) => *width,
            ParsedSize::Provider(provider) => provider.get_next::<f32>(context).unwrap_or(32.0),
            ParsedSize::None => width.unwrap_or(32.0)
        };
        let height = match &self.parsed_height {
            ParsedSize::Unit(height) => *height,
            ParsedSize::Provider(provider) => provider.get_next::<f32>(context).unwrap_or(32.0),
            ParsedSize::None => height.unwrap_or(32.0)
        };

        self.size.x = width.max(self.min_size.x).min(self.max_size.x);
        self.size.y = height.max(self.min_size.y).min(self.max_size.y);
        self.position = position;

        self.build_child(context, font);
    }

    pub fn draw(&mut self, gui: &mut GuiVariables) {
        gui.draw_list
            .push_clip_rect(Rect::new_size_vector(self.position, self.size));

        self.layout.draw(gui);
        gui.draw_list.pop_clip_rect();
    }

    pub fn mouse_down(&mut self, context: &mut Context, font: &Font, x: i32, y: i32, button: i32) -> bool {
        self.layout.mouse_down(context, font, x, y, button)
    }

    pub fn mouse_up(&mut self, context: &mut Context, font: &Font, x: i32, y: i32, button: i32) -> bool {
        self.layout.mouse_up(context, font, x, y, button)
    }

    pub fn key_down(&mut self, context: &mut Context, key: GuiKey) -> bool {
        self.layout.key_down(context, key)
    }

    pub fn key_up(&mut self, context: &mut Context, key: GuiKey) -> bool {
        self.layout.key_up(context, key)
    }

    pub fn key_repeat(&mut self, context: &mut Context, key: GuiKey) -> bool {
        self.layout.key_repeat(context, key)
    }

    pub fn text_input(&mut self, context: &mut Context, text: &str) -> bool {
        self.layout.text_input(context, text)
    }

    pub fn new_window(
        parser: &mut Parser,
        context: &mut Context,
        value: &Value,
        position: Vector2,
        size: Vector2,
        width_provider: Option<String>,
        height_provider: Option<String>,
        mut parse_response: ParseResultBuilder
    ) -> (Option<Self>, ParseResult) {
        let child_value = value.expect("child");
        let child_response = parser.parse_element(context, child_value);
        let child = match parse_response.with_messages_from(child_response) {
            Some(child) => child,
            None => return (None, parse_response.build()),
        };
        let child_base = ElementBase {
            position: Vector2::new_zero(),
            size: Vector2::new_zero(),
            parsed_size: Size {
                width: Unit::Undecided,
                height: Unit::Undecided,
            },
        };
        let title = value.expect_str("title").to_string();
        let child = Zone::Element(DockedElement {
            title,
            element: child,
        });
        let position = value.optional_vector2("position", Vector2::new_zero());
        let layout = Dockable::new_dockable(child_base, child);
        let (parsed_width, parsed_height) = parse_window_sizing(value);
        (Some(Window {
            position,
            size,
            min_size: value.optional_vector2("min_size", Vector2::new(24.0, 24.0)),
            max_size: value.optional_vector2("max_size", Vector2::new_max()),
            layout,
            allow_resize: value.optional_bool("allow_resize", true),
            allow_move: value.optional_bool("allow_move", true),
            parsed_width,
            parsed_height,
        }),
        parse_response.build())
    }

    pub fn rebuild(&mut self, context: &mut Context, font: &Font) {
        if let ParsedSize::Provider(provider) = &self.parsed_width {
            if let Some(provider_value) = provider.get_next::<f32>(context) {
                self.size.x = provider_value;
            }
        }
        if let ParsedSize::Provider(provider) = &self.parsed_height {
            if let Some(provider_value) = provider.get_next::<f32>(context) {
                self.size.y = provider_value;
            }
        }
        self.build_child(context, font);
    }

    pub fn get_undocked_element(
        &mut self,
        context: &mut Context,
        font: &Font,
        mouse_x: i32,
        mouse_y: i32,
    ) -> Option<Zone> {
        let undocked_element = self.get_dockable().get_undocked_element(context, mouse_x, mouse_y);
        if undocked_element.is_some() {
            self.build_child(context, font);
        }
        undocked_element
    }

    pub fn dock(
        &mut self,
        context: &mut Context,
        font: &Font,
        dockable: &mut Dockable,
        x_pos: i32,
        y_pos: i32,
    ) -> bool {
        let return_value = self.layout.dock(context, dockable, x_pos, y_pos);
        self.build_child(context, font);
        return_value
    }

    fn build_child(&mut self, context: &mut Context, font: &Font) {
        self.layout.build(
            self.position,
            Some(self.size.x),
            Some(self.size.y),
            context,
            font,
        );
    }

    pub fn allow_move(&self) -> bool {
        self.allow_move
    }

    pub fn allow_resize(&self) -> bool {
        self.allow_resize
    }

    pub fn draw_docking_ui(&mut self, gui: &mut GuiVariables) {
        self.layout.draw_docking_ui(gui);
    }

    pub fn get_dockable(&mut self) -> &mut Dockable {
        &mut self.layout
    }
}