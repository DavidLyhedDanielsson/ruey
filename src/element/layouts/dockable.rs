use element::docking::{
    split,
    split::{AddAndResize, IterSplits, IterSplitsMut, Split, SplitZone},
    tab, Zone,
};
use element::*;
use font::{HorizontalOrigin, VerticalOrigin};
use rect::Edge;

#[derive(Clone)]
pub struct Dockable {
    base: ElementBase,
    zone: Zone,
    hover_side: Option<Edge>,
    resizing: bool,
    resize_area: Rect,
    resize_offset: Option<f32>,
    grabbed_title_position: Option<Vector2>,
    title_size: f32,
    mouse_down_position: Option<Vector2>,
}

impl Element for Dockable {
    fn parse(_parser: &mut Parser, _context: &mut Context, _value: &Value, _base: ElementBase) -> ParseResult {
        panic!("Dockable should not be created by parsing; use Dockable::new_dockable");
    }

    fn build(
        &mut self,
        position: Vector2,
        width: Option<f32>,
        height: Option<f32>,
        context: &mut Context,
        font: &Font,
    ) {
        self.base.position = position;
        self.base.size = Vector2::new(width.unwrap(), height.unwrap());
        self.title_size = if self.is_element() {
            context.style.window.title_bar_height()
        } else {
            0.0
        };

        let area = Rect::new_size(
            position.x,
            position.y + self.title_size,
            width.unwrap(),
            height.unwrap() - self.title_size,
        );
        Dockable::build_rec(&mut self.zone, area, context, font);
    }

    fn draw(&mut self, gui: &mut GuiVariables) {
        self.title_size = if self.is_element() {
            gui.context.style.window.title_bar_height()
        } else {
            0.0
        };
        if self.resizing {
            let area = self.base.get_area();
            let mut zone = Zone::Empty;
            std::mem::swap(&mut zone, &mut self.zone);
            self.resize_rec(gui.context, area, &mut zone, gui.mouse_x as f32, gui.mouse_y as f32);
            std::mem::swap(&mut zone, &mut self.zone);
            self.build(
                self.base.position,
                Some(self.base.size.x),
                Some(self.base.size.y),
                &mut gui.context,
                &gui.font,
            );
        } else {
            self.hover_side = None;
            self.resize_offset = None;
            self.resize_area = Rect::new_zero();
        }

        if let Zone::Element(docked_element) = &self.zone {
            gui.draw_list
                .push_colored_quad(self.base.get_area(), gui.context.style.window.title_bar_color);
            gui.font.create_text_positioned(
                &docked_element.title,
                gui.context.style.window.title_bar_text_color,
                &mut gui.draw_list.vertices,
                &mut gui.draw_list.indices,
                self.get_base().get_area().min + gui.context.style.window.title_text_offset(),
                HorizontalOrigin::Left,
                VerticalOrigin::Top,
                1.0,
            );
        }

        let area = Rect::new_size_vector(self.base.position, self.base.size);
        let mut element = Zone::Empty;
        std::mem::swap(&mut self.zone, &mut element);
        self.draw_rec(&mut element, area, gui);
        std::mem::swap(&mut self.zone, &mut element);

        if let Some(side) = self.hover_side {
            if !self.resize_area.almost_equal(self.base.get_area()) {
                let area = match side {
                    Edge::Top => Rect::new_size(
                        self.resize_area.min.x,
                        self.resize_area.min.y - 3.0,
                        self.resize_area.size().x,
                        7.0,
                    ),
                    Edge::Bottom => Rect::new_size(
                        self.resize_area.min.x,
                        self.resize_area.max.y - 3.0,
                        self.resize_area.size().x,
                        7.0,
                    ),
                    Edge::Left => Rect::new_size(
                        self.resize_area.min.x - 3.0,
                        self.resize_area.min.y,
                        7.0,
                        self.resize_area.size().y,
                    ),
                    Edge::Right => Rect::new_size(
                        self.resize_area.max.x - 3.0,
                        self.resize_area.min.y,
                        7.0,
                        self.resize_area.size().y,
                    ),
                };
                gui.draw_list
                    .push_colored_quad(area, gui.context.style.window.resize_highlight_color);
            } else {
                self.hover_side = None
            }
        }
    }

    fn get_providers(&self, _context: &mut HashSet<String>) {}

    fn box_clone(&self) -> Box<dyn Element> {
        Box::new(self.clone())
    }

    fn mouse_down(&mut self, context: &mut Context, font: &Font, x: i32, y: i32, button: i32) -> bool {
        if button == 1 && self.hover_side.is_some() {
            self.resizing = true;
            true
        } else {
            let mut element = Zone::Empty;
            std::mem::swap(&mut self.zone, &mut element);
            let return_value = self.mouse_down_rec(context, font, true, &mut element, self.base.get_area(), x, y, button);
            std::mem::swap(&mut self.zone, &mut element);
            return_value
        }
    }

    fn mouse_up(&mut self, context: &mut Context, font: &Font, x: i32, y: i32, button: i32) -> bool {
        let mouse_down_position = self.mouse_down_position;
        self.mouse_down_position = None;

        if self.grabbed_title_position.is_some() {
            self.grabbed_title_position = None;
            return true;
        }
        if self.resizing {
            self.resizing = false;
        } else if mouse_down_position.is_some() {
            let mut element = Zone::Empty;
            std::mem::swap(&mut self.zone, &mut element);
            self.mouse_up_rec(context, font, &mut element, self.base.get_area(), x as f32, y as f32, button);
            std::mem::swap(&mut self.zone, &mut element);
            return true;
        }
        false
    }

    fn get_base(&self) -> &ElementBase {
        &self.base
    }
    fn get_base_mut(&mut self) -> &mut ElementBase {
        &mut self.base
    }
}

impl Dockable {
    pub fn get_undocked_element(&mut self, context: &Context, mouse_x: i32, mouse_y: i32) -> Option<Zone> {
        if let Some(position) = self.grabbed_title_position {
            let dist = position - Vector2::new(mouse_x as f32, mouse_y as f32);
            let dist = dist.x * dist.x + dist.y * dist.y;
            if dist > 15.0 {
                let mut zone = Zone::Empty;
                std::mem::swap(&mut zone, &mut self.zone);
                let (zone, undocked_element) = self.get_undocked_element_rec(context, zone, self.base.get_area());
                self.grabbed_title_position = None;
                self.zone = zone;
                undocked_element
            } else {
                None
            }
        } else {
            None
        }
    }

    fn get_undocked_element_rec(&mut self, context: &Context, mut zone: Zone, area: Rect) -> (Zone, Option<Zone>) {
        let grabbed_position = self.grabbed_title_position.unwrap();
        match zone {
            Zone::Split(direction, ref mut splits) => {
                let mut position = area.min;
                for i in 0..splits.len() {
                    let split = splits.remove(i);
                    let size = if direction == split::Direction::Vertical {
                        Vector2::new(area.size().x, area.size().y * split.size)
                    } else {
                        Vector2::new(area.size().x * split.size, area.size().y)
                    };

                    let split_area = Rect::new_size_vector(position, size);
                    if split_area.contains(grabbed_position.x, grabbed_position.y) {
                        if let Zone::Element(docked_element) = split.zone {
                            if splits.len() == 1 {
                                let last_element = splits.remove(0).zone;
                                return (last_element, Some(Zone::Element(docked_element)));
                            } else {
                                split::set_sizes_uniformly(splits);
                                return (zone, Some(Zone::Element(docked_element)));
                            }
                        } else {
                            let (element, docked_element) =
                                self.get_undocked_element_rec(context, split.zone, split_area);
                            splits.insert(
                                i,
                                Split {
                                    size: split.size,
                                    zone: element,
                                },
                            );
                            return (zone, docked_element);
                        }
                    } else {
                        splits.insert(i, split);
                    }
                    if direction == split::Direction::Vertical {
                        position.y += size.y;
                    } else {
                        position.x += size.x;
                    }
                }
                (zone, None)
            }
            Zone::Tabbed(ref mut index, ref mut tabs) => {
                if tab::tabs_area_contains(context, area, grabbed_position.x, grabbed_position.y) {
                    for tab in tab::iter_tab_areas(context, area, tabs.len()) {
                        if tab.contains(grabbed_position.x, grabbed_position.y) {
                            if tabs.len() == 2 {
                                let element = tabs.remove(*index);
                                let remaining_element = tabs.remove(0);
                                return (remaining_element, Some(element));
                            } else {
                                let element = tabs.remove(*index);
                                *index = (*index).min(tabs.len() - 1);
                                return (zone, Some(element));
                            }
                        }
                    }
                } else {
                    let element_area = context.style.window.tab_element_area(area);
                    let element = tabs.remove(*index);
                    match element {
                        Zone::Element(docked_element) => {
                            if tabs.len() == 1 {
                                let remaining_element = tabs.remove(0);
                                return (remaining_element, Some(Zone::Element(docked_element)));
                            } else {
                                *index = (*index).min(tabs.len() - 1);
                                return (zone, Some(Zone::Element(docked_element)));
                            }
                        }
                        _ => {
                            let (element, docked_element) =
                                self.get_undocked_element_rec(context, element, element_area);
                            tabs.insert(*index, element);
                            return (zone, docked_element);
                        }
                    }
                }
                (zone, None) // Only here as a safety
            }
            _ => (zone, None),
        }
    }

    fn draw_rec(&mut self, zone: &mut Zone, area: Rect, gui: &mut GuiVariables) {
        match zone {
            Zone::Split(direction, splits) => {
                if self.hover_side.is_none() {
                    if let Some((distance, side)) = area.get_closest_edge(gui.mouse_x as f32, gui.mouse_y as f32) {
                        if distance < 5.0 {
                            self.hover_side = Some(side);
                            self.resize_area = area;
                        }
                    }
                }

                for split in splits.iter_splits_mut(area, *direction) {
                    if let Zone::Element(docked_element) = &split.split.zone {
                        let element_area = gui
                            .context
                            .style
                            .window
                            .unpad_unborder_area(docked_element.element.get_base().get_area());
                        let min = element_area.min;
                        let max = element_area.max;
                        let size = element_area.size();
                        let border_size = gui.context.style.window.border_thickness;

                        // Top/bottom
                        gui.draw_list.push_colored_quad(
                            Rect::new_size(min.x, min.y, size.x, border_size),
                            gui.context.style.window.split_border_color,
                        );
                        gui.draw_list.push_colored_quad(
                            Rect::new_size(min.x, max.y - border_size, size.x, border_size),
                            gui.context.style.window.split_border_color,
                        );
                        // Left/right
                        gui.draw_list.push_colored_quad(
                            Rect::new_size(min.x, min.y, border_size, size.y),
                            gui.context.style.window.split_border_color,
                        );
                        gui.draw_list.push_colored_quad(
                            Rect::new_size(max.x - border_size, min.y, border_size, size.y),
                            gui.context.style.window.split_border_color,
                        );

                        gui.draw_list.push_colored_quad(
                            Rect::new_size(
                                split.area.min.x,
                                split.area.min.y,
                                split.area.size().x,
                                gui.context.style.window.title_bar_height(),
                            ),
                            gui.context.style.window.split_border_color,
                        );
                        gui.font.create_text_positioned(
                            &docked_element.title,
                            gui.context.style.window.title_bar_text_color,
                            &mut gui.draw_list.vertices,
                            &mut gui.draw_list.indices,
                            split.area.min + gui.context.style.window.title_text_offset(),
                            HorizontalOrigin::Left,
                            VerticalOrigin::Top,
                            1.0,
                        );
                    }

                    self.draw_rec(&mut split.split.zone, split.area, gui);

                    if self.hover_side.is_none() {
                        if let Some((distance, edge)) =
                            split.area.get_closest_edge(gui.mouse_x as f32, gui.mouse_y as f32)
                        {
                            if distance < 5.0 {
                                self.hover_side = Some(edge);
                                self.resize_area = split.area;
                            }
                        }
                    }
                }
            }
            Zone::Tabbed(index, tabs) => {
                gui.draw_list
                    .push_colored_quad(area, gui.context.style.window.split_border_color);
                tab::draw_tabs(
                    gui,
                    gui.context.style.window.tab_tabs_area(area),
                    *index,
                    tabs.len(),
                    |i| {
                        if let Zone::Element(docked_element) = tabs.get(i).unwrap() {
                            &docked_element.title
                        } else {
                            "Tab"
                        }
                    },
                );
                self.draw_rec(
                    &mut tabs.get_mut(*index).unwrap(),
                    gui.context.style.window.tab_element_area(area),
                    gui,
                );
            }
            Zone::Element(docked_element) => {
                let element_background_area = gui
                    .context
                    .style
                    .window
                    .unpad_area(docked_element.element.get_base().get_area());
                gui.draw_list.push_colored_quad(
                    element_background_area,
                    gui.context.style.window.element_background_color,
                );
                docked_element.element.draw(gui);
            }
            Zone::Empty => {}
        }
    }

    pub fn draw_docking_ui(&mut self, gui: &mut GuiVariables) {
        let area = self.base.get_area();
        gui.draw_list
            .push_colored_quad(area, gui.context.style.window.window_docking_color);

        let x = gui.mouse_x as f32;
        let y = gui.mouse_y as f32;

        let north_zone = gui.context.style.window.dock_north_zone(area);
        let south_zone = gui.context.style.window.dock_south_zone(area);
        let west_zone = gui.context.style.window.dock_west_zone(area);
        let east_zone = gui.context.style.window.dock_east_zone(area);

        if north_zone.contains(x, y) {
            gui.draw_list
                .push_colored_quad(north_zone, gui.context.style.window.focused_docking_zone_color);
        } else if south_zone.contains(x, y) {
            gui.draw_list
                .push_colored_quad(south_zone, gui.context.style.window.focused_docking_zone_color);
        } else if west_zone.contains(x, y) {
            gui.draw_list
                .push_colored_quad(west_zone, gui.context.style.window.focused_docking_zone_color);
        } else if east_zone.contains(x, y) {
            gui.draw_list
                .push_colored_quad(east_zone, gui.context.style.window.focused_docking_zone_color);
        } else {
            Dockable::draw_docking_ui_rec(area, &mut self.zone, gui);
        }
    }

    fn draw_docking_ui_rec(area: Rect, zone: &mut Zone, gui: &mut GuiVariables) {
        match zone {
            Zone::Split(direction, splits) => {
                for split in splits.iter_splits_mut(area, *direction) {
                    if split.area.contains(gui.mouse_x as f32, gui.mouse_y as f32) {
                        Dockable::draw_docking_ui_rec(split.area, &mut split.split.zone, gui);
                        return;
                    }
                }
            }
            Zone::Tabbed(ref mut index, tabs) => {
                for (i, tab_area) in tab::iter_tab_areas(gui.context, area, tabs.len()).enumerate() {
                    if tab_area.contains(gui.mouse_x as f32, gui.mouse_y as f32) {
                        let left_area = gui.context.style.window.tab_left_area(tab_area, i);
                        if left_area.contains(gui.mouse_x as f32, gui.mouse_y as f32) {
                            gui.draw_list
                                .push_colored_quad(left_area, gui.context.style.window.focused_docking_zone_color);
                        } else {
                            let right_area = gui.context.style.window.tab_right_area(tab_area, i, tabs.len() - 1);
                            gui.draw_list
                                .push_colored_quad(right_area, gui.context.style.window.focused_docking_zone_color);
                        }
                        *index = i;
                        break;
                    }
                }
                let element = tabs.get_mut(*index).unwrap();
                Dockable::draw_docking_ui_rec(gui.context.style.window.tab_element_area(area), element, gui);
            }
            Zone::Element(docked_element) => {
                let title_area = gui.context.style.window.tab_tabs_area(area);
                if title_area.contains(gui.mouse_x as f32, gui.mouse_y as f32) {
                    let left_area = gui.context.style.window.tab_left_area(title_area, 0);
                    if left_area.contains(gui.mouse_x as f32, gui.mouse_y as f32) {
                        gui.draw_list
                            .push_colored_quad(left_area, gui.context.style.window.focused_docking_zone_color);
                    } else {
                        let right_area = gui.context.style.window.tab_right_area(title_area, 0, 0);
                        gui.draw_list
                            .push_colored_quad(right_area, gui.context.style.window.focused_docking_zone_color);
                    }
                } else {
                    let element_area = docked_element.element.get_base().get_area();
                    if element_area.contains(gui.mouse_x as f32, gui.mouse_y as f32) {
                        split::draw_split(element_area, gui);
                    }
                }
            }
            Zone::Empty => {}
        }
    }

    fn resize_rec(&mut self, context: &mut Context, area: Rect, zone: &mut Zone, x: f32, y: f32) {
        match zone {
            Zone::Split(direction, splits) => {
                let mut resize_data = None;
                for (i, split) in splits.iter_splits(area, *direction).enumerate() {
                    if split.area.almost_equal(self.resize_area) {
                        resize_data = Some((i, split.area));
                        break;
                    } else if split.area.contains_or_equal(self.resize_area) {
                        self.resize_rec(context, split.area, &mut splits.get_mut(i).unwrap().zone, x, y);
                        break;
                    }
                }

                if let Some(resize_data) = resize_data {
                    let i = resize_data.0;
                    let split_area = resize_data.1;
                    match self.hover_side.unwrap() {
                        Edge::Left => {
                            let percent_diff = self.get_percent_diff(area.size().x, x, split_area.min.x);
                            let (previous, current) = splits.split_at_mut(i);
                            split::clamp_sizes(&mut current[0], &mut previous[i - 1], percent_diff);

                            let new_width = area.size().x * splits.get(i).unwrap().size;
                            let new_min_position = Vector2::new(split_area.max.x - new_width, split_area.min.y);
                            self.resize_area =
                                Rect::new_size(new_min_position.x, new_min_position.y, new_width, split_area.size().y);
                        }
                        Edge::Right => {
                            let percent_diff = self.get_percent_diff(area.size().x, split_area.max.x, x);
                            let (current, next) = splits.split_at_mut(i + 1);
                            split::clamp_sizes(&mut current[i], &mut next[0], percent_diff);

                            let new_width = area.size().x * splits.get(i).unwrap().size;
                            self.resize_area =
                                Rect::new_size(split_area.min.x, split_area.min.y, new_width, split_area.size().y);
                        }
                        Edge::Top => {
                            let percent_diff = self.get_percent_diff(area.size().y, y, split_area.min.y);
                            let (previous, current) = splits.split_at_mut(i);
                            split::clamp_sizes(&mut current[0], &mut previous[i - 1], percent_diff);

                            let new_height = area.size().y * splits.get(i).unwrap().size;
                            let new_min_position = Vector2::new(split_area.min.x, split_area.max.y - new_height);
                            self.resize_area =
                                Rect::new_size(new_min_position.x, new_min_position.y, split_area.size().x, new_height);
                        }
                        Edge::Bottom => {
                            let percent_diff = self.get_percent_diff(area.size().y, split_area.max.y, y);
                            let (current, next) = splits.split_at_mut(i + 1);
                            split::clamp_sizes(&mut current[i], &mut next[0], percent_diff);

                            let new_height = area.size().y * splits.get(i).unwrap().size;
                            self.resize_area =
                                Rect::new_size(split_area.min.x, split_area.min.y, split_area.size().x, new_height);
                        }
                    }
                }
            }
            Zone::Tabbed(index, elements) => {
                self.resize_rec(
                    context,
                    context.style.window.tab_element_area(area),
                    elements.get_mut(*index).unwrap(),
                    x,
                    y,
                );
            }
            _ => {}
        }
    }

    fn mouse_up_rec(&mut self, context: &mut Context, font: &Font, zone: &mut Zone, area: Rect, x: f32, y: f32, button: i32) {
        match zone {
            Zone::Split(direction, splits) => {
                for split in splits.iter_splits_mut(area, *direction) {
                    if split.area.contains(x, y) {
                        return self.mouse_up_rec(context, font, &mut split.split.zone, split.area, x, y, button);
                    }
                }
            }
            Zone::Tabbed(ref mut index, elements) => {
                if !tab::tabs_area_contains(context, area, x, y) {
                    unsafe {
                        let element = elements.get_unchecked_mut(*index);
                        self.mouse_up_rec(
                            context,
                            font,
                            element,
                            context.style.window.tab_element_area(area),
                            x,
                            y,
                            button,
                        )
                    }
                }
            }
            Zone::Element(docked_element) => {
                docked_element.element.mouse_up(context, font, x as i32, y as i32, button);
                self.mouse_down_position = None;
            }
            _ => {}
        }
    }

    #[allow(clippy::too_many_arguments)] // I know, I know... TODO
    fn mouse_down_rec(
        &mut self,
        context: &mut Context,
        font: &Font,
        first_element: bool,
        zone: &mut Zone,
        area: Rect,
        x: i32,
        y: i32,
        button: i32,
    ) -> bool {
        match zone {
            Zone::Split(direction, splits) => {
                for split in splits.iter_splits_mut(area, *direction) {
                    if split.area.contains(x as f32, y as f32) {
                        return self.mouse_down_rec(context, font, false, &mut split.split.zone, split.area, x, y, button);
                    }
                }
                false
            }
            Zone::Tabbed(ref mut index, elements) => {
                if tab::tabs_area_contains(context, area, x as f32, y as f32) {
                    for (i, tab_area) in tab::iter_tab_areas(context, area, elements.len()).enumerate() {
                        if tab_area.contains(x as f32, y as f32) {
                            *index = i;
                            if button == 3 {
                                self.grabbed_title_position = Some(Vector2::new(x as f32, y as f32));
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }

                unsafe {
                    let element = elements.get_unchecked_mut(*index);
                    self.mouse_down_rec(
                        context,
                        font,
                        false,
                        element,
                        context.style.window.tab_element_area(area),
                        x,
                        y,
                        button,
                    )
                }
            }
            Zone::Element(docked_element) => {
                if context
                    .style
                    .window
                    .unpad_unborder_area(docked_element.element.get_base().get_area())
                    .contains(x as f32, y as f32)
                {
                    self.mouse_down_position = Some(Vector2::new(x as f32, y as f32));
                    let event_handeled = docked_element.element.mouse_down(context, font, x, y, button);
                    if !event_handeled && button == 3 {
                        false
                    } else {
                        event_handeled
                    }
                } else {
                    // If the element area does not contain the cursor, the titlebar does
                    if button == 1 {
                        false
                    } else if button == 3 {
                        if first_element {
                            false
                        } else {
                            self.grabbed_title_position = Some(Vector2::new(x as f32, y as f32));
                            true
                        }
                    } else {
                        false
                    }
                }
            }
            _ => false,
        }
    }

    pub fn new_dockable(base: ElementBase, zone: Zone) -> Self {
        Dockable {
            base,
            zone,
            hover_side: None,
            resizing: false,
            resize_area: Rect::new_zero(),
            resize_offset: None,
            grabbed_title_position: None,
            title_size: 0.0,
            mouse_down_position: None,
        }
    }

    fn build_rec(zone: &mut Zone, area: Rect, context: &mut Context, font: &Font) {
        match zone {
            Zone::Split(direction, splits) => {
                for split in splits.iter_splits_mut(area, *direction) {
                    match &mut split.split.zone {
                        Zone::Element(ref mut docked_element) => {
                            let element_area = context.style.window.split_element_area(split.area);
                            docked_element.element.build(
                                element_area.min,
                                Some(element_area.size().x),
                                Some(element_area.size().y),
                                context,
                                font,
                            )
                        }
                        _ => Dockable::build_rec(&mut split.split.zone, split.area, context, font),
                    }
                }
            }
            Zone::Tabbed(_, ref mut elements) => {
                let element_area = context.style.window.tab_element_area(area);
                for element in elements {
                    match element {
                        Zone::Element(ref mut docked_element) => {
                            let element_area = context.style.window.border_pad_area(element_area);
                            docked_element.element.build(
                                element_area.min,
                                Some(element_area.size().x),
                                Some(element_area.size().y),
                                context,
                                font,
                            )
                        }
                        _ => Dockable::build_rec(element, element_area, context, font),
                    }
                }
            }
            Zone::Element(docked_element) => {
                let element_area = context.style.window.border_pad_area(area);
                docked_element.element.build(
                    element_area.min,
                    Some(element_area.size().x),
                    Some(element_area.size().y),
                    context,
                    font,
                );
            }
            Zone::Empty => {}
        }
    }

    pub fn dock(&mut self, context: &Context, dockable: &mut Dockable, x_pos: i32, y_pos: i32) -> bool {
        let area = self.base.get_area();

        let x = x_pos as f32;
        let y = y_pos as f32;

        let north_zone = context.style.window.dock_north_zone(area);
        let south_zone = context.style.window.dock_south_zone(area);
        let west_zone = context.style.window.dock_west_zone(area);
        let east_zone = context.style.window.dock_east_zone(area);

        let mut success = true;
        if north_zone.contains(x, y) {
            self.replace_element(|zone| split::create_split(split::Direction::Vertical, dockable.consume(), zone));
        } else if south_zone.contains(x, y) {
            self.replace_element(|zone| split::create_split(split::Direction::Vertical, zone, dockable.consume()));
        } else if west_zone.contains(x, y) {
            self.replace_element(|zone| split::create_split(split::Direction::Horizontal, dockable.consume(), zone));
        } else if east_zone.contains(x, y) {
            self.replace_element(|zone| split::create_split(split::Direction::Horizontal, zone, dockable.consume()));
        } else {
            let element = self.consume();
            let (new_element, modified) = self.dock_rec(context, element, dockable, area, x_pos, y_pos);
            self.zone = new_element;
            success = modified;
        }
        success
    }

    fn dock_rec(
        &self,
        context: &Context,
        zone: Zone,
        dockable: &mut Dockable,
        area: Rect,
        x_pos: i32,
        y_pos: i32,
    ) -> (Zone, bool) {
        let x = x_pos as f32;
        let y = y_pos as f32;

        match zone {
            Zone::Split(direction, mut splits) => {
                let mut modified = false;
                for i in 0..splits.len() {
                    if !modified {
                        let split = splits.get_mut(i).unwrap();
                        if let Zone::Element(docked_element) = &split.zone {
                            let base = docked_element.element.get_base();
                            let split_title_area = Rect::new_size(
                                base.position.x,
                                base.position.y - context.style.window.title_bar_height(),
                                base.size.x,
                                context.style.window.title_bar_height(),
                            );
                            let element_area = base.get_area();

                            if split_title_area.contains(x, y) {
                                modified = true;
                                splits.modify_zone_at(i, |zone| tab::create_tab(1, zone, dockable.consume()));
                            } else if element_area.contains(x, y) {
                                modified = true;
                                match split::get_split_zone(element_area, x, y) {
                                    SplitZone::Top => {
                                        if direction == split::Direction::Vertical {
                                            splits.insert_and_resize_zone(i, dockable.consume());
                                        } else {
                                            splits.modify_zone_at(i, |zone| {
                                                split::create_two_split(
                                                    split::Direction::Vertical,
                                                    dockable.consume(),
                                                    zone,
                                                )
                                            });
                                        }
                                    }
                                    SplitZone::Bottom => {
                                        if direction == split::Direction::Vertical {
                                            splits.insert_and_resize_zone(i + 1, dockable.consume());
                                        } else {
                                            splits.modify_zone_at(i, |zone| {
                                                split::create_two_split(
                                                    split::Direction::Vertical,
                                                    zone,
                                                    dockable.consume(),
                                                )
                                            })
                                        }
                                    }
                                    SplitZone::Left => {
                                        if direction == split::Direction::Vertical {
                                            splits.modify_zone_at(i, |zone| {
                                                split::create_two_split(
                                                    split::Direction::Horizontal,
                                                    dockable.consume(),
                                                    zone,
                                                )
                                            });
                                        } else {
                                            splits.insert_and_resize(i, Split::new(0.0, dockable.consume()));
                                        }
                                    }
                                    SplitZone::Right => {
                                        if direction == split::Direction::Vertical {
                                            splits.modify_zone_at(i, |zone| {
                                                split::create_two_split(
                                                    split::Direction::Horizontal,
                                                    zone,
                                                    dockable.consume(),
                                                )
                                            });
                                        } else {
                                            splits.insert_and_resize_zone(i + 1, dockable.consume());
                                        }
                                    }
                                    SplitZone::Center => {
                                        splits.modify_zone_at(i, |zone| tab::create_tab(1, zone, dockable.consume()));
                                    }
                                }
                            }
                        } else {
                            let split_area = split::get_nth_split_area(area, direction, &splits, i);
                            if split_area.contains(x, y) {
                                let split = splits.remove(i);
                                let (new_zone, was_modified) =
                                    self.dock_rec(context, split.zone, dockable, split_area, x_pos, y_pos);
                                splits.insert(i, Split::new(split.size, new_zone));
                                modified = was_modified;
                            }
                        }
                    }
                }

                (Zone::Split(direction, splits), modified)
            }
            Zone::Tabbed(index, mut tabs) => {
                if tab::tabs_area_contains(context, area, x, y) {
                    for (i, tab_area) in tab::iter_tab_areas(context, area, tabs.len()).enumerate() {
                        if tab_area.contains(x, y) {
                            let left_area = if i == 0 {
                                Rect::new_size_vector(tab_area.min, tab_area.size() * Vector2::new(0.5, 1.0))
                            } else {
                                Rect::new_size_vector(
                                    tab_area.min - Vector2::new(tab_area.size().x * 0.5, 0.0),
                                    tab_area.size(),
                                )
                            };
                            if left_area.contains(x, y) {
                                tabs.insert(i, dockable.consume());
                                return (Zone::Tabbed(i, tabs), true);
                            } else {
                                tabs.insert(i + 1, dockable.consume());
                                return (Zone::Tabbed(i + 1, tabs), true);
                            }
                        }
                    }
                    (Zone::Tabbed(index, tabs), false) // Should not happen
                } else {
                    let element_area = context.style.window.tab_element_area(area);
                    let old_element = tabs.remove(index);
                    let (new_zone, was_modified) =
                        self.dock_rec(context, old_element, dockable, element_area, x_pos, y_pos);
                    tabs.insert(index, new_zone);
                    (Zone::Tabbed(index, tabs), was_modified)
                }
            }
            Zone::Element(docked_element) => {
                let x = x_pos as f32;
                let y = y_pos as f32;

                let title_area = Rect::new_size(
                    area.min.x,
                    area.min.y,
                    area.size().x,
                    context.style.window.title_bar_height(),
                );
                let element_area = docked_element.element.get_base().get_area();

                if title_area.contains(x, y) {
                    let left_area = context.style.window.tab_left_area(title_area, 0);
                    if left_area.contains(x, y) {
                        (
                            tab::create_tab(0, dockable.consume(), Zone::Element(docked_element)),
                            true,
                        )
                    } else {
                        (
                            tab::create_tab(1, Zone::Element(docked_element), dockable.consume()),
                            true,
                        )
                    }
                } else {
                    match split::get_split_zone(element_area, x, y) {
                        SplitZone::Top => (
                            split::create_two_split(
                                split::Direction::Vertical,
                                dockable.consume(),
                                Zone::Element(docked_element),
                            ),
                            true,
                        ),
                        SplitZone::Bottom => (
                            split::create_two_split(
                                split::Direction::Vertical,
                                Zone::Element(docked_element),
                                dockable.consume(),
                            ),
                            true,
                        ),
                        SplitZone::Left => (
                            split::create_two_split(
                                split::Direction::Horizontal,
                                dockable.consume(),
                                Zone::Element(docked_element),
                            ),
                            true,
                        ),
                        SplitZone::Right => (
                            split::create_two_split(
                                split::Direction::Horizontal,
                                Zone::Element(docked_element),
                                dockable.consume(),
                            ),
                            true,
                        ),
                        SplitZone::Center => (
                            tab::create_tab(1, Zone::Element(docked_element), dockable.consume()),
                            true,
                        ),
                    }
                }
            }
            Zone::Empty => (Zone::Empty, false),
        }
    }

    pub fn consume(&mut self) -> Zone {
        let mut zone = Zone::Empty;
        std::mem::swap(&mut self.zone, &mut zone);
        zone
    }

    pub fn replace_element<F>(&mut self, func: F)
    where
        F: FnOnce(Zone) -> Zone,
    {
        let mut self_element = Zone::Empty;
        std::mem::swap(&mut self.zone, &mut self_element);
        self.zone = func(self_element);
    }

    fn get_percent_diff(&mut self, area_size: f32, min_position: f32, max_position: f32) -> f32 {
        let percent_per_pixel = 1.0 / area_size;
        let mut pixel_distance = min_position - max_position;
        if self.resize_offset.is_none() {
            self.resize_offset = Some(pixel_distance);
        }
        pixel_distance -= self.resize_offset.unwrap();
        pixel_distance * percent_per_pixel
    }

    pub fn get_title(&self) -> Option<String> {
        match &self.zone {
            Zone::Element(element) => Some(element.title.clone()),
            _ => None,
        }
    }

    pub fn is_element(&self) -> bool {
        match self.zone {
            Zone::Element(_) => true,
            _ => false,
        }
    }
}
