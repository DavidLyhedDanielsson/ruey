pub mod split;
pub mod tab;

pub use self::split::{Direction, Split};
pub use element::Element;

#[derive(Clone)]
pub struct DockedElement {
    pub title: String,
    pub element: Box<dyn Element>,
}

impl std::fmt::Debug for DockedElement {
    fn fmt(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "DockedElement {{ title: {}, element: ... }}", self.title)
    }
}

#[derive(Debug, Clone)]
pub enum Zone {
    Split(split::Direction, Vec<Split>),
    Tabbed(usize, Vec<Zone>),
    Element(DockedElement),
    Empty,
}
