use element::docking::Zone;
use element::GuiVariables;
use rect::{Edge, Rect};
use vector2::Vector2;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Direction {
    Vertical,
    Horizontal,
}

#[derive(Debug, Clone)]
pub struct Split {
    pub size: f32,
    pub zone: Zone,
}

impl Split {
    pub fn new(size: f32, zone: Zone) -> Self {
        Split { size, zone }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum SplitZone {
    Top = 0,
    Bottom,
    Left,
    Right,
    Center,
}

const SPLIT_ZONE_OFFSETS: [Vector2; 5] = [
    Vector2 { x: 0.0, y: 0.0 },
    Vector2 { x: 0.0, y: 0.5 },
    Vector2 { x: 0.0, y: 0.0 },
    Vector2 { x: 0.5, y: 0.0 },
    Vector2 { x: 0.25, y: 0.25 },
];

const SPLIT_ZONE_SIZES: [Vector2; 5] = [
    Vector2 { x: 1.0, y: 0.5 },
    Vector2 { x: 1.0, y: 0.5 },
    Vector2 { x: 0.5, y: 1.0 },
    Vector2 { x: 0.5, y: 1.0 },
    Vector2 { x: 0.5, y: 0.5 },
];

const MIN_SPLIT_SIZE: f32 = 0.0512;

pub fn zone_contains(area: Rect, split_zone: SplitZone, x: f32, y: f32) -> bool {
    get_split_zone_area(area, split_zone).contains(x, y)
}

pub fn set_sizes_uniformly(splits: &mut Vec<Split>) {
    let size = 1.0 / (splits.len() as f32);
    for split in splits.iter_mut() {
        split.size = size;
    }
}

pub fn clamp_sizes(active_split: &mut Split, other_split: &mut Split, percent_diff: f32) {
    let total_size = active_split.size + other_split.size;
    if other_split.size + percent_diff < MIN_SPLIT_SIZE {
        active_split.size = total_size - MIN_SPLIT_SIZE;
        other_split.size = MIN_SPLIT_SIZE;
    } else if active_split.size - percent_diff < MIN_SPLIT_SIZE {
        active_split.size = MIN_SPLIT_SIZE;
        other_split.size = total_size - MIN_SPLIT_SIZE;
    } else {
        active_split.size -= percent_diff;
        other_split.size += percent_diff;
    }
}

pub fn draw_split(area: Rect, gui: &mut GuiVariables) {
    let x = gui.mouse_x as f32;
    let y = gui.mouse_y as f32;

    let zone = get_split_zone(area, x, y);
    let split_area = get_split_zone_area(area, zone);

    gui.draw_list
        .push_colored_quad(split_area, gui.context.style.window.focused_docking_zone_color);
}

pub fn get_split_zone(area: Rect, x: f32, y: f32) -> SplitZone {
    let mut distance_from_center = Vector2::new(x, y) - (area.min + area.size() * Vector2::new(0.5, 0.5));
    distance_from_center.x = distance_from_center.x.abs();
    distance_from_center.y = distance_from_center.y.abs();

    if distance_from_center.x <= area.size().x * SPLIT_ZONE_SIZES[SplitZone::Center as usize].x * 0.5
        && distance_from_center.y <= area.size().y * SPLIT_ZONE_SIZES[SplitZone::Center as usize].y * 0.5
    {
        SplitZone::Center
    } else {
        match area.get_closest_edge_triangle(x, y).unwrap() {
            Edge::Top => SplitZone::Top,
            Edge::Bottom => SplitZone::Bottom,
            Edge::Left => SplitZone::Left,
            Edge::Right => SplitZone::Right,
        }
    }
}

fn get_split_zone_area(area: Rect, split_zone: SplitZone) -> Rect {
    let offset = SPLIT_ZONE_OFFSETS[split_zone as usize];
    let size_mult = SPLIT_ZONE_SIZES[split_zone as usize];

    Rect::new_size_vector(area.min + area.size() * offset, area.size() * size_mult)
}

#[allow(clippy::collapsible_if)]
pub fn create_split(direction: Direction, first: Zone, last: Zone) -> Zone {
    // There are three main cases to handle: Creating a split from...
    // - Two splits
    // - One element and one split
    // - Two elements
    if let Zone::Split(first_direction, mut splits) = first {
        if let Zone::Split(last_direction, mut last_splits) = last {
            // Two splits
            if first_direction == direction {
                if last_direction == direction {
                    splits.append_and_resize(&mut last_splits);
                    Zone::Split(direction, splits)
                } else {
                    splits.push_and_resize_zone(Zone::Split(last_direction, last_splits));
                    Zone::Split(direction, splits)
                }
            } else {
                if last_direction == direction {
                    last_splits.insert_and_resize_zone(0, Zone::Split(first_direction, splits));
                    Zone::Split(direction, last_splits)
                } else {
                    create_two_split(
                        direction,
                        Zone::Split(first_direction, splits),
                        Zone::Split(first_direction, last_splits),
                    )
                }
            }
        } else {
            // Split + element
            if first_direction == direction {
                splits.push_and_resize_zone(last);
                Zone::Split(direction, splits)
            } else {
                create_two_split(direction, Zone::Split(first_direction, splits), last)
            }
        }
    } else if let Zone::Split(last_direction, mut last_splits) = last {
        // Element + split
        if last_direction == direction {
            last_splits.insert_and_resize_zone(0, first);
            Zone::Split(direction, last_splits)
        } else {
            create_two_split(direction, first, Zone::Split(last_direction, last_splits))
        }
    } else {
        // Two elements
        create_two_split(direction, first, last)
    }
}

pub fn create_two_split(direction: Direction, first: Zone, last: Zone) -> Zone {
    Zone::Split(direction, vec![Split::new(0.5, first), Split::new(0.5, last)])
}

pub fn get_nth_split_area(area: Rect, direction: Direction, splits: &[Split], n: usize) -> Rect {
    let mut position = area.min;
    let mut size = if direction == Direction::Horizontal {
        Vector2::new(area.size().x * splits.get(0).unwrap().size, area.size().y)
    } else {
        Vector2::new(area.size().x, area.size().y * splits.get(0).unwrap().size)
    };
    for i in 1..(n + 1).min(splits.len()) {
        if direction == Direction::Horizontal {
            position.x += size.x;
            size = Vector2::new(area.size().x * splits.get(i).unwrap().size, area.size().y);
        } else {
            position.y += size.y;
            size = Vector2::new(area.size().x, area.size().y * splits.get(i).unwrap().size);
        };
    }
    Rect::new_size_vector(position, size)
}

pub trait AddAndResize<T> {
    fn append_and_resize(&mut self, other: &mut Vec<T>);
    fn push_and_resize(&mut self, element: T);
    fn push_and_resize_zone(&mut self, zone: Zone);
    fn insert_and_resize(&mut self, index: usize, element: T);
    fn insert_and_resize_zone(&mut self, index: usize, zone: Zone);

    fn modify_zone_at<F>(&mut self, index: usize, f: F)
    where
        F: FnOnce(Zone) -> Zone;
}

impl AddAndResize<Split> for Vec<Split> {
    fn append_and_resize(&mut self, other: &mut Vec<Split>) {
        self.append(other);
        set_sizes_uniformly(self);
    }
    fn push_and_resize(&mut self, element: Split) {
        self.push(element);
        set_sizes_uniformly(self);
    }
    fn push_and_resize_zone(&mut self, zone: Zone) {
        self.push(Split::new(0.0, zone));
        set_sizes_uniformly(self);
    }
    fn insert_and_resize(&mut self, index: usize, element: Split) {
        self.insert(index, element);
        set_sizes_uniformly(self);
    }
    fn insert_and_resize_zone(&mut self, index: usize, zone: Zone) {
        self.insert(index, Split::new(0.0, zone));
        set_sizes_uniformly(self);
    }

    fn modify_zone_at<F>(&mut self, index: usize, f: F)
    where
        F: FnOnce(Zone) -> Zone,
    {
        let mut split = self.get_mut(index).unwrap();
        let mut temp = Zone::Empty;
        std::mem::swap(&mut temp, &mut split.zone);
        split.zone = f(temp);
    }
}

pub struct PositionedSplit<'a> {
    pub area: Rect,
    pub split: &'a Split,
}
pub struct PositionedSplitMut<'a> {
    pub area: Rect,
    pub split: &'a mut Split,
}

pub struct SplitIter<'a> {
    direction: Direction,
    position: Vector2,
    area: Rect,
    iter: std::slice::Iter<'a, Split>,
}
pub struct SplitIterMut<'a> {
    direction: Direction,
    position: Vector2,
    area: Rect,
    iter: std::slice::IterMut<'a, Split>,
}

pub trait IterSplitsMut {
    fn iter_splits_mut(&mut self, area: Rect, direction: Direction) -> SplitIterMut;
}

pub trait IterSplits {
    fn iter_splits(&self, area: Rect, direction: Direction) -> SplitIter;
}

impl IterSplitsMut for Vec<Split> {
    fn iter_splits_mut(&mut self, area: Rect, direction: Direction) -> SplitIterMut {
        SplitIterMut {
            direction,
            position: area.min,
            area,
            iter: self.iter_mut(),
        }
    }
}
impl IterSplits for Vec<Split> {
    fn iter_splits(&self, area: Rect, direction: Direction) -> SplitIter {
        SplitIter {
            direction,
            position: area.min,
            area,
            iter: self.iter(),
        }
    }
}

impl<'a> Iterator for SplitIterMut<'a> {
    type Item = PositionedSplitMut<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        if let Some(split) = self.iter.next() {
            let size = if self.direction == Direction::Horizontal {
                Vector2::new(self.area.size().x * split.size, self.area.size().y)
            } else {
                Vector2::new(self.area.size().x, self.area.size().y * split.size)
            };
            let area = Rect::new_size_vector(self.position, size);
            if self.direction == Direction::Horizontal {
                self.position.x += size.x;
            } else {
                self.position.y += size.y;
            };
            Some(PositionedSplitMut { area, split })
        } else {
            None
        }
    }
}

impl<'a> Iterator for SplitIter<'a> {
    type Item = PositionedSplit<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        if let Some(split) = self.iter.next() {
            let size = if self.direction == Direction::Horizontal {
                Vector2::new(self.area.size().x * split.size, self.area.size().y)
            } else {
                Vector2::new(self.area.size().x, self.area.size().y * split.size)
            };
            let area = Rect::new_size_vector(self.position, size);
            if self.direction == Direction::Horizontal {
                self.position.x += size.x;
            } else {
                self.position.y += size.y;
            };
            Some(PositionedSplit { area, split })
        } else {
            None
        }
    }
}

#[allow(non_upper_case_globals)]
#[cfg(test)]
mod tests {
    use self::Direction::{Horizontal, Vertical};
    use element::docking::split::*;
    use lazy_static::*;
    use testutil::test_util::*;

    #[allow(clippy::float_cmp)]
    #[test]
    fn test_set_sizes_uniformly() {
        let mut splits = vec![
            Split::new(0.77, Zone::Empty),
            Split::new(124.55, Zone::Empty),
            Split::new(-32.1, Zone::Empty),
        ];
        set_sizes_uniformly(&mut splits);
        splits
            .iter()
            .for_each(|split| assert_eq!(split.size, 1.0 / splits.len() as f32))
    }

    #[allow(clippy::float_cmp)]
    mod test_clamp_sizes {
        use super::*;

        #[test]
        fn test_positive_change() {
            let mut active_split = Split::new(0.5, Zone::Empty);
            let mut other_split = Split::new(0.5, Zone::Empty);

            clamp_sizes(&mut active_split, &mut other_split, 0.25);
            assert_eq!(active_split.size, 0.5 - 0.25, "active_split is not resized correctly");
            assert_eq!(other_split.size, 0.5 + 0.25, "other_split is not resized correctly");
        }
        #[test]
        fn test_negative_change() {
            let mut active_split = Split::new(0.5, Zone::Empty);
            let mut other_split = Split::new(0.5, Zone::Empty);

            clamp_sizes(&mut active_split, &mut other_split, -0.25);
            assert_eq!(active_split.size, 0.5 + 0.25, "active_split is not resized correctly");
            assert_eq!(other_split.size, 0.5 - 0.25, "other_split is not resized correctly");
        }
        #[test]
        fn test_max_clamp() {
            let mut active_split = Split::new(0.5, Zone::Empty);
            let mut other_split = Split::new(0.5, Zone::Empty);

            clamp_sizes(&mut active_split, &mut other_split, -0.5);
            assert_eq!(active_split.size, 1.0 - MIN_SPLIT_SIZE, "active_split is not clamped");
            assert_eq!(other_split.size, MIN_SPLIT_SIZE, "other_split is not clamped");
        }
        #[test]
        fn test_min_clamp() {
            let mut active_split = Split::new(0.5, Zone::Empty);
            let mut other_split = Split::new(0.5, Zone::Empty);

            clamp_sizes(&mut active_split, &mut other_split, 1.0);
            assert_eq!(active_split.size, MIN_SPLIT_SIZE, "active_split is not clamped");
            assert_eq!(other_split.size, 1.0 - MIN_SPLIT_SIZE, "other_split is not clamped");
        }
    }

    mod test_get_split_zone {
        use super::*;

        lazy_static! {
            static ref center_radius: Vector2 =
                area.size() * SPLIT_ZONE_SIZES[SplitZone::Center as usize] * Vector2::new(0.5, 0.5);
            static ref top_center: Vector2 = Vector2::new(area_center.x, area_center.y - center_radius.y);
            static ref bottom_center: Vector2 = Vector2::new(area_center.x, area_center.y + center_radius.y);
            static ref left_center: Vector2 = Vector2::new(area_center.x - center_radius.x, area_center.y);
            static ref right_center: Vector2 = Vector2::new(area_center.x + center_radius.x, area_center.y);
        }

        #[test]
        fn test_top() {
            assert_eq!(
                get_split_zone(*area, area_center.x, top_center.y - 1.0),
                SplitZone::Top,
                "Zone is off at the center"
            );
            assert_eq!(
                get_split_zone(*area, area_center.x, area.min.y),
                SplitZone::Top,
                "Zone is off at the edge"
            );
            assert_eq!(
                get_split_zone(*area, area.min.x + 1.0, area.min.y),
                SplitZone::Top,
                "Zone is off at the top-left"
            );
            assert_eq!(
                get_split_zone(*area, area.max.x - 1.0, area.min.y),
                SplitZone::Top,
                "Zone is off at the top-right"
            );
        }
        #[test]
        fn test_bottom() {
            assert_eq!(
                get_split_zone(*area, area_center.x, bottom_center.y + 1.0),
                SplitZone::Bottom,
                "Zone is off at the center"
            );
            assert_eq!(
                get_split_zone(*area, area_center.x, area.max.y),
                SplitZone::Bottom,
                "Zone is off at the edge"
            );
            assert_eq!(
                get_split_zone(*area, area.min.x + 1.0, area.max.y),
                SplitZone::Bottom,
                "Zone is off at the bottom-left"
            );
            assert_eq!(
                get_split_zone(*area, area.max.x - 1.0, area.max.y),
                SplitZone::Bottom,
                "Zone is off at the bottom-right"
            );
        }
        #[test]
        fn test_left() {
            assert_eq!(
                get_split_zone(*area, left_center.x - 1.0, area_center.y),
                SplitZone::Left,
                "Zone is off at the center"
            );
            assert_eq!(
                get_split_zone(*area, area.min.x, area_center.y),
                SplitZone::Left,
                "Zone is off at the edge"
            );
            assert_eq!(
                get_split_zone(*area, area.min.x, area.min.y + 1.0),
                SplitZone::Left,
                "Zone is off at the top-left"
            );
            assert_eq!(
                get_split_zone(*area, area.min.x, area.max.y - 1.0),
                SplitZone::Left,
                "Zone is off at the bottom-left"
            );
        }
        #[test]
        fn test_right() {
            assert_eq!(
                get_split_zone(*area, right_center.x + 1.0, area_center.y),
                SplitZone::Right,
                "Zone is off at the center"
            );
            assert_eq!(
                get_split_zone(*area, area.max.x, area_center.y),
                SplitZone::Right,
                "Zone is off at the edge"
            );
            assert_eq!(
                get_split_zone(*area, area.max.x, area.min.y + 1.0),
                SplitZone::Right,
                "Zone is off at the top-right"
            );
            assert_eq!(
                get_split_zone(*area, area.max.x, area.max.y - 1.0),
                SplitZone::Right,
                "Zone is off at the bottom-right"
            );
        }
        #[test]
        fn test_center() {
            assert_eq!(
                get_split_zone(*area, area_center.x, area_center.y),
                SplitZone::Center,
                "Center point is not in zone"
            );
            assert_eq!(
                get_split_zone(*area, top_center.x, top_center.y),
                SplitZone::Center,
                "Top center point is not in zone"
            );
            assert_eq!(
                get_split_zone(*area, bottom_center.x, bottom_center.y),
                SplitZone::Center,
                "Bottom center point is not in zone"
            );
            assert_eq!(
                get_split_zone(*area, left_center.x, left_center.y),
                SplitZone::Center,
                "Left center point is not in zone"
            );
            assert_eq!(
                get_split_zone(*area, right_center.x, right_center.y),
                SplitZone::Center,
                "Right center point is not in zone"
            );
        }
        #[should_panic(expected = "called `Option::unwrap()` on a `None` value")]
        #[test]
        fn test_outside() {
            get_split_zone(*area, 0.0, 0.0);
        }
    }

    #[allow(clippy::float_cmp)]
    mod test_create_split {
        use super::*;

        // Split + split
        #[test]
        fn test_vsplit_vertical_vertical() {
            let expected = vec![
                Split::new(0.25, zone_element(uuids[0])),
                Split::new(0.25, zone_element(uuids[1])),
                Split::new(0.25, zone_element(uuids[2])),
                Split::new(0.25, zone_element(uuids[3])),
            ];
            let (_, splits) = unpack_split(create_split(
                Vertical,
                vertical_split(uuids[0], uuids[1]),
                vertical_split(uuids[2], uuids[3]),
            ));
            assert_splits(&splits, &expected);
        }
        #[test]
        fn test_vsplit_horizontal_horizontal() {
            let expected = create_two_split(
                Direction::Vertical,
                horizontal_split(uuids[0], uuids[1]),
                horizontal_split(uuids[2], uuids[3]),
            );
            let split = create_split(
                Vertical,
                horizontal_split(uuids[0], uuids[1]),
                horizontal_split(uuids[2], uuids[3]),
            );
            assert_zones(&split, &expected);
        }
        #[test]
        fn test_hsplit_vertical_vertical() {
            let expected = create_two_split(
                Direction::Horizontal,
                vertical_split(uuids[0], uuids[1]),
                vertical_split(uuids[2], uuids[3]),
            );
            let split = create_split(
                Horizontal,
                vertical_split(uuids[0], uuids[1]),
                vertical_split(uuids[2], uuids[3]),
            );
            assert_zones(&split, &expected);
        }
        #[test]
        fn test_hsplit_horizontal_horizontal() {
            let expected = vec![
                Split::new(0.25, zone_element(uuids[0])),
                Split::new(0.25, zone_element(uuids[1])),
                Split::new(0.25, zone_element(uuids[2])),
                Split::new(0.25, zone_element(uuids[3])),
            ];
            let (_, splits) = unpack_split(create_split(
                Horizontal,
                horizontal_split(uuids[0], uuids[1]),
                horizontal_split(uuids[2], uuids[3]),
            ));
            assert_splits(&splits, &expected);
        }
        // Split + element
        #[test]
        fn test_vsplit_vertical_element() {
            let expected = vec![
                Split::new(0.33, zone_element(uuids[0])),
                Split::new(0.33, zone_element(uuids[1])),
                Split::new(0.33, zone_element(uuids[2])),
            ];
            let (_, splits) = unpack_split(create_split(
                Vertical,
                vertical_split(uuids[0], uuids[1]),
                zone_element(uuids[2]),
            ));
            assert_splits(&splits, &expected);
        }
        #[test]
        fn test_vsplit_horizontal_element() {
            let expected = create_two_split(
                Direction::Vertical,
                horizontal_split(uuids[0], uuids[1]),
                zone_element(uuids[2]),
            );
            let split = create_split(Vertical, horizontal_split(uuids[0], uuids[1]), zone_element(uuids[2]));
            assert_zones(&split, &expected);
        }
        #[test]
        fn test_hsplit_vertical_element() {
            let expected = create_two_split(
                Direction::Horizontal,
                vertical_split(uuids[0], uuids[1]),
                zone_element(uuids[2]),
            );
            let split = create_split(Horizontal, vertical_split(uuids[0], uuids[1]), zone_element(uuids[2]));
            assert_zones(&split, &expected);
        }
        #[test]
        fn test_hsplit_horizontal_element() {
            let expected = vec![
                Split::new(0.33, zone_element(uuids[0])),
                Split::new(0.33, zone_element(uuids[1])),
                Split::new(0.33, zone_element(uuids[2])),
            ];
            let (_, splits) = unpack_split(create_split(
                Horizontal,
                horizontal_split(uuids[0], uuids[1]),
                zone_element(uuids[2]),
            ));
            assert_splits(&splits, &expected);
        }
        // Element + split
        #[test]
        fn test_vsplit_element_vertical() {
            let expected = vec![
                Split::new(0.33, zone_element(uuids[0])),
                Split::new(0.33, zone_element(uuids[1])),
                Split::new(0.33, zone_element(uuids[2])),
            ];
            let (_, splits) = unpack_split(create_split(
                Vertical,
                zone_element(uuids[0]),
                vertical_split(uuids[1], uuids[2]),
            ));
            assert_splits(&splits, &expected);
        }
        #[test]
        fn test_vsplit_element_horizontal() {
            let expected = create_two_split(
                Direction::Vertical,
                zone_element(uuids[0]),
                horizontal_split(uuids[1], uuids[2]),
            );
            let split = create_split(Vertical, zone_element(uuids[0]), horizontal_split(uuids[1], uuids[2]));
            assert_zones(&split, &expected);
        }
        #[test]
        fn test_hsplit_element_vertical() {
            let expected = create_two_split(
                Direction::Horizontal,
                zone_element(uuids[0]),
                vertical_split(uuids[1], uuids[2]),
            );
            let split = create_split(Horizontal, zone_element(uuids[0]), vertical_split(uuids[1], uuids[2]));
            assert_zones(&split, &expected);
        }
        #[test]
        fn test_hsplit_element_horizontal() {
            let expected = vec![
                Split::new(0.33, zone_element(uuids[0])),
                Split::new(0.33, zone_element(uuids[1])),
                Split::new(0.33, zone_element(uuids[2])),
            ];
            let (_, splits) = unpack_split(create_split(
                Horizontal,
                zone_element(uuids[0]),
                horizontal_split(uuids[1], uuids[2]),
            ));
            assert_splits(&splits, &expected);
        }
        // Element + element
        #[test]
        fn test_vsplit_element_element() {
            let expected = vertical_split(uuids[0], uuids[1]);
            let splits = create_split(Vertical, zone_element(uuids[0]), zone_element(uuids[1]));
            assert_zones(&splits, &expected);
        }
        #[test]
        fn test_hsplit_element_element() {
            let expected = horizontal_split(uuids[0], uuids[1]);
            let splits = create_split(Horizontal, zone_element(uuids[0]), zone_element(uuids[1]));
            assert_zones(&splits, &expected);
        }
    }

    #[test]
    fn test_create_two_split() {
        let expected = Zone::Split(
            Vertical,
            vec![
                Split::new(0.5, zone_element(uuids[0])),
                Split::new(0.5, zone_element(uuids[1])),
            ],
        );
        let split = create_two_split(Vertical, zone_element(uuids[0]), zone_element(uuids[1]));
        assert_zones(&split, &expected);
    }

    mod test_get_nth_split_area {
        use super::*;

        #[test]
        fn test_vertical() {
            let splits = vec![
                Split::new(0.1, Zone::Empty),
                Split::new(0.1, Zone::Empty),
                Split::new(0.5, Zone::Empty),
                Split::new(0.3, Zone::Empty),
            ];
            let first_split_area = get_nth_split_area(*area, Vertical, &splits, 0);
            let second_split_area = get_nth_split_area(*area, Vertical, &splits, 1);
            let third_split_area = get_nth_split_area(*area, Vertical, &splits, 2);
            let fourth_split_area = get_nth_split_area(*area, Vertical, &splits, 3);

            assert_eq!(
                first_split_area,
                Rect::new_size((*area).min.x, (*area).min.y, (*area).size().x, (*area).size().y * 0.1)
            );
            assert_eq!(
                second_split_area,
                Rect::new_size(
                    (*area).min.x,
                    (*area).min.y + (*area).size().y * 0.1,
                    (*area).size().x,
                    (*area).size().y * 0.1
                )
            );
            assert_eq!(
                third_split_area,
                Rect::new_size(
                    (*area).min.x,
                    (*area).min.y + (*area).size().y * (0.1 + 0.1),
                    (*area).size().x,
                    (*area).size().y * 0.5
                )
            );
            assert_eq!(
                fourth_split_area,
                Rect::new_size(
                    (*area).min.x,
                    (*area).min.y + (*area).size().y * (0.1 + 0.1 + 0.5),
                    (*area).size().x,
                    (*area).size().y * 0.3
                )
            );
        }
        #[test]
        fn test_horizontal() {
            let splits = vec![
                Split::new(0.1, Zone::Empty),
                Split::new(0.1, Zone::Empty),
                Split::new(0.5, Zone::Empty),
                Split::new(0.3, Zone::Empty),
            ];
            let first_split_area = get_nth_split_area(*area, Horizontal, &splits, 0);
            let second_split_area = get_nth_split_area(*area, Horizontal, &splits, 1);
            let third_split_area = get_nth_split_area(*area, Horizontal, &splits, 2);
            let fourth_split_area = get_nth_split_area(*area, Horizontal, &splits, 3);

            assert_eq!(
                first_split_area,
                Rect::new_size((*area).min.x, (*area).min.y, (*area).size().x * 0.1, (*area).size().y)
            );
            assert_eq!(
                second_split_area,
                Rect::new_size(
                    (*area).min.x + (*area).size().x * 0.1,
                    (*area).min.y,
                    (*area).size().x * 0.1,
                    (*area).size().y
                )
            );
            assert_eq!(
                third_split_area,
                Rect::new_size(
                    (*area).min.x + (*area).size().x * (0.1 + 0.1),
                    (*area).min.y,
                    (*area).size().x * 0.5,
                    (*area).size().y
                )
            );
            assert_eq!(
                fourth_split_area,
                Rect::new_size(
                    (*area).min.x + (*area).size().x * (0.1 + 0.1 + 0.5),
                    (*area).min.y,
                    (*area).size().x * 0.3,
                    (*area).size().y
                )
            );
        }
    }

    mod test_iter {
        use super::*;

        #[test]
        fn test_splits_vertical() {
            let mut splits = vec![
                Split::new(0.2, Zone::Empty),
                Split::new(0.4, Zone::Empty),
                Split::new(0.3, Zone::Empty),
                Split::new(0.1, Zone::Empty),
            ];

            let areas = vec![
                Rect::new_size((*area).min.x, (*area).min.y, (*area).size().x, (*area).size().y * 0.2),
                Rect::new_size(
                    (*area).min.x,
                    (*area).min.y + (*area).size().y * 0.2,
                    (*area).size().x,
                    (*area).size().y * 0.4,
                ),
                Rect::new_size(
                    (*area).min.x,
                    (*area).min.y + (*area).size().y * (0.2 + 0.4),
                    (*area).size().x,
                    (*area).size().y * 0.3,
                ),
                Rect::new_size(
                    (*area).min.x,
                    (*area).min.y + (*area).size().y * (0.2 + 0.4 + 0.3),
                    (*area).size().x,
                    (*area).size().y * 0.1,
                ),
            ];

            for (i, split) in splits.iter_splits(*area, Direction::Vertical).enumerate() {
                assert!(areas[i].almost_equal(split.area));
            }
            for (i, split) in splits.iter_splits_mut(*area, Direction::Vertical).enumerate() {
                assert!(areas[i].almost_equal(split.area));
            }
        }
        #[test]
        fn test_splits_horizontal() {
            let mut splits = vec![
                Split::new(0.2, Zone::Empty),
                Split::new(0.4, Zone::Empty),
                Split::new(0.3, Zone::Empty),
                Split::new(0.1, Zone::Empty),
            ];

            let areas = vec![
                Rect::new_size((*area).min.x, (*area).min.y, (*area).size().x * 0.2, (*area).size().y),
                Rect::new_size(
                    (*area).min.x + (*area).size().x * 0.2,
                    (*area).min.y,
                    (*area).size().x * 0.4,
                    (*area).size().y,
                ),
                Rect::new_size(
                    (*area).min.x + (*area).size().x * (0.2 + 0.4),
                    (*area).min.y,
                    (*area).size().x * 0.3,
                    (*area).size().y,
                ),
                Rect::new_size(
                    (*area).min.x + (*area).size().x * (0.2 + 0.4 + 0.3),
                    (*area).min.y,
                    (*area).size().x * 0.1,
                    (*area).size().y,
                ),
            ];

            for (i, split) in splits.iter_splits(*area, Direction::Horizontal).enumerate() {
                assert!(areas[i].almost_equal(split.area));
            }
            for (i, split) in splits.iter_splits_mut(*area, Direction::Horizontal).enumerate() {
                assert!(areas[i].almost_equal(split.area));
            }
        }
    }

    mod test_add_and_resize {
        use super::*;

        #[test]
        fn test_append_and_resize() {
            let expected_size = 1.0 / 6.0;
            let expected = vec![
                Split::new(expected_size, zone_element(uuids[0])),
                Split::new(expected_size, zone_element(uuids[1])),
                Split::new(expected_size, zone_element(uuids[2])),
                Split::new(expected_size, zone_element(uuids[3])),
                Split::new(expected_size, zone_element(uuids[4])),
                Split::new(expected_size, zone_element(uuids[5])),
            ];

            let mut first_splits = vec![
                Split::new(0.2, zone_element(uuids[0])),
                Split::new(0.4, zone_element(uuids[1])),
                Split::new(0.4, zone_element(uuids[2])),
            ];
            let mut second_splits = vec![
                Split::new(0.8, zone_element(uuids[3])),
                Split::new(0.1, zone_element(uuids[4])),
                Split::new(0.1, zone_element(uuids[5])),
            ];
            first_splits.append_and_resize(&mut second_splits);
            assert_splits(&expected, &first_splits);
        }
        #[test]
        fn test_push_and_resize() {
            let expected_size = 1.0 / 4.0;
            let expected = vec![
                Split::new(expected_size, zone_element(uuids[0])),
                Split::new(expected_size, zone_element(uuids[1])),
                Split::new(expected_size, zone_element(uuids[2])),
                Split::new(expected_size, zone_element(uuids[3])),
            ];
            let mut splits = vec![
                Split::new(0.2, zone_element(uuids[0])),
                Split::new(0.4, zone_element(uuids[1])),
                Split::new(0.4, zone_element(uuids[2])),
            ];
            splits.push_and_resize(Split::new(0.1, zone_element(uuids[3])));
            assert_splits(&expected, &splits);
        }
        #[test]
        fn test_push_and_resize_zone() {
            let expected_size = 1.0 / 4.0;
            let expected = vec![
                Split::new(expected_size, zone_element(uuids[0])),
                Split::new(expected_size, zone_element(uuids[1])),
                Split::new(expected_size, zone_element(uuids[2])),
                Split::new(expected_size, zone_element(uuids[3])),
            ];
            let mut splits = vec![
                Split::new(0.2, zone_element(uuids[0])),
                Split::new(0.4, zone_element(uuids[1])),
                Split::new(0.4, zone_element(uuids[2])),
            ];
            splits.push_and_resize_zone(zone_element(uuids[3]));
            assert_splits(&expected, &splits);
        }
        #[test]
        fn test_insert_and_resize() {
            let expected_size = 1.0 / 4.0;
            let expected = vec![
                Split::new(expected_size, zone_element(uuids[0])),
                Split::new(expected_size, zone_element(uuids[3])),
                Split::new(expected_size, zone_element(uuids[1])),
                Split::new(expected_size, zone_element(uuids[2])),
            ];
            let mut splits = vec![
                Split::new(0.2, zone_element(uuids[0])),
                Split::new(0.4, zone_element(uuids[1])),
                Split::new(0.4, zone_element(uuids[2])),
            ];
            splits.insert_and_resize(1, Split::new(0.1, zone_element(uuids[3])));
            assert_splits(&expected, &splits);
        }
        #[test]
        fn test_insert_and_resize_zone() {
            let expected_size = 1.0 / 4.0;
            let expected = vec![
                Split::new(expected_size, zone_element(uuids[0])),
                Split::new(expected_size, zone_element(uuids[3])),
                Split::new(expected_size, zone_element(uuids[1])),
                Split::new(expected_size, zone_element(uuids[2])),
            ];
            let mut splits = vec![
                Split::new(0.2, zone_element(uuids[0])),
                Split::new(0.4, zone_element(uuids[1])),
                Split::new(0.4, zone_element(uuids[2])),
            ];
            splits.insert_and_resize_zone(1, zone_element(uuids[3]));
            assert_splits(&expected, &splits);
        }
        #[test]
        fn test_modify_zone_at() {
            let expected_size = 1.0 / 3.0;
            let expected = vec![
                Split::new(expected_size, zone_element(uuids[0])),
                Split::new(expected_size, zone_element(uuids[3])),
                Split::new(expected_size, zone_element(uuids[2])),
            ];
            let mut splits = vec![
                Split::new(expected_size, zone_element(uuids[0])),
                Split::new(expected_size, zone_element(uuids[1])),
                Split::new(expected_size, zone_element(uuids[2])),
            ];
            splits.modify_zone_at(1, |zone| {
                assert_eq!(zone.dummy_uuid(), uuids[1], "Modified zone has wrong UUID");
                zone_element(uuids[3])
            });
            assert_splits(&expected, &splits);
        }
    }
}
