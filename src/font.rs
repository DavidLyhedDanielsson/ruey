//extern crate image;

use color::Color;
use std::cmp::max;
use std::cmp::min;
use std::collections::BTreeMap;
use std::fs;
use std::io::Read;
use std::path::Path;
use vertex::Vertex;
use guierror::GuiError;
use vector2::Vector2;
use parsing::alignment::*;
use std::convert::From;

#[derive(Debug, Copy, Clone)]
pub struct Character {
    pub u_min: f32,
    pub u_max: f32,
    pub v_min: f32,
    pub v_max: f32,
    pub width: u8,
    pub y_offset: u8,
    pub x_advance: u8,
}

#[derive(Debug)]
pub struct Font {
    characters: BTreeMap<char, Character>,
    error_character: Character,
    font_texture_data: Vec<u8>,
    ascent: i8,
    descent: i8,
    font_height: i32,
}

#[derive(Copy, Clone, Debug)]
pub enum VerticalOrigin {
    Top,
    Center,
    Bottom,
}

impl From<VerticalAlignment> for VerticalOrigin {
    fn from(alignment: VerticalAlignment) -> Self {
        match alignment {
            VerticalAlignment::Top => VerticalOrigin::Top,
            VerticalAlignment::Center => VerticalOrigin::Center,
            VerticalAlignment::Bottom => VerticalOrigin::Bottom,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum HorizontalOrigin {
    Left,
    Center,
    Right,
}

impl From<HorizontalAlignment> for HorizontalOrigin {
    fn from(alignment: HorizontalAlignment) -> Self {
        match alignment {
            HorizontalAlignment::Left => HorizontalOrigin::Left,
            HorizontalAlignment::Center => HorizontalOrigin::Center,
            HorizontalAlignment::Right => HorizontalOrigin::Right,
        }
    }
}

#[allow(clippy::too_many_arguments)] // Only warns about events in this file, might want to add more events at some point
impl Font {
    pub fn new(path: &Path) -> Result<Font, GuiError> {
        let mut font_data = Vec::<u8>::new();
        fs::File::open(path)?.read_to_end(&mut font_data)?;

        let collection = rusttype::FontCollection::from_bytes(font_data)?;
        let font = collection.into_font()?;
        let mut character_string = String::with_capacity(127 - 33); //Visible ASCII
        for i in 33..127 {
            character_string.push(unsafe { ::std::char::from_u32_unchecked(i) });
        }

        let font_height = 18;
        let scale = rusttype::Scale {
            x: font_height as f32,
            y: font_height as f32,
        };
        let ascent = font.v_metrics(scale).ascent.ceil() as i8;
        let descent = font.v_metrics(scale).descent.ceil() as i8;
        let offset = rusttype::point(0.0, f32::from(ascent));

        let mut font_texture_data = vec![0 as u8; 512 * 512];
        // Square to draw single colors
        font_texture_data[0] = 255;
        font_texture_data[1] = 255;
        font_texture_data[512] = 255;
        font_texture_data[513] = 255;
        let texel_size: f32 = 0.0;
        let mut base_x: i32 = 2;
        let mut base_y: i32 = 0;

        let mut iter = character_string.chars();
        let mut characters: BTreeMap<char, Character> = BTreeMap::new();
        font_texture_data[0] = 255;
        characters.insert(
            ' ',
            Character {
                u_min: 0.0,
                u_max: 0.0,
                v_min: 0.0,
                v_max: 0.0,
                width: 0,
                y_offset: 0,
                x_advance: 10,
            },
        );
        font.layout(&character_string, scale, offset).for_each(|glyph| {
            if let Some(bounds) = glyph.pixel_bounding_box() {
                if base_x + bounds.width() as i32 >= 512 {
                    base_x = 0;
                    base_y += i32::from(ascent) + 1;
                }

                glyph.draw(|x, y, v| {
                    font_texture_data[(base_x + x as i32 + (base_y + y as i32) * 512) as usize] = (v * 255.0) as u8;
                });

                let width = bounds.width() as i32;
                characters.insert(
                    iter.next().expect("Character iterator out-of-bounds"),
                    Character {
                        u_min: (base_x as f32 + texel_size) / 512.0,
                        u_max: ((base_x + width) as f32 - texel_size) / 512.0,
                        v_min: ((base_y) as f32 + texel_size) / 512.0,
                        v_max: ((base_y + i32::from(ascent)) as f32 - texel_size) / 512.0,
                        width: width as u8,
                        y_offset: bounds.min.y as u8,
                        x_advance: glyph.unpositioned().h_metrics().advance_width as u8,
                    },
                );

                base_x += bounds.width() as i32 + 1;
            } else {
                error!(
                    "Error creating character \"{}\"",
                    iter.next().expect("Character iterator out-of-bounds")
                );
            }
        });

        //image::save_buffer("outfile.png", &font_texture_data, 512, 512, image::ColorType::Gray(8)).unwrap();
        Ok(Font {
            error_character: *characters.get(&'?').unwrap_or_else(|| characters.get(&' ').unwrap()),
            characters,
            font_texture_data,
            ascent,
            descent,
            font_height,
        })
    }

    pub fn create_text(
        &self,
        text: &str,
        color: Color,
        size: f32,
        vertex_buffer: &mut Vec<Vertex>,
        index_buffer: &mut Vec<u32>,
    ) {
        let mut x: f32 = 0.0;
        for ch in text.chars() {
            let character = self.characters.get(&ch).unwrap_or(&self.error_character);
            let offset = vertex_buffer.len() as u32;

            vertex_buffer.push(Vertex::new(
                x,
                f32::from(character.y_offset) * size,
                character.u_min,
                character.v_min,
                color,
            ));
            vertex_buffer.push(Vertex::new(
                x,
                (f32::from(character.y_offset) + f32::from(self.ascent)) * size,
                character.u_min,
                character.v_max,
                color,
            ));
            vertex_buffer.push(Vertex::new(
                x + f32::from(character.width) * size,
                (f32::from(character.y_offset) + f32::from(self.ascent)) * size,
                character.u_max,
                character.v_max,
                color,
            ));
            vertex_buffer.push(Vertex::new(
                x + f32::from(character.width) * size,
                f32::from(character.y_offset) * size,
                character.u_max,
                character.v_min,
                color,
            ));

            index_buffer.push(offset);
            index_buffer.push(offset + 1);
            index_buffer.push(offset + 2);

            index_buffer.push(offset);
            index_buffer.push(offset + 2);
            index_buffer.push(offset + 3);

            x += f32::from(character.x_advance) * size;
        }
    }

    pub fn create_text_positioned(
        &self,
        text: &str,
        color: Color,
        vertex_buffer: &mut Vec<Vertex>,
        index_buffer: &mut Vec<u32>,
        position: Vector2,
        horizontal_origin: HorizontalOrigin,
        vertical_origin: VerticalOrigin,
        size: f32,
    ) {
        let vertex_buffer_offset = vertex_buffer.len();
        self.create_text(text, color, size, vertex_buffer, index_buffer);
        self.position_text(
            &mut vertex_buffer[vertex_buffer_offset..],
            Vector2::new(position.x.round(), position.y.round()),
            horizontal_origin,
            vertical_origin,
        );
    }

    pub fn measure_str(&self, text: &str) -> (i32, i32) {
        let mut width = 0;

        for ch in text.chars() {
            let character = self.characters.get(&ch).unwrap_or(&self.error_character);
            width += i32::from(character.x_advance);
        }

        (width, i32::from(self.ascent))
    }

    pub fn get_width_at_index(&self, text: &str, index: usize) -> usize {
        let mut width = 0;

        for (i, ch) in text.chars().enumerate() {
            let character = self.characters.get(&ch).unwrap_or(&self.error_character);

            if i == index {
                return width;
            }

            width += usize::from(character.x_advance);
        }

        width
    }

    pub fn get_str_index(&self, text: &str, target_width: usize) -> usize {
        let mut width = 0;

        for (i, ch) in text.chars().enumerate() {
            let character = self.characters.get(&ch).unwrap_or(&self.error_character);
            if width + usize::from(character.x_advance) > target_width {
                let percent = (target_width - width) as f32 / f32::from(character.x_advance);

                if percent <= 0.45 {
                    return i;
                } else {
                    return i + 1;
                }
            } else {
                width += usize::from(character.x_advance);
            }
        }

        text.chars().count()
    }

    pub fn measure_text(text_vertices: &mut [Vertex]) -> (i32, i32) {
        let mut min_x = 0;
        let mut max_x = 0;
        let mut min_y = 0;
        let mut max_y = 0;

        for vertex in text_vertices.iter() {
            min_x = min(min_x, vertex.x as i32);
            max_x = max(max_x, vertex.x as i32);
            min_y = min(min_y, vertex.y as i32);
            max_y = max(max_y, vertex.y as i32);
        }

        (max_x - min_x, max_y - min_y)
    }

    pub fn position_text(
        &self,
        text_vertices: &mut [Vertex],
        position: Vector2,
        horizontal_origin: HorizontalOrigin,
        vertical_origin: VerticalOrigin,
    ) {
        let mut min_x = 0;
        let mut max_x = 0;

        for vertex in text_vertices.iter() {
            min_x = min(min_x, vertex.x as i32);
            max_x = max(max_x, vertex.x as i32);
        }

        let center_x = (min_x + max_x) / 2;

        let position_x = position.x as i32;

        let offset_x = match horizontal_origin {
            HorizontalOrigin::Left => position_x - min_x,
            HorizontalOrigin::Center => position_x - center_x,
            HorizontalOrigin::Right => position_x - max_x,
        };
        let offset_y = self.get_vertical_position(position, vertical_origin);

        for mut vertex in text_vertices.iter_mut() {
            vertex.x += offset_x as f32;
            vertex.y += offset_y as f32;
        }
    }

    pub fn get_vertical_position(&self, position: Vector2, vertical_origin: VerticalOrigin) -> f32 {
        match vertical_origin {
            VerticalOrigin::Top => position.y,
            VerticalOrigin::Center => position.y - f32::from(self.ascent - self.descent) * 0.5,
            VerticalOrigin::Bottom => position.y - f32::from(self.ascent - self.descent), // Descent should be negative
        }
    }

    pub fn get_texture_data(&self) -> &Vec<u8> {
        &self.font_texture_data
    }

    pub fn get_line_height(&self) -> i32 {
        i32::from(self.ascent)
    }

    pub fn get_font_height(&self) -> i32 {
        self.font_height
    }
}
