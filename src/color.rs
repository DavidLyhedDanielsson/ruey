use serde::de::SeqAccess;
use std::fmt;

use serde::de::{Deserialize, Deserializer, Visitor};

#[derive(Clone, Copy, Debug, PartialEq)]
#[cfg_attr(test, derive(Default))]
pub struct Color {
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32,
}

impl Color {
    pub fn new(r: f32, g: f32, b: f32, a: f32) -> Color {
        Color { r, g, b, a }
    }

    pub fn newb(r: u8, g: u8, b: u8, a: u8) -> Color {
        Color {
            r: f32::from(r) / 255.0,
            g: f32::from(g) / 255.0,
            b: f32::from(b) / 255.0,
            a: f32::from(a) / 255.0,
        }
    }

    pub fn white() -> Color {
        Color::new(1.0, 1.0, 1.0, 1.0)
    }
}

impl<'de> Deserialize<'de> for Color {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct ColorVisitor;

        impl<'de> Visitor<'de> for ColorVisitor {
            type Value = Color;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("A hex value with 8 digits e.g. #ABCDEFFF")
            }

            fn visit_seq<A>(self, mut seq: A) -> Result<Color, A::Error>
            where
                A: SeqAccess<'de>,
            {
                let r = seq.next_element::<u8>()?.unwrap();
                let g = seq.next_element::<u8>()?.unwrap();
                let b = seq.next_element::<u8>()?.unwrap();
                let a = seq.next_element::<u8>()?.unwrap();
                Ok(Color::newb(r, g, b, a))
            }
        }

        deserializer.deserialize_tuple_struct("Color", 4, ColorVisitor)
    }
}
