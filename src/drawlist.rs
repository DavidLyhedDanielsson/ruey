use color::Color;
use rect::Rect;
use std::vec::Vec;
use vertex::Vertex;

pub struct DrawCommand {
    pub index_offset: u32,
    pub index_count: u32,
    pub clip_rect: Rect,
}

#[derive(Default)]
pub struct DrawList {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u32>,
    pub commands: Vec<DrawCommand>,
    clip_rects: Vec<Rect>,
}

impl DrawList {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn new_command(&mut self) {
        let clip_rect = *self.clip_rects.last().expect("No clip rect! Did you pop one too many?");
        if self.commands.is_empty() {
            self.commands.push(DrawCommand {
                index_offset: self.indices.len() as u32,
                index_count: 0,
                clip_rect,
            });
        } else {
            let last_clip_rect;
            {
                let last_command: &mut DrawCommand = self.commands.last_mut().unwrap();
                last_command.index_count = self.indices.len() as u32 - last_command.index_offset;
                last_clip_rect = last_command.clip_rect;
            }

            if last_clip_rect != clip_rect {
                self.commands.push(DrawCommand {
                    index_offset: self.indices.len() as u32,
                    index_count: 0,
                    clip_rect,
                });
            }
        }
    }

    pub fn push_clip_rect(&mut self, clip_rect: Rect) {
        let new_clip_rect;
        if let Some(last_clip_rect) = self.clip_rects.last() {
            new_clip_rect = Rect::new_intersection(clip_rect, *last_clip_rect);
        } else {
            new_clip_rect = clip_rect;
        }
        self.clip_rects.push(new_clip_rect);
        self.new_command();
    }

    pub fn pop_clip_rect(&mut self) {
        self.clip_rects.pop();
        self.new_command();
    }

    pub fn push_quad(&mut self, area: Rect, uv_area: Rect, color: Color) {
        let index_offset = self.vertices.len() as u32;

        self.vertices
            .push(Vertex::new(area.min.x, area.min.y, uv_area.min.x, uv_area.min.y, color));
        self.vertices
            .push(Vertex::new(area.min.x, area.max.y, uv_area.min.x, uv_area.max.y, color));
        self.vertices
            .push(Vertex::new(area.max.x, area.max.y, uv_area.max.x, uv_area.max.y, color));
        self.vertices
            .push(Vertex::new(area.max.x, area.min.y, uv_area.max.x, uv_area.min.y, color));

        self.indices.push(index_offset);
        self.indices.push(index_offset + 1);
        self.indices.push(index_offset + 2);

        self.indices.push(index_offset);
        self.indices.push(index_offset + 2);
        self.indices.push(index_offset + 3);
    }

    pub fn push_irregular_quad(
        &mut self,
        v0: (f32, f32),
        v1: (f32, f32),
        v2: (f32, f32),
        v3: (f32, f32),
        color: Color,
    ) {
        let index_offset = self.vertices.len() as u32;
        self.indices.push(index_offset);
        self.indices.push(index_offset + 1);
        self.indices.push(index_offset + 2);
        self.indices.push(index_offset);
        self.indices.push(index_offset + 2);
        self.indices.push(index_offset + 3);

        let uv = 1.0 / 512.0;
        self.vertices.push(Vertex::new(v0.0, v0.1, uv, uv, color));
        self.vertices.push(Vertex::new(v1.0, v1.1, uv, uv, color));
        self.vertices.push(Vertex::new(v2.0, v2.1, uv, uv, color));
        self.vertices.push(Vertex::new(v3.0, v3.1, uv, uv, color));
    }

    pub fn push_triangle(&mut self, v0: (f32, f32), v1: (f32, f32), v2: (f32, f32), color: Color) {
        let index_offset = self.vertices.len() as u32;
        self.indices.push(index_offset);
        self.indices.push(index_offset + 1);
        self.indices.push(index_offset + 2);

        let uv = 1.0 / 512.0;
        self.vertices.push(Vertex::new(v0.0, v0.1, uv, uv, color));
        self.vertices.push(Vertex::new(v1.0, v1.1, uv, uv, color));
        self.vertices.push(Vertex::new(v2.0, v2.1, uv, uv, color));
    }

    pub fn push_outline_quad(&mut self, quad: Rect, thickness: f32, color: Color) {
        self.push_irregular_quad(
            (quad.min.x - thickness, quad.min.y - thickness),
            (quad.min.x + thickness, quad.min.y + thickness),
            (quad.min.x + thickness, quad.max.y - thickness),
            (quad.min.x - thickness, quad.max.y + thickness),
            color,
        );
        self.push_irregular_quad(
            (quad.min.x - thickness, quad.min.y - thickness),
            (quad.min.x + thickness, quad.min.y + thickness),
            (quad.max.x - thickness, quad.min.y + thickness),
            (quad.max.x + thickness, quad.min.y - thickness),
            color,
        );
        self.push_irregular_quad(
            (quad.max.x - thickness, quad.min.y + thickness),
            (quad.max.x + thickness, quad.min.y - thickness),
            (quad.max.x + thickness, quad.max.y + thickness),
            (quad.max.x - thickness, quad.max.y - thickness),
            color,
        );
        self.push_irregular_quad(
            (quad.min.x - thickness, quad.max.y + thickness),
            (quad.min.x + thickness, quad.max.y - thickness),
            (quad.max.x - thickness, quad.max.y - thickness),
            (quad.max.x + thickness, quad.max.y + thickness),
            color,
        );
    }

    pub fn push_colored_quad(&mut self, area: Rect, color: Color) {
        self.push_multicolored_quad(area, color, color, color, color)
    }

    pub fn push_multicolored_quad(
        &mut self,
        area: Rect,
        top_left: Color,
        top_right: Color,
        bottom_left: Color,
        bottom_right: Color,
    ) {
        let uv = 1.0 / 512.0;

        let index_offset = self.vertices.len() as u32;

        self.vertices
            .push(Vertex::new(area.min.x, area.min.y, uv, uv, top_left));
        self.vertices
            .push(Vertex::new(area.min.x, area.max.y, uv, uv, bottom_left));
        self.vertices
            .push(Vertex::new(area.max.x, area.max.y, uv, uv, bottom_right));
        self.vertices
            .push(Vertex::new(area.max.x, area.min.y, uv, uv, top_right));

        self.indices.push(index_offset);
        self.indices.push(index_offset + 1);
        self.indices.push(index_offset + 2);

        self.indices.push(index_offset);
        self.indices.push(index_offset + 2);
        self.indices.push(index_offset + 3);
    }
}
