use color::Color;

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct Vertex {
    pub x: f32,
    pub y: f32,
    pub u: f32,
    pub v: f32,
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32,
}

impl Vertex {
    pub fn new(x: f32, y: f32, u: f32, v: f32, color: Color) -> Vertex {
        Vertex {
            x,
            y,
            u,
            v,
            r: color.r,
            g: color.g,
            b: color.b,
            a: color.a,
        }
    }
}
