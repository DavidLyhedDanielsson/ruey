use ruey::vertex::Vertex;
use std::ffi::CString;

// (programme, vao, vertex buffer, index buffer)
pub fn init_gl(font_texture_data: &[u8]) -> (gl::types::GLuint, gl::types::GLuint, gl::types::GLuint, gl::types::GLuint) {
    let mut vao: gl::types::GLuint = 0;
    let mut vertex_buffer: gl::types::GLuint = 0;
    let mut index_buffer: gl::types::GLuint = 0;
    unsafe {
        gl::GenVertexArrays(1, &mut vao);
        gl::BindVertexArray(vao);

        gl::GenBuffers(1, &mut vertex_buffer);
        gl::BindBuffer(gl::ARRAY_BUFFER, vertex_buffer);
        gl::BufferData(
            gl::ARRAY_BUFFER,
            std::mem::size_of::<Vertex>() as isize * 4096 * 4 as gl::types::GLsizeiptr,
            std::ptr::null(),
            gl::DYNAMIC_DRAW,
        );

        gl::GenBuffers(1, &mut index_buffer);
        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, index_buffer);
        gl::BufferData(
            gl::ELEMENT_ARRAY_BUFFER,
            std::mem::size_of::<gl::types::GLint>() as isize * 4096 * 6 as gl::types::GLsizeiptr,
            std::ptr::null(),
            gl::DYNAMIC_DRAW,
        );
    }

    let vertex_shader: gl::types::GLuint;
    unsafe {
        let vertex_shader_source = standard_vertex_shader();
        vertex_shader = gl::CreateShader(gl::VERTEX_SHADER);
        gl::ShaderSource(
            vertex_shader,
            1,
            &vertex_shader_source.as_c_str().as_ptr(),
            std::ptr::null(),
        );
        gl::CompileShader(vertex_shader);

        let mut success: gl::types::GLint = 0;
        gl::GetShaderiv(vertex_shader, gl::COMPILE_STATUS, &mut success);
        if success == 0 {
            panic!("Vertex shader compilation failed");
        }
    }

    let fragment_shader: gl::types::GLuint;
    unsafe {
        let fragment_shader_source = standard_fragment_shader();
        fragment_shader = gl::CreateShader(gl::FRAGMENT_SHADER);
        gl::ShaderSource(
            fragment_shader,
            1,
            &fragment_shader_source.as_c_str().as_ptr(),
            std::ptr::null(),
        );
        gl::CompileShader(fragment_shader);

        let mut success: gl::types::GLint = 0;
        gl::GetShaderiv(fragment_shader, gl::COMPILE_STATUS, &mut success);
        if success == 0 {
            panic!("Fragment shader compilation failed");
        }
    }

    let program: gl::types::GLuint;
    unsafe {
        program = gl::CreateProgram();
        gl::AttachShader(program, vertex_shader);
        gl::AttachShader(program, fragment_shader);
        gl::LinkProgram(program);

        let mut success: gl::types::GLint = 0;
        gl::GetProgramiv(program, gl::LINK_STATUS, &mut success);
        if success == 0 {
            panic!("Shader program linking error");
        }

        gl::DetachShader(program, vertex_shader);
        gl::DetachShader(program, fragment_shader);
        gl::DeleteShader(vertex_shader);
        gl::DeleteShader(fragment_shader);

        gl::EnableVertexAttribArray(0);
        gl::EnableVertexAttribArray(1);
        gl::EnableVertexAttribArray(2);

        gl::VertexAttribPointer(
            0,
            2,
            gl::FLOAT,
            gl::FALSE,
            std::mem::size_of::<Vertex>() as gl::types::GLsizei,
            std::ptr::null(),
        );
        gl::VertexAttribPointer(
            1,
            2,
            gl::FLOAT,
            gl::FALSE,
            std::mem::size_of::<Vertex>() as gl::types::GLsizei,
            (std::mem::size_of::<f32>() * 2) as *const gl::types::GLvoid,
        );
        gl::VertexAttribPointer(
            2,
            4,
            gl::FLOAT,
            gl::FALSE,
            std::mem::size_of::<Vertex>() as gl::types::GLsizei,
            (std::mem::size_of::<f32>() * 4) as *const gl::types::GLvoid,
        );
        gl::UseProgram(program);
    }

    let mut font_texture: gl::types::GLuint = 0;
    unsafe {
        gl::GenTextures(1, &mut font_texture);
        gl::BindTexture(gl::TEXTURE_2D, font_texture);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::REPEAT as i32);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::REPEAT as i32);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as i32);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
        gl::TexImage2D(
            gl::TEXTURE_2D,
            0,
            gl::R8 as i32,
            512,
            512,
            0,
            gl::RED,
            gl::UNSIGNED_BYTE,
            font_texture_data.as_ptr() as *const std::os::raw::c_void,
        );
    }

    unsafe {
        let err = gl::GetError();
        if err != 0 {
            panic!("GL error after initialization: {}", err);
        }
    }

    (program, vao, vertex_buffer, index_buffer)
}

fn standard_vertex_shader() -> CString {
    CString::new(
        "\
         #version 330 core\n\
         layout(location = 0) in vec2 position;\
         layout(location = 1) in vec2 uv;\
         layout(location = 2) in vec4 color;\
         out vec4 vertexColor;\
         out vec2 vertexUV;\
         uniform vec2 resolution;\
         void main(){\
         vertexColor = color;\
         vec2 pos = position / vec2(resolution);\
         pos.y = 1.0f - pos.y;\
         pos -= vec2(0.5f, 0.5f);\
         pos *= 2.0f;\
         gl_Position = vec4(pos, 0.0f, 1.0f);\
         vertexUV = uv;\
         }",
    )
    .unwrap()
}

fn standard_fragment_shader() -> CString {
    CString::new(
        "\
         #version 330 core\n\
         in vec4 vertexColor;\
         in vec2 vertexUV;\
         out vec4 fragmentColor;\
         uniform sampler2D tex;\
         void main(){\
         float col = texture(tex, vertexUV).r;\
         fragmentColor = vec4(vertexColor.rgb, col * vertexColor.a);\
         }",
    )
    .unwrap()
}
