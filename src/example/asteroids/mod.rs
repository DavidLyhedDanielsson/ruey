use example::mainutil;

use notify::{DebouncedEvent, RecommendedWatcher, RecursiveMode, Watcher};
use ruey::context::drindexprovider::DRIndexProvider;
use ruey::context::Context;
use ruey::drawlist::DrawList;
use ruey::guierror::GuiError;
use ruey::{guikey::GuiKey, rect::Rect, vertex::Vertex, Gui};
use std::collections::BTreeMap;
use std::sync::mpsc::{channel, TryRecvError};
use std::time::Duration;
use std::{ffi::CString, time};
use stopwatch::Stopwatch;
use cgmath::{self, Matrix, SquareMatrix, InnerSpace, Vector3, Point3, Matrix4};
use rand::{thread_rng, Rng};

struct Asteroid {
    position_percent: f32,
    position_increment: f32,
    scale_percent: f32,
    radius_percent: f32,
    height_percent: f32,
}
impl Asteroid {
    fn new(position_percent: f32, scale_percent: f32, radius_percent: f32, height_percent: f32, position_increment: f32) -> Self {
        Asteroid { position_percent, scale_percent, radius_percent, height_percent, position_increment }
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
struct Vertex3D {
    position: Vector3<f32>,
    padding0 : f32,
    normal: Vector3<f32>,
    padding1 : f32,
    color: Vector3<f32>,
    padding2 : f32,
}
impl Vertex3D {
    fn new(position: Vector3<f32>, normal: Vector3<f32>, color: Vector3<f32>) -> Self {
        Vertex3D {
            position,
            normal,
            color,
            padding0: 0.0,
            padding1: 0.0,
            padding2: 0.0,
        }
    }
}

// (programme, vao, index buffer, world matrices buffer)
fn shader_setup(vertices: &[Vertex3D], indices: &[u32]) -> (gl::types::GLuint, gl::types::GLuint, gl::types::GLuint, gl::types::GLuint) {
    let mut vao: gl::types::GLuint = 0;
    let mut vertex_buffer: gl::types::GLuint = 0;
    let mut index_buffer: gl::types::GLuint = 0;
    let mut world_matrices: gl::types::GLuint = 0;
    unsafe {
        gl::GenVertexArrays(1, &mut vao);
        gl::BindVertexArray(vao);

        gl::GenBuffers(1, &mut world_matrices);
        gl::BindBuffer(gl::ARRAY_BUFFER, world_matrices);
        gl::BufferData(
            gl::ARRAY_BUFFER,
            (std::mem::size_of::<cgmath::Matrix4::<f32>>() as isize * 1024 as isize) as gl::types::GLsizeiptr,
            std::ptr::null(),
            gl::DYNAMIC_DRAW,
        );

        gl::GenBuffers(1, &mut vertex_buffer);
        gl::BindBuffer(gl::ARRAY_BUFFER, vertex_buffer);
        gl::BufferData(
            gl::ARRAY_BUFFER,
            (std::mem::size_of::<Vertex3D>() as isize * vertices.len() as isize) as gl::types::GLsizeiptr,
            vertices.as_ptr() as *const gl::types::GLvoid,
            gl::STATIC_DRAW,
        );

        gl::GenBuffers(1, &mut index_buffer);
        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, index_buffer);
        gl::BufferData(
            gl::ELEMENT_ARRAY_BUFFER,
            (std::mem::size_of::<u32>() as isize * indices.len() as isize) as gl::types::GLsizeiptr,
            indices.as_ptr() as *const gl::types::GLvoid,
            gl::STATIC_DRAW,
        );
    }

    let vertex_shader: gl::types::GLuint;
    unsafe {
        let vertex_shader_source = 
            CString::new(
                r#"\
                #version 330 core
                layout(location = 0) in vec3 position;
                layout(location = 1) in vec3 normal;
                layout(location = 2) in vec3 color;
                layout(location = 3) in mat4 worldMatrix;
                out vec3 vertexColor;
                uniform mat4 viewProjectionMatrix;
                void main(){
                    vec3 lightPosition = vec3(0.0f, 0.0f, 0.0f);

                    vec3 worldPosition = (worldMatrix * vec4(position, 1.0f)).xyz;
                    vec3 worldNormal = (worldMatrix * vec4(normal, 0.0)).xyz;

                    vec3 lightDir = normalize(lightPosition - worldPosition);
                    float lightContrib = dot(lightDir, worldNormal);
                    vertexColor = lightContrib * 0.3f + vec3(0.5f, 0.5f, 0.5f) * color;
                    gl_Position = viewProjectionMatrix * worldMatrix * vec4(position, 1.0);
                }"#,
            )
            .unwrap();
        vertex_shader = gl::CreateShader(gl::VERTEX_SHADER);
        gl::ShaderSource(
            vertex_shader,
            1,
            &vertex_shader_source.as_c_str().as_ptr(),
            std::ptr::null(),
        );
        gl::CompileShader(vertex_shader);

        let mut success: gl::types::GLint = 0;
        gl::GetShaderiv(vertex_shader, gl::COMPILE_STATUS, &mut success);
        if success == 0 {
            panic!("3D vertex shader compilation failed");
        }
    }


    let fragment_shader: gl::types::GLuint;
    unsafe {
        let fragment_shader_source = 
            CString::new(
                r#"\
                #version 330 core
                in vec3 vertexColor;
                out vec4 fragColor;
                void main(){
                    fragColor = vec4(vertexColor, 1.0f);
                }"#,
            )
            .unwrap();
        fragment_shader = gl::CreateShader(gl::FRAGMENT_SHADER);
        gl::ShaderSource(
            fragment_shader,
            1,
            &fragment_shader_source.as_c_str().as_ptr(),
            std::ptr::null(),
        );
        gl::CompileShader(fragment_shader);

        let mut success: gl::types::GLint = 0;
        gl::GetShaderiv(fragment_shader, gl::COMPILE_STATUS, &mut success);
        if success == 0 {
            panic!("3D fragment shader compilation failed");
        }
    }

    let program: gl::types::GLuint;
    unsafe {
        program = gl::CreateProgram();
        gl::AttachShader(program, vertex_shader);
        gl::AttachShader(program, fragment_shader);
        gl::LinkProgram(program);

        let mut success: gl::types::GLint = 0;
        gl::GetProgramiv(program, gl::LINK_STATUS, &mut success);
        if success == 0 {
            panic!("Shader program linking error");
        }

        gl::DetachShader(program, vertex_shader);
        gl::DetachShader(program, fragment_shader);
        gl::DeleteShader(vertex_shader);
        gl::DeleteShader(fragment_shader);

        gl::EnableVertexAttribArray(0); // position
        gl::EnableVertexAttribArray(1); // normal
        gl::EnableVertexAttribArray(2); // color

        gl::VertexAttribPointer(
            0,
            3,
            gl::FLOAT,
            gl::FALSE,
            std::mem::size_of::<Vertex3D>() as gl::types::GLsizei,
            std::ptr::null(),
        );
        gl::VertexAttribPointer(
            1,
            3,
            gl::FLOAT,
            gl::TRUE,
            std::mem::size_of::<Vertex3D>() as gl::types::GLsizei,
            (std::mem::size_of::<f32>() * 4) as *const gl::types::GLvoid,
        );
        gl::VertexAttribPointer(
            2,
            3,
            gl::FLOAT,
            gl::FALSE,
            std::mem::size_of::<Vertex3D>() as gl::types::GLsizei,
            (std::mem::size_of::<f32>() * 8) as *const gl::types::GLvoid,
        );

        gl::BindBuffer(gl::ARRAY_BUFFER, 0);
        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);

        gl::BindBuffer(gl::ARRAY_BUFFER, world_matrices);

        gl::EnableVertexAttribArray(3);
        gl::EnableVertexAttribArray(4);
        gl::EnableVertexAttribArray(5);
        gl::EnableVertexAttribArray(6);
        gl::VertexAttribPointer(
            3,
            4,
            gl::FLOAT,
            gl::FALSE,
            std::mem::size_of::<cgmath::Matrix4::<f32>>() as gl::types::GLsizei,
            std::ptr::null(),
        );
        gl::VertexAttribPointer(
            4,
            4,
            gl::FLOAT,
            gl::FALSE,
            std::mem::size_of::<cgmath::Matrix4::<f32>>() as gl::types::GLsizei,
            (std::mem::size_of::<f32>() * 4) as *const gl::types::GLvoid,
        );
        gl::VertexAttribPointer(
            5,
            4,
            gl::FLOAT,
            gl::FALSE,
            std::mem::size_of::<cgmath::Matrix4::<f32>>() as gl::types::GLsizei,
            (std::mem::size_of::<f32>() * 8) as *const gl::types::GLvoid,
        );
        gl::VertexAttribPointer(
            6,
            4,
            gl::FLOAT,
            gl::FALSE,
            std::mem::size_of::<cgmath::Matrix4::<f32>>() as gl::types::GLsizei,
            (std::mem::size_of::<f32>() * 12) as *const gl::types::GLvoid,
        );
        gl::VertexAttribDivisor(3, 1);
        gl::VertexAttribDivisor(4, 1);
        gl::VertexAttribDivisor(5, 1);
        gl::VertexAttribDivisor(6, 1);

        gl::BindBuffer(gl::ARRAY_BUFFER, 0);
    }

    unsafe {
        let err = gl::GetError();
        if err != 0 {
            panic!("GL error after initialization: {}", err);
        }
    }
    (program, vao, index_buffer, world_matrices)
}

#[allow(clippy::single_match, clippy::cognitive_complexity)]
pub fn main() {
    env_logger::init();

    let mut resolution_x: f32 = 512.0;
    let mut resolution_y: f32 = 512.0;

    let mut gui;
    match Gui::new(std::path::Path::new("data/fonts/CamingoCode-Bold.ttf")) {
        Ok(new_gui) => gui = new_gui,
        Err(err) => panic!("Error creating gui: {}", err),
    }
    let mut context;
    match Context::new(std::path::Path::new("data/style.ron")) {
        Ok(new_context) => context = new_context,
        Err(err) => panic!("Error creating context: {}", err),
    }

    let mut strings_tree: BTreeMap<String, String> = BTreeMap::new();
    strings_tree.insert("avg_fps".to_string(), "0".to_string());
    context.add_index_provider("strings", &mut strings_tree);

    let mut min_scale: f32 = 0.05;
    let mut max_scale: f32 = 0.1;
    let mut min_radius: f32 = 2.0;
    let mut max_radius: f32 = 9.0;
    let mut min_height: f32 = -2.5;
    let mut max_height: f32 = 2.5;
    let mut min_rotation_speed: f32 = 0.0005;
    let mut max_rotation_speed: f32 = 0.0075;
    let mut instances = 512.0;
    let mut asteroid_provider = DRIndexProvider::<f32>::new();
    asteroid_provider.add("min_scale", &mut min_scale);
    asteroid_provider.add("max_scale", &mut max_scale);
    asteroid_provider.add("min_radius", &mut min_radius);
    asteroid_provider.add("max_radius", &mut max_radius);
    asteroid_provider.add("min_height", &mut min_height);
    asteroid_provider.add("max_height", &mut max_height);
    asteroid_provider.add("min_rotation_speed", &mut min_rotation_speed);
    asteroid_provider.add("max_rotation_speed", &mut max_rotation_speed);
    asteroid_provider.add("instances", &mut instances);
    context.take_index_provider("asteroid", asteroid_provider);

    let mut camera_angle = 0;
    let mut camera_provider = DRIndexProvider::<i32>::new();
    camera_provider.add("angle", &mut camera_angle);
    context.take_index_provider("camera", camera_provider);

    let mut gui_status: Result<(), GuiError> =
        match gui.load_file(&mut context, std::path::Path::new("data/testfile.gui.ron")) {
            Ok(parse_result) => {
                if !parse_result.errors.is_empty() || !parse_result.warnings.is_empty() {
                    error!("{:}", parse_result);
                }
                gui.build(Rect::new(0.0, 0.0, resolution_x, resolution_y), &mut context);
                Ok(())
            }
            Err(err) => {
                error!("Error creating gui: {}", err);
                Err(err)
            }
        };

    let (file_watch_transmitter, file_watch_receiver) = channel();
    let mut watcher: RecommendedWatcher = Watcher::new(file_watch_transmitter, Duration::from_millis(50)).unwrap();
    watcher.watch("data", RecursiveMode::NonRecursive).unwrap();

    let sdl = sdl2::init().unwrap();
    let sdl_video = sdl.video().unwrap();
    let gl_attr = sdl_video.gl_attr();
    gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
    gl_attr.set_context_version(3, 3);
    let sdl_window = sdl_video
        .window("ruey SDL/GL", resolution_x as u32, resolution_y as u32)
        .resizable()
        .opengl()
        .build()
        .unwrap();

    let _gl_context = sdl_window.gl_create_context().unwrap();
    gl::load_with(|s| sdl_video.gl_get_proc_address(s) as *const std::os::raw::c_void);

    let (shader_program_2d, vao_2d, vertex_buffer_2d, index_buffer_2d) = mainutil::init_gl(gui.get_font_texture());
    let resolution_attrib =
        unsafe { gl::GetUniformLocation(shader_program_2d, CString::new("resolution").unwrap().as_c_str().as_ptr()) };
    unsafe {
        gl::UseProgram(shader_program_2d);
        gl::Uniform2f(resolution_attrib, resolution_x, resolution_y);
    }

    unsafe {
        gl::ClearColor(86.0 / 255.0, 120.0 / 255.0, 154.0 / 255.0, 1.0);
    }

    let (ico_vertices, ico_indicies) = generate_icosphere();
    let (ico_programme, ico_vao, ico_ibo, world_matrices) = shader_setup(&ico_vertices, &ico_indicies);

    let mut keys_down = std::collections::HashSet::<sdl2::keyboard::Keycode>::new();

    let mut random = rand::thread_rng();

    let mut asteroids = Vec::<Asteroid>::new();
    for i in 0..1024 {
        asteroids.push(Asteroid::new(random.gen_range(0.0, 1.0),
                                     random.gen_range(0.0, 1.0),
                                     random.gen_range(0.0, 1.0),
                                     random.gen_range(0.0, 1.0),
                                     random.gen_range(0.0, 1.0)));
    }

    let mut event_pump = sdl.event_pump().unwrap();
    let mut timer = Stopwatch::start_new();

    let mut time_accumulation = 0;
    let mut total_draw_time = 0;
    let mut frame_count = 0;
    let mut counter = 0.0_f32;
    'mainloop: loop {
        let mouse_state = sdl2::mouse::MouseState::new(&event_pump);
        for event in event_pump.poll_iter() {
            match event {
                sdl2::event::Event::Quit { .. } => break 'mainloop,
                sdl2::event::Event::Window { win_event, .. } => match win_event {
                    sdl2::event::WindowEvent::Resized(x, y) => {
                        resolution_x = x as f32;
                        resolution_y = y as f32;
                        unsafe {
                            gl::Viewport(0, 0, x, y);
                            gl::UseProgram(shader_program_2d);
                            gl::Uniform2f(resolution_attrib, x as f32, y as f32);
                        };
                        if gui_status.is_ok() {
                            gui.set_area(Rect::new(0.0, 0.0, resolution_x as f32, resolution_y as f32), &mut context);
                        }
                    }
                    _ => {}
                },
                sdl2::event::Event::KeyDown { keycode, .. } => {
                    let keycode = keycode.expect("No keycode in KeyDown event");
                    if !context.wants_keyboard_focus() && !keys_down.contains(&keycode) {
                        keys_down.insert(keycode);
                    }
                    if let Some(key) = translate_key(keycode) {
                        gui.key_down(&mut context, key)
                    }
                }
                sdl2::event::Event::KeyUp { keycode, .. } => {
                    let keycode = keycode.expect("No keycode in KeyUp event");
                    keys_down.remove(&keycode);
                    if let Some(key) = translate_key(keycode) {
                        gui.key_up(&mut context, key)
                    }
                }
                sdl2::event::Event::MouseButtonDown { x, y, mouse_btn, .. } => {
                    gui.mouse_down(&mut context, x, y, mouse_btn as i32)
                }
                sdl2::event::Event::MouseButtonUp { x, y, mouse_btn, .. } => {
                    gui.mouse_up(&mut context, x, y, mouse_btn as i32)
                }
                sdl2::event::Event::TextInput { text, .. } => {
                    gui.text_input(&mut context, &text);
                }
                _ => {}
            }
        }
        match file_watch_receiver.try_recv() {
            Ok(event) => match event {
                DebouncedEvent::Write(path) => {
                    if path.to_str().unwrap().contains("style.ron") {
                        info!("Reloading style from data/style.ron");
                        match context.load_style(std::path::Path::new("data/style.ron")) {
                            Ok(_) => {
                                debug!("Successfully reloaded style");
                            }
                            Err(err) => {
                                error!("Error reloading gui: {}", err);
                            }
                        }
                    } else if path.to_str().unwrap().contains("testfile.gui.ron") {
                        info!("Reloading GUI from data/testfile.gui.ron");
                        match gui.load_file(&mut context, std::path::Path::new("data/testfile.gui.ron")) {
                            Ok(parse_result) => {
                                let build_timer = Stopwatch::start_new();
                                context.reset_providers();
                                gui.build(Rect::new(0.0, 0.0, resolution_x, resolution_y), &mut context);
                                if !parse_result.errors.is_empty() || !parse_result.warnings.is_empty() {
                                    error!("{:}", parse_result);
                                }
                                debug!(
                                    "Building took {} ms",
                                    build_timer.elapsed().subsec_nanos() as f32 * 0.000_001
                                );
                                gui_status = Ok(());
                            }
                            Err(err) => {
                                error!("Error reloading gui: {}", err);
                                gui_status = Err(err);
                            }
                        }
                    }
                }
                _ => {}
            },
            Err(err) => {
                if err == TryRecvError::Disconnected {
                    panic!("File watcher channel disconnected!")
                }
            }
        }

        unsafe {
            gl::Disable(gl::SCISSOR_TEST);
            gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
            gl::Enable(gl::DEPTH_TEST);

            gl::UseProgram(ico_programme);
            gl::BindVertexArray(ico_vao);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, ico_ibo);

            let view_projection_matrix_location = gl::GetUniformLocation(ico_programme, CString::new("viewProjectionMatrix").unwrap().as_c_str().as_ptr());

            let mut matrices = Vec::<Matrix4<f32>>::new();
            for asteroid in &mut asteroids {
                let scale = min_scale + (max_scale - min_scale) * asteroid.scale_percent;
                let radius = min_radius + (max_radius - min_radius) * asteroid.radius_percent;
                let position = asteroid.position_percent * 3.141526 * 2.0;
                let height = min_height + (max_height - min_height) * asteroid.height_percent;

                let scale = Matrix4::from_scale(scale);
                let translation = Matrix4::from_translation(Vector3::new(position.sin() * radius, height, position.cos() * radius));
                matrices.push(translation * scale);
                asteroid.position_percent += min_rotation_speed + (max_rotation_speed - min_rotation_speed) * asteroid.position_increment;
            }
            gl::BindBuffer(gl::ARRAY_BUFFER, world_matrices);
            gl::BufferSubData(
                gl::ARRAY_BUFFER,
                0,
                (std::mem::size_of::<cgmath::Matrix4::<f32>>() as isize * asteroids.len() as isize) as gl::types::GLsizeiptr,
                matrices.as_ptr() as *const std::ffi::c_void,
            );

            let (camera_position, camera_look_position, camera_up) = match camera_angle {
                0 => (Point3::new(0.0, 7.0, -7.0), Point3::new(0.0, 0.0, 0.0), Vector3::new(0.0, 1.0, 0.0)),
                1 => (Point3::new(0.0, 0.0, 0.0), Point3::new(0.0, 0.0, 1.0), Vector3::new(0.0, 1.0, 0.0)),
                2 => (Point3::new(0.0, 7.0, 0.0), Point3::new(0.0, 0.0, 0.0), Vector3::new(0.0, 0.0, 1.0)),
                _ => (Point3::new(0.0, 7.0, -7.0), Point3::new(0.0, 0.0, 0.0), Vector3::new(0.0, 1.0, 0.0)),
            };
            
            let view_matrix: Matrix4<f32> = Matrix4::look_at(camera_position, camera_look_position, camera_up);
            let projection_matrix = cgmath::perspective(cgmath::Rad(2.0_f32), 1.0_f32, 1_f32, 100_f32);
            let view_projection_matrix = projection_matrix * view_matrix;
            gl::UniformMatrix4fv(view_projection_matrix_location, 1, gl::FALSE, view_projection_matrix.as_ptr() as *const gl::types::GLfloat);

            gl::DrawElementsInstanced(
                gl::TRIANGLES,
                ico_indicies.len() as gl::types::GLsizei,
                gl::UNSIGNED_INT,
                std::ptr::null(),
                instances as i32,
            );

            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
            gl::BindVertexArray(0);
            gl::UseProgram(0);
            
            let err = gl::GetError();
            if err != 0 {
                panic!("GL error after 3d drawing: {}", err);
            }
        }

        unsafe {
            gl::Enable(gl::BLEND);
            gl::Enable(gl::SCISSOR_TEST);
            gl::Disable(gl::DEPTH_TEST);
            gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
            gl::BlendEquation(gl::FUNC_ADD);

            gl::UseProgram(shader_program_2d);
            gl::BindVertexArray(vao_2d);
            gl::BindBuffer(gl::ARRAY_BUFFER, vertex_buffer_2d);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, index_buffer_2d);

            if gui_status.is_ok() {
                let draw_timer = Stopwatch::start_new();
                context.reset_providers();
                let (draw_list, overlay_draw_list) = gui.draw(mouse_state.x(), mouse_state.y(), &mut context);
                total_draw_time += draw_timer.elapsed().subsec_nanos();

                draw_draw_list(&draw_list, resolution_y);
                draw_draw_list(&overlay_draw_list, resolution_y);
            }

            let err = gl::GetError();
            if err != 0 {
                panic!("GL error after 2d drawing: {}", err);
            }
        }

        sdl_window.gl_swap_window();

        // Loosely 30 FPS
        let elapsed = timer.elapsed();
        if elapsed.subsec_millis() < 33 {
            let sleep_time = time::Duration::from_millis(33);
            if elapsed < sleep_time {
                std::thread::sleep(sleep_time - elapsed);
            }
        }
        time_accumulation += timer.elapsed().subsec_millis();
        frame_count += 1;
        if time_accumulation >= 1000 {
            *strings_tree.get_mut("avg_fps").unwrap() =
                (total_draw_time as f32 / frame_count as f32 * 0.000_001).to_string();
            total_draw_time = 0;
            time_accumulation = 0;
            frame_count = 0;
        }
        counter += timer.elapsed().subsec_millis() as f32 * 0.001;
        timer.restart();
    }
}

fn draw_draw_list(draw_list: &DrawList, resolution_y: f32) {
    if !draw_list.commands.is_empty() {
        unsafe {
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (draw_list.vertices.len() * std::mem::size_of::<Vertex>()) as gl::types::GLsizeiptr,
                draw_list.vertices.as_ptr() as *const gl::types::GLvoid,
                gl::DYNAMIC_DRAW,
            );
            gl::BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                (draw_list.indices.len() * std::mem::size_of::<u32>()) as gl::types::GLsizeiptr,
                draw_list.indices.as_ptr() as *const gl::types::GLvoid,
                gl::DYNAMIC_DRAW,
            );

            for command in &draw_list.commands {
                let clip_rect = command.clip_rect;
                gl::Scissor(
                    clip_rect.min.x as gl::types::GLint,
                    (resolution_y - clip_rect.min.y - clip_rect.size().y) as gl::types::GLint,
                    clip_rect.size().x as gl::types::GLsizei,
                    clip_rect.size().y as gl::types::GLsizei,
                );
                gl::DrawElements(
                    gl::TRIANGLES,
                    command.index_count as gl::types::GLsizei,
                    gl::UNSIGNED_INT,
                    (command.index_offset * std::mem::size_of::<u32>() as u32) as *const std::os::raw::c_void,
                );
            }
        }
    }
}

fn translate_key(keycode: sdl2::keyboard::Keycode) -> Option<GuiKey> {
    match keycode {
        sdl2::keyboard::Keycode::Up => Some(GuiKey::UP),
        sdl2::keyboard::Keycode::Down => Some(GuiKey::DOWN),
        sdl2::keyboard::Keycode::Left => Some(GuiKey::LEFT),
        sdl2::keyboard::Keycode::Right => Some(GuiKey::RIGHT),
        sdl2::keyboard::Keycode::Return => Some(GuiKey::ENTER),
        sdl2::keyboard::Keycode::Backspace => Some(GuiKey::BACKSPACE),
        _ => None,
    }
}

fn generate_icosphere() -> (Vec<Vertex3D>, Vec<u32>) {
    let dist = (1.0_f32 + 5.0_f32.sqrt()) * 0.5_f32;
    let vertices: [Vector3<f32>; 12] = [
        Vector3::new(-1.0, dist, 0.0),
        Vector3::new(1.0, dist, 0.0),
        Vector3::new(-1.0, -dist, 0.0),
        Vector3::new(1.0, -dist, 0.0),
        Vector3::new(0.0, -1.0, dist),
        Vector3::new(0.0, 1.0, dist),
        Vector3::new(0.0, -1.0, -dist),
        Vector3::new(0.0, 1.0, -dist),
        Vector3::new(dist, 0.0, -1.0),
        Vector3::new(dist, 0.0, 1.0),
        Vector3::new(-dist, 0.0, -1.0),
        Vector3::new(-dist, 0.0, 1.0),
    ];
    let indices: [u32; 60] = [
        0, 11, 5,
        0, 5, 1,
        0, 1, 7,
        0, 7, 10,
        0, 10, 11,
        1, 5, 9,
        5, 11, 4,
        11, 10, 2,
        10, 7, 6,
        7, 1, 8,
        3, 9, 4,
        3, 4, 2,
        3, 2, 6,
        3, 6, 8,
        3, 8, 9,
        4, 9, 5,
        2, 4, 11,
        6, 2, 10,
        8, 6, 7,
        9, 8, 1,
    ];
    let mut triangle_vertices = Vec::<Vertex3D>::new();
    let mut triangle_indices = Vec::<u32>::new();

    indices.chunks(3).for_each(|indices| {
        let v0 = vertices[indices[0] as usize];
        let v1 = vertices[indices[1] as usize];
        let v2 = vertices[indices[2] as usize];
        let normal = (v1 - v0).cross(v2 - v0).normalize();

        triangle_vertices.push(Vertex3D::new(v0, normal, Vector3::new(1.0, 1.0, 1.0)));
        triangle_vertices.push(Vertex3D::new(v1, normal, Vector3::new(1.0, 1.0, 1.0)));
        triangle_vertices.push(Vertex3D::new(v2, normal, Vector3::new(1.0, 1.0, 1.0)));

        let index_offset = triangle_indices.len() as u32;
        triangle_indices.push(index_offset);
        triangle_indices.push(index_offset + 1);
        triangle_indices.push(index_offset + 2);
    });

    (triangle_vertices, triangle_indices)
}