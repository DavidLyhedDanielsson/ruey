use color::Color;

#[derive(Clone, Copy, Debug, PartialEq)]
#[cfg_attr(test, derive(Default))]
pub struct HSVColor {
    pub hue: f32,
    pub saturation: f32,
    pub value: f32,
}

impl HSVColor {
    pub fn new(hue: f32, saturation: f32, value: f32) -> HSVColor {
        HSVColor { hue, saturation, value }
    }

    #[allow(clippy::float_cmp)]
    pub fn new_rgb(r: f32, g: f32, b: f32) -> HSVColor {
        let r = r.max(0.0).min(1.0);
        let g = g.max(0.0).min(1.0);
        let b = b.max(0.0).min(1.0);

        let max = r.max(g.max(b));
        let min = r.min(g.min(b));

        let range = max - min;
        let mut hue = if range < 0.00001 {
            0.0
        } else if max == r {
            60.0 * (g - b) / range
        } else if max == g {
            60.0 * (2.0 + (b - r) / range)
        } else {
            60.0 * (4.0 + (r - g) / range)
        };

        if hue < 0.0 {
            hue += 360.0;
        } else if hue > 360.0 {
            hue -= 360.0;
        }

        let saturation = if max < 0.00001 { 0.0 } else { range / max };
        let value = max;

        HSVColor { hue, saturation, value }
    }

    pub fn to_rgb(&self) -> Color {
        self.to_rgba(1.0)
    }

    pub fn to_rgba(&self, alpha: f32) -> Color {
        let hue_to_color = |n: f32| {
            let k = (n + self.hue / 60.0) % 6.0;
            let k = k.min(4.0 - k).min(1.0).max(0.0);
            self.value - self.value * self.saturation * k
        };
        let r = hue_to_color(5.0);
        let g = hue_to_color(3.0);
        let b = hue_to_color(1.0);
        Color::new(r, g, b, alpha)
    }
}
