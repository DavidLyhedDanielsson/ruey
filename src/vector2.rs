use std::ops::{Add, AddAssign, Mul, Sub};

#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub struct Vector2 {
    pub x: f32,
    pub y: f32,
}

impl Vector2 {
    pub fn new(x: f32, y: f32) -> Vector2 {
        Vector2 { x, y }
    }
    pub fn new_replicate(val: f32) -> Vector2 {
        Vector2 { x: val, y: val }
    }
    pub fn new_zero() -> Vector2 {
        Vector2 { x: 0.0, y: 0.0 }
    }
    pub fn new_min() -> Vector2 {
        Vector2 {
            x: std::f32::MIN,
            y: std::f32::MIN,
        }
    }
    pub fn new_max() -> Vector2 {
        Vector2 {
            x: std::f32::MAX,
            y: std::f32::MAX,
        }
    }
    pub fn new_offset(self, x: f32, y: f32) -> Vector2 {
        Vector2::new(self.x + x, self.y + y)
    }
    #[allow(clippy::float_cmp)]
    pub fn equals(self, other: Vector2) -> bool {
        self.x == other.x && self.y == other.y
    }
    pub fn almost_equal(self, other: Vector2) -> bool {
        (self.x - other.x).abs() < 0.1 && (self.y - other.y).abs() < 0.1
    }
    pub fn dot(self, other: Vector2) -> f32 {
        self.x * other.x + self.y * other.y
    }
    pub fn min(self, other: Vector2) -> Vector2 {
        Vector2::new(self.x.min(other.x), self.y.min(other.y))
    }
    pub fn max(self, other: Vector2) -> Vector2 {
        Vector2::new(self.x.max(other.x), self.y.max(other.y))
    }
    pub fn half(self) -> Vector2 {
        Vector2::new(self.x * 0.5, self.y * 0.5)
    }
}

impl Add for Vector2 {
    type Output = Vector2;
    fn add(self, rhs: Vector2) -> Vector2 {
        Vector2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl<'a, 'b> Add<&'b Vector2> for &'a Vector2 {
    type Output = Vector2;
    fn add(self, rhs: &'b Vector2) -> Vector2 {
        Vector2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl AddAssign for Vector2 {
    fn add_assign(&mut self, rhs: Vector2) {
        *self = Vector2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Sub for Vector2 {
    type Output = Vector2;
    fn sub(self, rhs: Vector2) -> Vector2 {
        Vector2 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl<'a, 'b> Sub<&'b Vector2> for &'a Vector2 {
    type Output = Vector2;
    fn sub(self, rhs: &'b Vector2) -> Vector2 {
        Vector2 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl Mul<Vector2> for Vector2 {
    type Output = Vector2;
    fn mul(self, rhs: Vector2) -> Vector2 {
        Vector2 {
            x: self.x * rhs.x,
            y: self.y * rhs.y,
        }
    }
}

impl Mul<f32> for Vector2 {
    type Output = Vector2;
    fn mul(self, rhs: f32) -> Vector2 {
        Vector2 {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}
