extern crate env_logger;
#[macro_use]
extern crate log;
extern crate gl;
extern crate cgmath;
extern crate notify;
extern crate ruey;
extern crate sdl2;
extern crate stopwatch;
extern crate rand;

mod example;

fn main() {
    example::asteroids::main()
}