extern crate regex;

pub mod color;
pub mod elements;

use self::elements::button::Button;
use self::elements::checkbox::Checkbox;
use self::elements::dropdown::Dropdown;
use self::elements::inputtext::InputText;
use self::elements::scrollbar::Scrollbar;
use self::elements::slider::Slider;
use self::elements::text::Text;
use self::elements::window::Window;
use self::elements::radiobutton::RadioButton;
use ron::de::from_str;

use serde::Deserialize;

use self::regex::Regex;
use std::collections::BTreeMap;

#[derive(Copy, Clone, Deserialize, Debug, PartialEq)]
#[cfg_attr(test, serde(default), derive(Default))]
pub struct Style {
    pub window: Window,
    pub dropdown: Dropdown,
    pub input_text: InputText,
    pub button: Button,
    pub scrollbar: Scrollbar,
    pub text: Text,
    pub checkbox: Checkbox,
    pub slider: Slider,
    pub radio_button: RadioButton,
}

impl Style {
    pub fn new(path: &std::path::Path) -> Result<Self, ron::de::Error> {
        let raw_ron = std::fs::read_to_string(path).unwrap();
        Style::new_from_string(raw_ron)
    }

    pub fn new_from_string(data: String) -> Result<Self, ron::de::Error> {
        let data = Style::strip_single_comments(data);
        let data = Style::strip_multi_comments(data);

        let (data, variables) = Style::extract_and_strip_variables(data);
        let data = Style::replace_variables(data, variables);

        from_str(&data)
    }

    fn strip_single_comments(data: String) -> String {
        let regex = Regex::new(r#"//.*\n"#).unwrap();
        regex.replace_all(&data, "\n").to_string()
    }

    fn strip_multi_comments(data: String) -> String {
        let regex = Regex::new(r#"(?s)/\*.*?\*/"#).unwrap();
        let mut stripped_data = data.clone();
        let mut erased_len = 0;
        for capture in regex.find_iter(&data) {
            let start = capture.start() - erased_len;
            let end = capture.end() - erased_len;
            let cap = capture.as_str().to_string();
            let new_line_count = cap.matches('\n').count();
            stripped_data.replace_range(start..end, &"\n".repeat(new_line_count));
            erased_len += end - start - new_line_count;
        }
        stripped_data
    }

    fn extract_and_strip_variables(data: String) -> (String, BTreeMap<String, String>) {
        // Strip here actually means comment out. That way lines numbers are preserved
        let mut variables: BTreeMap<String, String> = BTreeMap::new();

        let regex = Regex::new(r#"(\$[a-zA-Z0-9_]+?)\s*:\s*(.*),"#).unwrap();
        for capture in regex.captures_iter(&data) {
            if capture[2].contains('$') {
                let value = variables.get(&capture[2]).unwrap().to_string();
                variables.insert(capture[1].to_string(), value);
            } else {
                variables.insert(capture[1].to_string(), capture[2].to_string());
            }
        }

        let regex = Regex::new(r#"(?s)(variables:.*?}[,]?)"#).unwrap();
        let replaced_string = regex.replace(&data, "/*$1*/").to_string();
        (replaced_string, variables)
    }

    fn replace_variables(mut data: String, variables: BTreeMap<String, String>) -> String {
        for (key, value) in variables {
            // Escape dollar sign
            let regex = Regex::new(&("\\".to_string() + &key + "(,|\n)")).unwrap();
            data = regex.replace_all::<&str>(&data, (value + "$1").as_ref()).to_string();
        }
        data
    }
}

#[cfg(test)]
mod tests {
    use color::Color;
    use style::Style;

    #[test]
    fn test_strip_single_comments() {
        let expected = r#"
        (
            not_a_comment: 123,
            
            
            no_comment: 1.0, 
        )
        "#;
        let stripped = Style::strip_single_comments(
            r#"
        (
            not_a_comment: 123,
            // This is a comment though!
            //Also a comment
            no_comment: 1.0, // But this is
        )
        "#
            .to_string(),
        );
        assert_eq!(expected, stripped);
    }

    #[test]
    fn test_strip_multi_comments() {
        let expected = "
        (
            not_a_comment: 123,
            
            \n\n
            no_comment: 1.0,
            of_a_variable: 123,
        )
        ";
        let stripped = Style::strip_multi_comments(
            r#"
        /**/(
            not_a_comment: 123,
            /* Single line */
            /* Multiple
            Lines in
            This comment */
            no_comment: 1.0,
            /*In front*/of_a_variable: 123,
        )
        "#
            .to_string(),
        );
        assert_eq!(expected, stripped);
    }

    mod test_extract_and_strip_variables {
        use super::*;

        #[test]
        fn test_with_comma() {
            let expected_ron = r#"
            (
                /*variables: {
                    $one: 1,
                    $two: 2.0,
                    $three: "three",
                    $four: (1, 2),
                },*/
                window: (
                    one: $one,
                    two: $two,
                    three: $three,
                    four: 4.0,
                )
            )
            "#;
            let (stripped, variables) = Style::extract_and_strip_variables(
                r#"
            (
                variables: {
                    $one: 1,
                    $two: 2.0,
                    $three: "three",
                    $four: (1, 2),
                },
                window: (
                    one: $one,
                    two: $two,
                    three: $three,
                    four: 4.0,
                )
            )
            "#
                .to_string(),
            );
            assert_eq!(expected_ron, stripped);
            assert_eq!(variables.get("$one").unwrap(), "1");
            assert_eq!(variables.get("$two").unwrap(), "2.0");
            assert_eq!(variables.get("$three").unwrap(), "\"three\"");
            assert_eq!(variables.get("$four").unwrap(), "(1, 2)");
        }

        #[test]
        fn test_without_comma() {
            let expected_ron = r#"
            (
                window: (
                    one: $one,
                    two: $two,
                    three: $three,
                    four: 4.0,
                ),
                /*variables: {
                    $one: 1,
                    $two: 2.0,
                    $three: "three",
                    $four: (1, 2),
                }*/
            )
            "#;
            let (stripped, variables) = Style::extract_and_strip_variables(
                r#"
            (
                window: (
                    one: $one,
                    two: $two,
                    three: $three,
                    four: 4.0,
                ),
                variables: {
                    $one: 1,
                    $two: 2.0,
                    $three: "three",
                    $four: (1, 2),
                }
            )
            "#
                .to_string(),
            );
            assert_eq!(expected_ron, stripped);
            assert_eq!(variables.get("$one").unwrap(), "1");
            assert_eq!(variables.get("$two").unwrap(), "2.0");
            assert_eq!(variables.get("$three").unwrap(), "\"three\"");
            assert_eq!(variables.get("$four").unwrap(), "(1, 2)");
        }
    }

    #[allow(clippy::float_cmp)]
    #[test]
    fn test_replacement() {
        let ron = r#"
        (
            variables: {
                $test: Color(0xFF, 0x0, 0xFF, 0xFF),
                $test_variable: Color(0xFF, 0xFF, 0xFF, 0xFF),
                $two: 2.0,
            },
            window: (
                element_background_color: $test,
                title_bar_color: $test_variable,
                title_bar_text_color: $test,
                title_bar_padding: $two,
                title_bar_title_padding: 123.0,
            )
        )
        "#;
        let style = Style::new_from_string(ron.to_string()).unwrap();
        assert_eq!(
            style.window.element_background_color,
            Color::newb(0xFF, 0x0, 0xFF, 0xFF)
        );
        assert_eq!(style.window.title_bar_color, Color::newb(0xFF, 0xFF, 0xFF, 0xFF));
        assert_eq!(style.window.title_bar_text_color, Color::newb(0xFF, 0x0, 0xFF, 0xFF));
        assert_eq!(style.window.title_bar_padding, 2.0);
        assert_eq!(style.window.title_bar_title_padding, 123.0);
    }

    #[should_panic(
        expected = "called `Result::unwrap()` on an `Err` value: Parser(ExpectedFloat, Position { col: 36, line: 7 })"
    )]
    #[test]
    fn test_missing_variable_sign() {
        let ron = r#"
        (
            variables: {
                $two: 2.0,
            },
            window: (
                title_bar_padding: two,
            )
        )
        "#;
        Style::new_from_string(ron.to_string()).unwrap();
    }

    #[test]
    fn test_replace_variables() {
        let expected = r#"
        (
            window: (
                one: 1,
                other: "Some string",
                third: 3.0,
                four: 4.0,
            )
        )
        "#
        .to_string();

        let ron = r#"
        (
            window: (
                one: $one,
                other: $something,
                third: 3.0,
                four: $four,
            )
        )
        "#
        .to_string();

        let mut variables = std::collections::BTreeMap::<String, String>::new();
        variables.insert("$one".to_string(), "1".to_string());
        variables.insert("$something".to_string(), "\"Some string\"".to_string());
        variables.insert("$four".to_string(), "4.0".to_string());

        assert_eq!(expected, Style::replace_variables(ron, variables));
    }
}
