use color::Color;
use context;
use rect::Rect;
use serde::Deserialize;
use vector2::Vector2;

#[derive(Copy, Clone, Deserialize, Debug, PartialEq)]
#[cfg_attr(test, serde(default), derive(Default))]
pub struct Window {
    pub element_background_color: Color,
    pub title_bar_color: Color,
    pub title_bar_text_color: Color,
    pub resize_highlight_color: Color,
    pub split_border_color: Color,
    pub focused_tab_color: Color,
    pub unfocused_tab_color: Color,
    pub focused_docking_zone_color: Color,
    pub window_docking_color: Color,

    pub title_bar_padding: f32,
    pub title_bar_title_padding: f32,
    pub border_thickness: f32,
    pub border_padding: f32,

    pub text_size: f32,
}

impl Window {
    pub fn add_to_provider(
        &mut self,
        color_provider: &mut context::commitprovider::CommitProvider<Color>,
        value_provider: &mut context::commitprovider::CommitProvider<f32>,
    ) {
        value_provider.add("title_bar_padding", &mut self.title_bar_padding);
        value_provider.add("title_bar_title_padding", &mut self.title_bar_title_padding);
        value_provider.add("border_thickness", &mut self.border_thickness);
        value_provider.add("border_padding", &mut self.border_padding);

        color_provider.add("element_background_color", &mut self.element_background_color);
        color_provider.add("title_bar_color", &mut self.title_bar_color);
        color_provider.add("title_bar_text_color", &mut self.title_bar_text_color);
        color_provider.add("resize_highlight_color", &mut self.resize_highlight_color);
        color_provider.add("split_border_color", &mut self.split_border_color);
        color_provider.add("focused_tab_color", &mut self.focused_tab_color);
        color_provider.add("unfocused_tab_color", &mut self.unfocused_tab_color);
        color_provider.add("focused_docking_zone_color", &mut self.focused_docking_zone_color);
        color_provider.add("window_docking_color", &mut self.window_docking_color);
    }

    pub fn title_bar_height(&self) -> f32 {
        self.text_size + self.title_bar_padding * 2.0
    }

    pub fn title_text_offset(&self) -> Vector2 {
        Vector2::new(self.title_bar_title_padding, self.title_bar_padding)
    }

    pub fn pad_area(&self, area: Rect) -> Rect {
        Rect::new(
            area.min.x + self.border_padding,
            area.min.y + self.border_padding,
            area.max.x - self.border_padding,
            area.max.y - self.border_padding,
        )
    }

    pub fn border_pad_area(&self, area: Rect) -> Rect {
        Rect::new(
            area.min.x + self.border_thickness + self.border_padding,
            area.min.y + self.border_thickness + self.border_padding,
            area.max.x - self.border_thickness - self.border_padding,
            area.max.y - self.border_thickness - self.border_padding,
        )
    }

    pub fn border_area(&self, area: Rect) -> Rect {
        Rect::new(
            area.min.x + self.border_thickness,
            area.min.y + self.border_thickness,
            area.max.x - self.border_thickness,
            area.max.y - self.border_thickness,
        )
    }

    pub fn unpad_area(&self, area: Rect) -> Rect {
        Rect::new(
            area.min.x - self.border_padding,
            area.min.y - self.border_padding,
            area.max.x + self.border_padding,
            area.max.y + self.border_padding,
        )
    }

    pub fn unborder_area(&self, area: Rect) -> Rect {
        Rect::new(
            area.min.x - self.border_thickness,
            area.min.y - self.border_thickness,
            area.max.x + self.border_thickness,
            area.max.y + self.border_thickness,
        )
    }

    pub fn unpad_unborder_area(&self, area: Rect) -> Rect {
        Rect::new(
            area.min.x - self.border_thickness - self.border_padding,
            area.min.y - self.border_thickness - self.border_padding,
            area.max.x + self.border_thickness + self.border_padding,
            area.max.y + self.border_thickness + self.border_padding,
        )
    }

    pub fn tab_tabs_area(&self, area: Rect) -> Rect {
        Rect::new(area.min.x, area.min.y, area.max.x, area.min.y + self.title_bar_height())
    }
    pub fn tab_element_area(&self, area: Rect) -> Rect {
        Rect::new(
            area.min.x + self.border_thickness,
            area.min.y + self.title_bar_height() + self.border_thickness,
            area.max.x - self.border_thickness,
            area.max.y - self.border_thickness,
        )
    }
    pub fn tab_left_area(&self, tab_area: Rect, tab_index: usize) -> Rect {
        if tab_index == 0 {
            Rect::new_size_vector(tab_area.min, tab_area.size() * Vector2::new(0.5, 1.0))
        } else {
            Rect::new_size_vector(
                tab_area.min - Vector2::new(tab_area.size().x * 0.5, 0.0),
                tab_area.size(),
            )
        }
    }
    pub fn tab_right_area(&self, tab_area: Rect, tab_index: usize, max_index: usize) -> Rect {
        if tab_index == max_index {
            let half_width = tab_area.size() * Vector2::new(0.5, 1.0);
            Rect::new_size_vector(tab_area.min + Vector2::new(half_width.x, 0.0), half_width)
        } else {
            Rect::new_size_vector(
                tab_area.min + Vector2::new(tab_area.size().x * 0.5, 0.0),
                tab_area.size(),
            )
        }
    }

    pub fn dock_north_zone(&self, area: Rect) -> Rect {
        Rect::new_size(area.min.x, area.min.y, area.size().x, self.dock_zone_size())
    }
    pub fn dock_south_zone(&self, area: Rect) -> Rect {
        Rect::new_size(
            area.min.x,
            area.max.y - self.dock_zone_size(),
            area.size().x,
            self.dock_zone_size(),
        )
    }
    pub fn dock_west_zone(&self, area: Rect) -> Rect {
        Rect::new_size(area.min.x, area.min.y, self.dock_zone_size(), area.size().y)
    }
    pub fn dock_east_zone(&self, area: Rect) -> Rect {
        Rect::new_size(
            area.max.x - self.dock_zone_size(),
            area.min.y,
            self.dock_zone_size(),
            area.size().y,
        )
    }
    fn dock_zone_size(&self) -> f32 {
        self.title_bar_height() * 0.5
    }

    pub fn split_element_area(&self, area: Rect) -> Rect {
        self.border_pad_area(Rect::new(
            area.min.x,
            area.min.y + self.title_bar_height(),
            area.max.x,
            area.max.y,
        ))
    }
}

#[allow(non_upper_case_globals, clippy::float_cmp)]
#[cfg(test)]
mod tests {
    use color::Color;
    use lazy_static::*;
    use style::elements::window::Window;
    use testutil::test_util::*;

    // These should be picked in such a way that adding them together produces a somewhat unique value so
    // that, for instance, title_bar_padding isn't used instead of title_bar_title_padding in the method
    const TITLE_BAR_PADDING: f32 = 1.0;
    const TITLE_BAR_TITLE_PADDING: f32 = 5.0;
    const BORDER_THICKNESS: f32 = 13.0;
    const BORDER_PADDING: f32 = 19.0;
    const TEXT_SIZE: f32 = 24.0;

    const TITLE_BAR_HEIGHT: f32 = TITLE_BAR_PADDING * 2.0 + TEXT_SIZE;

    lazy_static! {
        static ref window: Window = Window {
            element_background_color: Color::newb(0x0, 0x0, 0x0, 0x0),
            title_bar_color: Color::newb(0x01, 0x0, 0x0, 0x0),
            title_bar_text_color: Color::newb(0x02, 0x0, 0x0, 0x0),
            resize_highlight_color: Color::newb(0x03, 0x0, 0x0, 0x0),
            split_border_color: Color::newb(0x04, 0x0, 0x0, 0x0),
            focused_tab_color: Color::newb(0x05, 0x0, 0x0, 0x0),
            unfocused_tab_color: Color::newb(0x06, 0x0, 0x0, 0x0),
            focused_docking_zone_color: Color::newb(0x07, 0x0, 0x0, 0x0),
            window_docking_color: Color::newb(0x08, 0x0, 0x0, 0x0),

            title_bar_padding: TITLE_BAR_PADDING,
            title_bar_title_padding: TITLE_BAR_TITLE_PADDING,
            border_thickness: BORDER_THICKNESS,
            border_padding: BORDER_PADDING,

            text_size: TEXT_SIZE,
        };
    }

    #[test]
    fn test_title_bar_height() {
        assert_eq!((*window).title_bar_height(), TITLE_BAR_HEIGHT)
    }

    #[test]
    fn test_title_bar_offset() {
        assert_eq!((*window).title_text_offset(), Vector2::new(5.0, 1.0))
    }

    #[test]
    fn test_pad_area() {
        let expected = Rect::new(
            (*area).min.x + BORDER_PADDING,
            (*area).min.y + BORDER_PADDING,
            (*area).max.x - BORDER_PADDING,
            (*area).max.y - BORDER_PADDING,
        );
        let p_area = (*window).pad_area(*area);
        assert!(
            expected.almost_equal(p_area),
            "Expected\n{:?}\ngot\n{:?}",
            expected,
            p_area
        );
    }

    #[test]
    fn test_border_pad_area() {
        let expected = Rect::new(
            (*area).min.x + BORDER_THICKNESS + BORDER_PADDING,
            (*area).min.y + BORDER_THICKNESS + BORDER_PADDING,
            (*area).max.x - BORDER_THICKNESS - BORDER_PADDING,
            (*area).max.y - BORDER_THICKNESS - BORDER_PADDING,
        );
        let bp_area = (*window).border_pad_area(*area);
        assert!(
            expected.almost_equal(bp_area),
            "Expected\n{:?}\ngot\n{:?}",
            expected,
            bp_area
        );
    }

    #[test]
    fn test_border_area() {
        let expected = Rect::new(
            (*area).min.x + BORDER_THICKNESS,
            (*area).min.y + BORDER_THICKNESS,
            (*area).max.x - BORDER_THICKNESS,
            (*area).max.y - BORDER_THICKNESS,
        );
        let b_area = (*window).border_area(*area);
        assert!(
            expected.almost_equal(b_area),
            "Expected\n{:?}\ngot\n{:?}",
            expected,
            b_area
        );
    }

    #[test]
    fn test_unpad_area() {
        let expected = Rect::new(
            (*area).min.x - BORDER_PADDING,
            (*area).min.y - BORDER_PADDING,
            (*area).max.x + BORDER_PADDING,
            (*area).max.y + BORDER_PADDING,
        );
        let up_area = (*window).unpad_area(*area);
        assert!(
            expected.almost_equal(up_area),
            "Expected\n{:?}\ngot\n{:?}",
            expected,
            up_area
        );
    }

    #[test]
    fn test_unborder_area() {
        let expected = Rect::new(
            (*area).min.x - BORDER_THICKNESS,
            (*area).min.y - BORDER_THICKNESS,
            (*area).max.x + BORDER_THICKNESS,
            (*area).max.y + BORDER_THICKNESS,
        );
        let ub_area = (*window).unborder_area(*area);
        assert!(
            expected.almost_equal(ub_area),
            "Expected\n{:?}\ngot\n{:?}",
            expected,
            ub_area
        );
    }

    #[test]
    fn test_unpad_unborder_area() {
        let expected = Rect::new(
            (*area).min.x - BORDER_PADDING - BORDER_THICKNESS,
            (*area).min.y - BORDER_PADDING - BORDER_THICKNESS,
            (*area).max.x + BORDER_PADDING + BORDER_THICKNESS,
            (*area).max.y + BORDER_PADDING + BORDER_THICKNESS,
        );
        let upb_area = (*window).unpad_unborder_area(*area);
        assert!(
            expected.almost_equal(upb_area),
            "Expected\n{:?}\ngot\n{:?}",
            expected,
            upb_area
        );
    }

    #[test]
    fn test_tab_tabs_area() {
        let expected = Rect::new(
            (*area).min.x,
            (*area).min.y,
            (*area).max.x,
            (*area).min.y + TITLE_BAR_HEIGHT,
        );
        let tt_area = (*window).tab_tabs_area(*area);
        assert!(
            expected.almost_equal(tt_area),
            "Expected\n{:?}\ngot\n{:?}",
            expected,
            tt_area
        );
    }

    mod test_tab_left_area {
        use super::*;
        #[test]
        fn test_leftmost_left_area() {
            let expected = Rect::new(
                (*area).min.x,
                (*area).min.y,
                (*area).min.x + (*area).size().x * 0.5,
                (*area).max.y,
            );
            let left_area = (*window).tab_left_area(*area, 0);
            assert!(
                expected.almost_equal(left_area),
                "Expected\n{:?}\ngot\n{:?}",
                expected,
                left_area
            );
        }
        #[test]
        fn test_left_area() {
            let expected = Rect::new(
                (*area).min.x - (*area).size().x * 0.5,
                (*area).min.y,
                (*area).min.x + (*area).size().x * 0.5,
                (*area).max.y,
            );
            let left_area = (*window).tab_left_area(*area, 1);
            assert!(
                expected.almost_equal(left_area),
                "Expected\n{:?}\ngot\n{:?}",
                expected,
                left_area
            );
        }
    }

    mod test_tab_right_area {
        use super::*;
        #[test]
        fn test_rightmost_right_area() {
            let expected = Rect::new(
                (*area).min.x + (*area).size().x * 0.5,
                (*area).min.y,
                (*area).max.x,
                (*area).max.y,
            );
            let right_area = (*window).tab_right_area(*area, 2, 2);
            assert!(
                expected.almost_equal(right_area),
                "Expected\n{:?}\ngot\n{:?}",
                expected,
                right_area
            );
        }
        #[test]
        fn test_right_area() {
            let expected = Rect::new(
                (*area).min.x + (*area).size().x * 0.5,
                (*area).min.y,
                (*area).max.x + (*area).size().x * 0.5,
                (*area).max.y,
            );
            let right_area = (*window).tab_right_area(*area, 0, 2);
            assert!(
                expected.almost_equal(right_area),
                "Expected\n{:?}\ngot\n{:?}",
                expected,
                right_area
            );
        }
    }

    #[test]
    fn test_north_zone() {
        let expected = Rect::new(
            (*area).min.x,
            (*area).min.y,
            (*area).max.x,
            (*area).min.y + TITLE_BAR_HEIGHT * 0.5,
        );
        let north_zone = (*window).dock_north_zone(*area);
        assert!(
            expected.almost_equal(north_zone),
            "Expected\n{:?}\ngot\n{:?}",
            expected,
            north_zone
        );
    }

    #[test]
    fn test_south_zone() {
        let expected = Rect::new(
            (*area).min.x,
            (*area).max.y - TITLE_BAR_HEIGHT * 0.5,
            (*area).max.x,
            (*area).max.y,
        );
        let south_zone = (*window).dock_south_zone(*area);
        assert!(
            expected.almost_equal(south_zone),
            "Expected\n{:?}\ngot\n{:?}",
            expected,
            south_zone
        );
    }

    #[test]
    fn test_west_zone() {
        let expected = Rect::new(
            (*area).min.x,
            (*area).min.y,
            (*area).min.x + TITLE_BAR_HEIGHT * 0.5,
            (*area).max.y,
        );
        let west_zone = (*window).dock_west_zone(*area);
        assert!(
            expected.almost_equal(west_zone),
            "Expected\n{:?}\ngot\n{:?}",
            expected,
            west_zone
        );
    }

    #[test]
    fn test_east_zone() {
        let expected = Rect::new(
            (*area).max.x - TITLE_BAR_HEIGHT * 0.5,
            (*area).min.y,
            (*area).max.x,
            (*area).max.y,
        );
        let east_zone = (*window).dock_east_zone(*area);
        assert!(
            expected.almost_equal(east_zone),
            "Expected\n{:?}\ngot\n{:?}",
            expected,
            east_zone
        );
    }

    #[test]
    fn test_split_element_area() {
        let expected = Rect::new(
            (*area).min.x + BORDER_THICKNESS + BORDER_PADDING,
            (*area).min.y + BORDER_THICKNESS + BORDER_PADDING + TITLE_BAR_HEIGHT,
            (*area).max.x - BORDER_THICKNESS - BORDER_PADDING,
            (*area).max.y - BORDER_THICKNESS - BORDER_PADDING,
        );
        let element_area = (*window).split_element_area(*area);
        assert!(
            expected.almost_equal(element_area),
            "Expected\n{:?}\ngot\n{:?}",
            expected,
            element_area
        );
    }
}
