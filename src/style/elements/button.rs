use serde::Deserialize;

use color::Color;

#[derive(Copy, Clone, Deserialize, Debug, PartialEq)]
#[cfg_attr(test, serde(default), derive(Default))]
pub struct Button {
    pub background_color: Color,
    pub background_color_hovered: Color,
    pub background_color_clicked: Color,
    pub text_color: Color,
    pub text_size: f32,
}
