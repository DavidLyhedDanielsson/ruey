use serde::Deserialize;

use color::Color;
use rect::Rect;

#[derive(Copy, Clone, Deserialize, Debug, PartialEq)]
#[cfg_attr(test, serde(default), derive(Default))]
pub struct Checkbox {
    pub background_color: Color,
    pub background_color_hovered: Color,
    pub background_color_clicked: Color,
    pub text_color: Color,

    pub check_padding: f32,
    pub check_thickness: f32,
    pub check_box_padding: f32,
    pub check_box_border_thickness: f32,
    pub check_box_text_padding: f32,
    pub text_vertical_offset: f32,
}

impl Checkbox {
    pub fn check_area(&self, area: Rect) -> Rect {
        Rect::new_size(
            area.min.x + self.check_box_padding,
            area.min.y + self.check_box_padding,
            area.size().y - self.check_box_padding * 2.0,
            area.size().y - self.check_box_padding * 2.0,
        )
    }
    pub fn bordered_check_area(&self, area: Rect) -> Rect {
        Rect::new_size(
            area.min.x + self.check_box_padding + self.check_box_border_thickness,
            area.min.y + self.check_box_padding + self.check_box_border_thickness,
            area.size().y - self.check_box_padding * 2.0 - self.check_box_border_thickness * 2.0,
            area.size().y - self.check_box_padding * 2.0 - self.check_box_border_thickness * 2.0,
        )
    }
}
