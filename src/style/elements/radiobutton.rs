use serde::Deserialize;
use color::Color;

#[derive(Copy, Clone, Deserialize, Debug, PartialEq)]
#[cfg_attr(test, serde(default), derive(Default))]
pub struct RadioButton {
    pub option_background_color_inactive: Color,
    pub option_text_color_inactive: Color,
    pub option_background_color_active: Color,
    pub option_text_color_active: Color,
    pub option_background_color_clicked: Color,
    pub option_text_color_clicked: Color,
    pub option_padding: f32,
    pub line_thickness: f32,
    pub line_bottom_offset: f32,
}
