use serde::Deserialize;

use color::Color;

#[derive(Copy, Clone, Deserialize, Debug, PartialEq)]
#[cfg_attr(test, serde(default), derive(Default))]
pub struct Slider {
    pub background_color: Color,
    pub step_background_color: Color,
    pub thumb_color: Color,
    pub thumb_color_hovered: Color,
    pub thumb_color_clicked: Color,
    pub min_max_text_color: Color,
    pub value_text_color: Color,
}
