pub mod commitprovider;
pub mod drindexprovider;
pub mod index;
pub mod sizedindex;

use self::{
    index::Index,
    sizedindex::SizedIndex,
};
use element::Element;
use guierror::GuiError;
use std::{
    any::{Any, TypeId},
    collections::BTreeMap,
    mem,
    os::raw::c_void,
    raw::TraitObject,
};
use style::Style;

struct AnonymousListProvider {
    ptr: TraitObject,
    type_id: TypeId,
    index: usize,
}

struct AnonymousIndexProvider {
    ptr: TraitObject,
    k_type_id: TypeId,
    v_type_id: TypeId,
}

pub struct ListProviderIter<'a, T> {
    provider: &'a ListProvider<T>,
    index: usize,
}

impl<'a, T: 'static> Iterator for ListProviderIter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.index += 1;
        self.provider.get(self.index - 1)
    }
}

pub struct ListProvider<T> {
    provider: *mut dyn SizedIndex<T>,
    current_index: *mut usize,
}

#[allow(clippy::len_without_is_empty)]
impl<T: 'static> ListProvider<T> {
    fn new(list_provider: &mut AnonymousListProvider) -> Option<Self> {
        if list_provider.type_id == TypeId::of::<T>() {
            let provider = unsafe { mem::transmute(list_provider.ptr) };
            Some(ListProvider {
                provider,
                current_index: &mut list_provider.index as *mut usize,
            })
        } else {
            None
        }
    }
    pub fn get(&self, index: usize) -> Option<&T> {
        unsafe { (*self.provider).get(index) }
    }
    pub fn get_mut(&mut self, index: usize) -> Option<&mut T> {
        unsafe { (*self.provider).get_mut(index) }
    }
    pub fn get_next(&self) -> Option<&T> {
        unsafe {
            let len = (*self.provider).len();
            if *self.current_index >= len {
                *self.current_index = 0;
            }
            *self.current_index += 1;
            (*self.provider).get(*self.current_index - 1)
        }
    }
    pub fn get_next_mut(&self) -> Option<&mut T> {
        unsafe {
            let len = (*self.provider).len();
            if *self.current_index >= len {
                *self.current_index = 0;
            }
            *self.current_index += 1;
            (*self.provider).get_mut(*self.current_index - 1)
        }
    }
    pub fn peek_next(&self) -> Option<&T> {
        unsafe { (*self.provider).get(*self.current_index) }
    }
    pub fn peek_next_mut(&self) -> Option<&mut T> {
        unsafe { (*self.provider).get_mut(*self.current_index) }
    }
    pub fn iter(&self) -> ListProviderIter<T> {
        ListProviderIter {
            provider: self,
            index: 0,
        }
    }
    pub fn len(&self) -> usize {
        unsafe { (*self.provider).len() }
    }
}

pub struct IndexProvider<V> {
    provider: *mut dyn Index<V>,
}

impl<V: 'static> IndexProvider<V> {
    fn new(index_provider: &mut AnonymousIndexProvider) -> Option<Self> {
        if index_provider.k_type_id == TypeId::of::<String>() && index_provider.v_type_id == TypeId::of::<V>() {
            let provider = unsafe { mem::transmute(index_provider.ptr) };
            Some(IndexProvider { provider })
        } else {
            None
        }
    }
    pub fn get(&self, index: &str) -> Option<&V> {
        unsafe { (*self.provider).get(index) }
    }
    pub fn get_mut(&self, index: &str) -> Option<&mut V> {
        unsafe { (*self.provider).get_mut(index) }
    }
}

pub struct Context {
    list_providers: BTreeMap<String, AnonymousListProvider>,
    index_provider: BTreeMap<String, AnonymousIndexProvider>,
    keyboard_focus: bool,
    // It would be nice to remove this raw pointer and replace it somehow.
    // It needs to be mutable to call event functions on the focused element
    // without traversing the entire GUI.
    focused_element: Option<*mut dyn Element>,
    pub style: Style,
    owned_providers: Vec<Box<dyn Any>>,
}

impl Context {
    pub fn new(style_file_path: &std::path::Path) -> Result<Self, GuiError> {
        let style = Style::new(style_file_path)?;
        Ok(Context::styled_new(style))
    }
    pub fn styled_new(style: Style) -> Self {
        Context {
            list_providers: Default::default(),
            index_provider: Default::default(),
            keyboard_focus: Default::default(),
            focused_element: Default::default(),
            style,
            owned_providers: Default::default(),
        }
    }

    pub fn add_list_provider<T: 'static>(&mut self, name: &str, provider: &mut dyn SizedIndex<T>) {
        let ptr: TraitObject = unsafe { mem::transmute(provider) };
        let anonymous_provider = AnonymousListProvider {
            ptr,
            type_id: TypeId::of::<T>(),
            index: 0,
        };
        self.list_providers.insert(name.to_owned(), anonymous_provider);
    }

    // This transmute should always be safe
    #[allow(clippy::not_unsafe_ptr_arg_deref)]
    pub fn add_index_provider<V: 'static>(&mut self, name: &str, provider: *mut dyn Index<V>) {
        let ptr: TraitObject = unsafe { mem::transmute(provider) };
        let anonymous_provider = AnonymousIndexProvider {
            ptr,
            k_type_id: TypeId::of::<String>(),
            v_type_id: TypeId::of::<V>(),
        };
        self.index_provider.insert(name.to_owned(), anonymous_provider);
    }

    pub fn take_list_provider<T, P>(&mut self, name: &str, provider: P)
    where
        T: 'static,
        P: 'static + SizedIndex<T>,
    {
        let mut boxed_provider = Box::new(provider);
        self.add_list_provider(name, boxed_provider.as_mut());
        self.owned_providers.push(boxed_provider);
    }

    pub fn take_index_provider<V, P>(&mut self, name: &str, provider: P)
    where
        V: 'static,
        P: 'static + Index<V>,
    {
        let mut boxed_provider = Box::new(provider);
        self.add_index_provider(name, boxed_provider.as_mut());
        self.owned_providers.push(boxed_provider);
    }

    pub fn get_list_provider<T: 'static>(&mut self, name: &str) -> Option<ListProvider<T>> {
        match self.list_providers.get_mut(name) {
            Some(anonymous_provider) => ListProvider::new(anonymous_provider),
            None => None,
        }
    }

    pub fn get_index_provider<V: 'static>(&mut self, name: &str) -> Option<IndexProvider<V>> {
        match self.index_provider.get_mut(name) {
            Some(anonymous_provider) => IndexProvider::new(anonymous_provider),
            None => None,
        }
    }

    pub fn reset_providers(&mut self) {
        for anonymous_vec in &mut self.list_providers.values_mut() {
            anonymous_vec.index = 0;
        }
    }

    pub fn reset_provider(&mut self, name: &str) {
        if let Some(anonymous_vec) = self.list_providers.get_mut(name) {
            anonymous_vec.index = 0
        }
    }

    pub fn provider_len(&mut self, name: &str) -> usize {
        match self.list_providers.get(name) {
            Some(anonymous_provider) => unsafe {
                let provider: *mut dyn SizedIndex<c_void> = mem::transmute(anonymous_provider.ptr);
                (*provider).len()
            },
            None => 0,
        }
    }

    pub fn request_keyboard_focus(&mut self) {
        self.keyboard_focus = true;
    }

    pub fn request_element_focus(&mut self, element: *mut dyn Element) -> bool {
        if let Some(current) = self.focused_element {
            current == element
        } else {
            self.focused_element = Some(element);
            true
        }
    }

    pub fn release_element_focus(&mut self, element: *mut dyn Element) {
        if let Some(current_element) = self.focused_element {
            if current_element == element {
                self.focused_element = None;
            }
        }
    }

    pub fn has_element_focus(&mut self, element: *mut dyn Element) -> bool {
        if let Some(current_element) = self.focused_element {
            current_element == element
        } else {
            false
        }
    }

    pub fn get_focused_element(&self) -> Option<*mut dyn Element> {
        self.focused_element
    }

    pub fn new_frame(&mut self) {
        self.keyboard_focus = false;
    }

    pub fn wants_keyboard_focus(&self) -> bool {
        self.keyboard_focus
    }

    pub fn load_style(&mut self, path: &std::path::Path) -> Result<(), ron::de::Error> {
        self.style = Style::new(path)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use testutil::test_util::*;

    mod test_list_provider {
        use super::*;

        #[test]
        fn test_add_take_get_list_provider() {
            let mut context = styled_context();

            let mut provider = vec![1, 2, 3, 4];
            let mut empty_provider = Vec::<i32>::new();
            let owned_provider = vec![4, 3, 2, 1];

            context.take_list_provider("owned_provider", owned_provider);
            // Since transmute and pointers are used, it's best to verify that
            // they are still valid after potential resizing is done of the
            // underlying collection.
            for i in 0..100 {
                context.take_list_provider(&i.to_string(), vec![123_456_789 as i32; i]);
            }

            context.add_list_provider("provider", &mut provider);
            assert_eq!(context.get_list_provider::<i32>("provider").unwrap().len(), 4);

            context.add_list_provider("provider", &mut empty_provider);
            assert_eq!(context.get_list_provider::<i32>("provider").unwrap().len(), 0);

            let returned_owned_provider = context.get_list_provider::<i32>("owned_provider").unwrap();
            assert_eq!(
                returned_owned_provider.iter().copied().collect::<Vec<i32>>(),
                vec![4, 3, 2, 1],
                "owned provider corrupted"
            );
            for i in 0..100 {
                let returned_owned_provider = context.get_list_provider::<i32>(&i.to_string()).unwrap();
                assert_eq!(
                    returned_owned_provider.iter().copied().collect::<Vec<i32>>(),
                    vec![123_456_789 as i32; i],
                    "owned provider corrupted"
                );
            }
        }

        #[test]
        fn test_reset_provider() {
            let mut context = styled_context();

            let mut provider = vec![1, 2, 3, 4];
            context.add_list_provider("provider", &mut provider);
            let other_provider = vec![40, 30, 20, 10];
            context.take_list_provider("other_provider", other_provider);

            // To make sure only one provider is reset
            let list_provider = context.get_list_provider::<i32>("provider").unwrap();
            let other_list_provider = context.get_list_provider::<i32>("other_provider").unwrap();

            assert_eq!(*list_provider.get_next().unwrap(), 1);
            assert_eq!(*other_list_provider.get_next().unwrap(), 40);
            context.reset_provider("provider");
            assert_eq!(*list_provider.get_next().unwrap(), 1);
            assert_eq!(*other_list_provider.get_next().unwrap(), 30);
        }

        #[test]
        fn test_reset_providers() {
            let mut context = styled_context();

            let mut provider = vec![1, 2, 3, 4];
            context.add_list_provider("provider", &mut provider);
            let other_provider = vec![40, 30, 20, 10];
            context.take_list_provider("other_provider", other_provider);

            let list_provider = context.get_list_provider::<i32>("provider").unwrap();
            let other_list_provider = context.get_list_provider::<i32>("other_provider").unwrap();

            assert_eq!(*list_provider.get_next().unwrap(), 1);
            assert_eq!(*other_list_provider.get_next().unwrap(), 40);
            context.reset_providers();
            assert_eq!(*list_provider.get_next().unwrap(), 1);
            assert_eq!(*other_list_provider.get_next().unwrap(), 40);
        }

        #[test]
        fn test_provider_len() {
            let mut context = styled_context();

            let mut provider = vec![1, 2, 3, 4];
            context.add_list_provider("provider", &mut provider);

            assert_eq!(context.provider_len("provider"), 4);
        }

        #[test]
        fn test_get_next() {
            let mut context = styled_context();
            let mut provider = vec![1, 2, 3, 4];
            context.add_list_provider("provider", &mut provider);

            let list_provider = context.get_list_provider::<i32>("provider").unwrap();

            assert_eq!(*list_provider.get_next().unwrap(), 1);
            *list_provider.get_next_mut().unwrap() = 200;
            assert_eq!(*list_provider.get_next().unwrap(), 3);
            *list_provider.get_next_mut().unwrap() = 400;
            assert_eq!(*list_provider.get_next().unwrap(), 1);
            assert_eq!(*list_provider.get_next().unwrap(), 200);
            assert_eq!(*list_provider.get_next().unwrap(), 3);
            assert_eq!(*list_provider.get_next().unwrap(), 400);
        }
    }

    mod test_index_provider {
        use super::*;
        use std::collections::BTreeMap;

        #[test]
        fn test_add_take_get_index_provider() {
            let mut context = styled_context();

            let mut provider: BTreeMap<String, String> = BTreeMap::new();
            provider.insert("First".to_string(), "One".to_string());
            provider.insert("Second".to_string(), "Two".to_string());
            provider.insert("Third".to_string(), "Three".to_string());
            let mut owned_provider: BTreeMap<String, i32> = BTreeMap::new();
            for i in 0..100 {
                owned_provider.insert(i.to_string(), i);
            }
            let mut empty_provider: BTreeMap<String, String> = BTreeMap::new();

            context.add_index_provider("provider", &mut provider);
            context.take_index_provider("owned_provider", owned_provider);

            assert_eq!(
                context
                    .get_index_provider::<String>("provider")
                    .unwrap()
                    .get("First")
                    .unwrap(),
                "One"
            );

            let returned_owned_provider = context.get_index_provider::<i32>("owned_provider").unwrap();
            for i in 0..100 {
                assert_eq!(
                    i,
                    *returned_owned_provider.get(&i.to_string()).unwrap(),
                    "owned provider corrupted"
                );
            }

            *context
                .get_index_provider::<String>("provider")
                .unwrap()
                .get_mut("First")
                .unwrap() = "Not one".to_string();
            assert_eq!(
                context
                    .get_index_provider::<String>("provider")
                    .unwrap()
                    .get("First")
                    .unwrap(),
                "Not one"
            );

            context.add_index_provider("provider", &mut empty_provider);
            assert!(context
                .get_index_provider::<String>("provider")
                .unwrap()
                .get("First")
                .is_none());
        }
    }

    #[test]
    fn test_request_and_wants_keyboard_focus() {
        let mut context = styled_context();
        context.request_keyboard_focus();
        assert!(context.wants_keyboard_focus());
    }

    #[test]
    fn test_request_and_release_and_has_element_focus() {
        let mut context = styled_context();

        let mut element = DummyElement::new();
        let mut other_element = DummyElement::new();

        assert!(context.request_element_focus(&mut element));
        assert!(context.has_element_focus(&mut element));
        assert!(!context.has_element_focus(&mut other_element));
        assert!(context.get_focused_element().is_some());

        assert!(!context.request_element_focus(&mut other_element));
        assert!(context.has_element_focus(&mut element));
        assert!(!context.has_element_focus(&mut other_element));
        assert!(context.get_focused_element().is_some());

        context.release_element_focus(&mut other_element);
        assert!(context.has_element_focus(&mut element));
        assert!(!context.has_element_focus(&mut other_element));
        assert!(context.get_focused_element().is_some());

        context.release_element_focus(&mut element);
        assert!(!context.has_element_focus(&mut element));
        assert!(!context.has_element_focus(&mut other_element));
        assert!(context.get_focused_element().is_none());

        assert!(context.request_element_focus(&mut other_element));
        assert!(!context.has_element_focus(&mut element));
        assert!(context.has_element_focus(&mut other_element));
        assert!(context.get_focused_element().is_some());
    }
}
