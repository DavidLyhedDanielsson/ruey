#[allow(clippy::len_without_is_empty)]
pub trait SizedIndex<T> {
    fn get(&self, index: usize) -> Option<&T>;
    fn get_mut(&mut self, index: usize) -> Option<&mut T>;
    fn len(&self) -> usize;
}

impl<T> SizedIndex<T> for Vec<T> {
    fn get(&self, index: usize) -> Option<&T> {
        self.as_slice().get(index)
    }
    fn get_mut(&mut self, index: usize) -> Option<&mut T> {
        self.as_mut_slice().get_mut(index)
    }
    fn len(&self) -> usize {
        self.len()
    }
}

#[cfg(test)]
mod tests {
    use context::sizedindex::SizedIndex;

    mod test_vec_impl {
        use super::*;
        #[test]
        fn test_get_and_len() {
            let mut vec = vec![1, 2, 3, 4];
            assert_eq!(*SizedIndex::get(&vec, 0).unwrap(), 1);
            *SizedIndex::get_mut(&mut vec, 1).unwrap() = 200;
            assert_eq!(*SizedIndex::get_mut(&mut vec, 1).unwrap(), 200);
            assert_eq!(*SizedIndex::get(&vec, 2).unwrap(), 3);
            *SizedIndex::get_mut(&mut vec, 1).unwrap() = 400;
            assert_eq!(*SizedIndex::get_mut(&mut vec, 1).unwrap(), 400);
            assert_eq!(SizedIndex::len(&vec), 4);

            assert!(SizedIndex::get(&vec, 123).is_none());
            assert!(SizedIndex::get_mut(&mut vec, 123).is_none());
        }
    }
}
