use context::index::Index;
use std::collections::BTreeMap;

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct CommitProvider<V: Copy> {
    temp_map: BTreeMap<String, V>,
    real_map: BTreeMap<String, *mut V>,
}

impl<V: Copy> CommitProvider<V> {
    pub fn new() -> CommitProvider<V> {
        CommitProvider {
            temp_map: BTreeMap::new(),
            real_map: BTreeMap::new(),
        }
    }

    pub fn add(&mut self, key: &str, val: &mut V) {
        self.temp_map.insert(key.to_string(), *val);
        self.real_map.insert(key.to_string(), val as *mut V);
    }

    pub fn commit(&mut self) {
        for (key, value) in self.temp_map.iter() {
            unsafe { **self.real_map.get_mut(key).unwrap() = *value };
        }
    }

    pub fn reset(&mut self) {
        for (key, value) in self.real_map.iter() {
            unsafe { *self.temp_map.get_mut(key).unwrap() = **value };
        }
    }
}

impl<V: Copy> Index<V> for CommitProvider<V> {
    fn get(&self, index: &str) -> Option<&V> {
        self.temp_map.get(index)
    }
    fn get_mut(&mut self, index: &str) -> Option<&mut V> {
        self.temp_map.get_mut(index)
    }
}
