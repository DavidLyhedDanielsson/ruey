use context::index::Index;
use std::collections::BTreeMap;

#[derive(Default)]
pub struct DRIndexProvider<V> {
    map: BTreeMap<String, *mut V>,
}

impl<V> DRIndexProvider<V> {
    pub fn new() -> DRIndexProvider<V> {
        DRIndexProvider { map: BTreeMap::new() }
    }

    pub fn add(&mut self, key: &str, val: &mut V) {
        self.map.insert(key.to_string(), val as *mut V);
    }
}

impl<V> Index<V> for DRIndexProvider<V> {
    fn get(&self, index: &str) -> Option<&V> {
        if let Some(ptr) = self.map.get(index) {
            unsafe { Some(&**ptr) }
        } else {
            None
        }
    }
    fn get_mut(&mut self, index: &str) -> Option<&mut V> {
        if let Some(ptr) = self.map.get_mut(index) {
            unsafe { Some(&mut **ptr) }
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use context::drindexprovider::DRIndexProvider;
    use context::index::Index;

    #[test]
    fn test_dr_index_provider() {
        let mut test_string = "string_test".to_string();

        let mut provider = DRIndexProvider::<String>::new();
        provider.add("test_string", &mut test_string);

        *provider.get_mut("test_string").unwrap() = "test_string".to_string();

        assert_eq!(provider.get("test_string").unwrap(), "test_string");
        assert!(provider.get("nothing").is_none());
    }
}
