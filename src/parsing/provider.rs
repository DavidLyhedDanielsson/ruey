use Context;

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum Provider {
    List(String),
    Index { name: String, index: String },
}

impl Provider {
    pub unsafe fn get_next_ref_mut<T: 'static>(&self, context: &mut Context) -> Option<&mut T> {
        match self {
            Provider::List(name) => {
                if let Some(provider) = context.get_list_provider::<T>(name) {
                    if let Some(val) = provider.get_next_mut() {
                        Some(&mut *(val as *mut T))
                    } else {
                        None
                    }
                } else {
                    None
                }
            }
            Provider::Index{ name, index } => {
                if let Some(provider) = context.get_index_provider::<T>(name) {
                    if let Some(val) = provider.get_mut(index) {
                        Some(&mut *(val as *mut T))
                    } else {
                        None
                    }
                } else {
                    None
                }
            }
        }
    }
    pub unsafe fn get_next_ref<T: 'static>(&self, context: &mut Context) -> Option<&T> {
        if let Some(val) = self.get_next_ref_mut::<T>(context) {
            Some(&*val)
        } else {
            None
        }
    }
    pub fn get_next<T: 'static + Copy>(&self, context: &mut Context) -> Option<T> {
        unsafe {
            if let Some(val) = self.get_next_ref::<T>(context) {
                Some(*val)
            } else {
                None
            }
        }
    }
    pub unsafe fn peek_next_ref_mut<T: 'static>(&self, context: &mut Context) -> Option<&mut T> {
        match self {
            Provider::List(name) => {
                if let Some(provider) = context.get_list_provider::<T>(name) {
                    if let Some(val) = provider.peek_next_mut() {
                        Some(&mut *(val as *mut T))
                    } else {
                        None
                    }
                } else {
                    None
                }
            }
            Provider::Index{ name, index } => {
                if let Some(provider) = context.get_index_provider::<T>(name) {
                    if let Some(val) = provider.get_mut(index) {
                        Some(&mut *(val as *mut T))
                    } else {
                        None
                    }
                } else {
                    None
                }
            }
        }
    }
    pub unsafe fn peek_next_ref<T: 'static>(&self, context: &mut Context) -> Option<&T> {
        if let Some(val) = self.peek_next_ref_mut(context) {
            Some(&*val)
        } else {
            None
        }
    }
    pub unsafe fn peek_next<T: 'static + Copy>(&self, context: &mut Context) -> Option<T> {
        if let Some(val) = self.peek_next_ref::<T>(context) {
            Some(*val)
        } else {
            None
        }
    }
    pub fn get_next_optional<T: 'static + Copy>(&self, context: &mut Context, default: T) -> T {
        unsafe {
            if let Some(val) = self.get_next_ref::<T>(context) {
                *val
            } else {
                default
            }
        }
    }
}