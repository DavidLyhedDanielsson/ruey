#[derive(Debug, Copy, Clone)]
pub enum VerticalAlignment {
    Top, Center, Bottom
}

#[derive(Debug, Copy, Clone)]
pub enum HorizontalAlignment {
    Left, Center, Right
}