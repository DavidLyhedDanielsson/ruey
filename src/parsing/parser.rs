use context::Context;
use element::{
    docking::{
        split::{Direction, Split},
        Zone,
    },
    layouts::{dockable::Dockable, linear::Linear, list::List, tabbed::Tabbed, switcher::Switcher, window::{self, Window}},
    widgets::{
        button::Button, checkbox::Checkbox, color::Color, colorpicker::ColorPicker, dropdown::Dropdown,
        inputtext::InputText, slider::Slider, text::Text, radiobutton::RadioButton,
    },
    Element, ElementBase, Size, Unit,
};
use guierror::{ParseResult, ParseResultBuilder};
use parsing::ronparser::GetValue;
use parsing::validationerror::ValidationError;
use parsing::ronparser::*;
use ron::Value;
use vector2::Vector2;
use windowmanager::WindowManager;
use std::str::FromStr;

pub struct Parser<'a> {
    parsing_window: bool,
    window_manager: &'a mut WindowManager,
}

fn parse_window_sizing(value: &Value) -> (i32, Option<String>, i32, Option<String>) {
    let (width, width_provider) =  if let Ok(provider) = value.get_str("width") {
        (0, Some(provider.to_string()))
    } else if let Ok(width_value) = value.get_i32("width") {
        (width_value, None)
    } else {
        (0, None)
    };

    let (height, height_provider) =  if let Ok(provider) = value.get_str("height") {
        (0, Some(provider.to_string()))
    } else if let Ok(height_value) = value.get_i32("height") {
        (height_value, None)
    } else {
        (0, None)
    };

    (width, width_provider, height, height_provider)
}

impl<'a> Parser<'a> {
    pub fn new(window_manager: &'a mut WindowManager) -> Self {
        Parser {
            parsing_window: false,
            window_manager,
        }
    }
    fn parse_window(&mut self, context: &mut Context, value: &Value) -> (Option<Window>, ParseResult) {
        let mut parse_response = ParseResultBuilder::new(value);
        let element_type = match value.get_str("type") {
            Ok(element_type) => element_type,
            Err(_) => return (None, parse_response.promote_and_build()),
        };
        match element_type {
            "window" => {
                if self.parsing_window {
                    parse_response.with_error(ValidationError::Message("Nested window layouts are not allowed"));
                    return (None, parse_response.build());
                }

                let (width, width_provider, height, height_provider) = parse_window_sizing(value);

                let validation_errors = Window::validate(value).validate();
                if !validation_errors.is_empty() {
                    return (None, ParseResultBuilder::new(value).with_errors(validation_errors).consume_build())
                }

                self.parsing_window = true;
                let (window, window_response) = Window::new_window(self, context, value, Vector2::new_zero(), Vector2::new(width as f32, height as f32), width_provider, height_provider, parse_response);
                self.parsing_window = false;

                (window, window_response)
            }
            "docked" => {
                self.parse_docked(context, value, parse_response)
            }
            _ => {
                parse_response.with_error(ValidationError::InvalidValue("type", "Either \"window\" or \"docked\""));
                (None, parse_response.build())
            }
        }
    }
    fn parse_docked(&mut self, context: &mut Context, value: &Value, mut parse_response: ParseResultBuilder) -> (Option<Window>, ParseResult) {
        let zone_value = match value.get("zone") {
            Ok(zone) => zone,
            Err(_) => return (None, parse_response.promote_and_build()),
        };
        let mut min_size = Vector2::new_zero();
        let mut max_size = Vector2::new_max();
        let mut allow_resize = true;
        let mut allow_move = true;
        let zone_type = match zone_value.get_str("type") {
            Ok(zone_type) => zone_type,
            Err(_) => return (None, parse_response.promote_and_build()),
        };
        match zone_type {
            "tab" => {
                //let (width, width_provider, height, height_provider) = parse_window_sizing(value);
                let children = match zone_value.get_array("children") {
                    Ok(children) => children,
                    Err(_) => return (None, parse_response.promote_and_build()),
                };
                let mut zones: Vec<Zone> = Vec::new();
                for child in children {
                    match self.parse_window(context, child) {
                        (Some(mut window), response) => {
                            min_size = min_size.max(window.min_size);
                            max_size = max_size.min(window.max_size);
                            allow_resize = allow_resize && window.allow_resize;
                            allow_move = allow_move && window.allow_move;
                            zones.push(window.get_dockable().consume());
                            parse_response.with_messages_from(response);
                        }
                        (None, response) => {
                            parse_response.with_messages_from(response);
                        }
                    }
                }
                let base = ElementBase {
                    position: Vector2::new_zero(),
                    size: Vector2::new_replicate(128.0),
                    parsed_size: Size {
                        width: Unit::Undecided,
                        height: Unit::Undecided,
                    }
                };
                (Some(Window {
                    position: Vector2::new_zero(),
                    size: Vector2::new_replicate(128.0),
                    min_size,
                    max_size,
                    layout: Dockable::new_dockable(base, Zone::Tabbed(0, zones)),
                    allow_resize,
                    allow_move,
                    parsed_width: window::ParsedSize::None,
                    parsed_height: window::ParsedSize::None,
                }), parse_response.build())
            }
            "split" => {
                /*let base = match Parser::parse_element_base::<Window>(value, &mut parse_response) {
                    Ok(base) => base,
                    Err(_) => return (None, parse_response.promote_and_build()),
                };*/
                let direction = match zone_value.get_str("direction") {
                    Ok("vertical") => Direction::Vertical,
                    Ok("horizontal") => Direction::Horizontal,
                    Ok(_) | Err(_) => return (None, parse_response.promote_and_build()),
                };
                let children = match zone_value.get_array("children") {
                    Ok(children) => children,
                    Err(_) => return (None, parse_response.promote_and_build()),
                };
                let mut zones: Vec<Split> = Vec::new();
                let size = 1.0 / children.len() as f32;
                for child in children {
                    match self.parse_window(context, child) {
                        (Some(mut window), response) => {
                            min_size = min_size.max(window.min_size);
                            max_size = max_size.min(window.max_size);
                            allow_resize = allow_resize && window.allow_resize;
                            allow_move = allow_move && window.allow_move;
                            zones.push(Split::new(size, window.get_dockable().consume()));
                            parse_response.with_messages_from(response);
                        }
                        (None, response) => {
                            parse_response.with_messages_from(response);
                        }
                    }
                }
                let base = ElementBase {
                    position: Vector2::new_zero(),
                    size: Vector2::new_replicate(128.0),
                    parsed_size: Size {
                        width: Unit::Undecided,
                        height: Unit::Undecided,
                    }
                };
                (Some(Window {
                    position: Vector2::new_zero(),
                    size: Vector2::new_zero(),
                    min_size,
                    max_size,
                    layout: Dockable::new_dockable(base, Zone::Split(direction, zones)),
                    allow_resize,
                    allow_move,
                    parsed_width: window::ParsedSize::None,
                    parsed_height: window::ParsedSize::None,
                }), parse_response.build())
            }
            _ => {
                parse_response.with_error(ValidationError::InvalidValue("type", r#""tab" or "split""#));
                (None, parse_response.build())
            }
        }
    }
    pub fn parse_element(&mut self, context: &mut Context, value: &Value) -> ParseResult {
        let mut parse_response = ParseResultBuilder::new(value);
        let element_type = match value.get_str("type") {
            Ok(element_type) => element_type,
            Err(_) => return parse_response.promote_and_build()
        };
        match element_type {
            "linear" => Parser::create_boxed_element::<Linear>(self, context, value),
            "color" => Parser::create_boxed_element::<Color>(self, context, value),
            "text" => Parser::create_boxed_element::<Text>(self, context, value),
            "inputtext" => Parser::create_boxed_element::<InputText>(self, context, value),
            "list" => Parser::create_boxed_element::<List>(self, context, value),
            "button" => Parser::create_boxed_element::<Button>(self, context, value),
            "dropdown" => Parser::create_boxed_element::<Dropdown>(self, context, value),
            "checkbox" => Parser::create_boxed_element::<Checkbox>(self, context, value),
            "slider" => Parser::create_boxed_element::<Slider>(self, context, value),
            "tabbed" => Parser::create_boxed_element::<Tabbed>(self, context, value),
            "switcher" => Parser::create_boxed_element::<Switcher>(self, context, value),
            "colorpicker" => Parser::create_boxed_element::<ColorPicker>(self, context, value),
            "radiobutton" => Parser::create_boxed_element::<RadioButton>(self, context, value),
            "window" => {
                parse_response.with_error(ValidationError::Message("Nested window layouts are not allowed"));
                parse_response.build()
            }
            "docked" => {
                parse_response.with_error(ValidationError::Message("Docked layouts must either be the root element, or a direct descendant of one"));
                parse_response.build()
            }
            _ => {
                parse_response.with_error(ValidationError::DynamicMessage(format!("No element \"{}\" exists", element_type)));
                parse_response.build()
            }
        }
    }
    pub fn parse_root(&mut self, context: &mut Context, value: &Value) -> ParseResult {
        let mut response_builder = ParseResultBuilder::new(value);
        match value {
            Value::Seq(vec) => {
                for element in vec {
                    match self.parse_window(context, element) {
                        (Some(window), response) => {
                            self.window_manager.add_window(window);
                            response_builder.with_messages_from(response);
                        }
                        (None, response) => {
                            response_builder.with_messages_from(response);
                        }
                    }
                }
            }
            _ => panic!("No"),
        }
        response_builder.build()
    }
    pub fn create_boxed_element<T: Element + 'static> (
        parser: &mut Parser,
        context: &mut Context,
        value: &Value,
    ) -> ParseResult {
        let mut parse_response = ParseResultBuilder::new(value);
        let parsed_base = Parser::parse_element_base::<T>(value, &mut parse_response);

        let validation_errors = T::validate(value).validate();
        if !validation_errors.is_empty() || parsed_base.is_err() {
            parse_response.with_errors(validation_errors);
            parse_response.build()
        } else {
            T::parse(parser, context, value, parsed_base.unwrap())
        }
    }

    pub fn parse_element_base<T: Element + 'static>(value: &Value, parse_response: &mut ParseResultBuilder) -> Result<ElementBase, ()> {
        let (width, height) = if let Ok(size) = value.get_vector2("size") {
            (Ok(Unit::Units(size.x)), Ok(Unit::Units(size.y)))
        } else {
            (Parser::try_parse_width_height::<T>(value, "width", parse_response),
            Parser::try_parse_width_height::<T>(value, "height", parse_response))
        };
        if width.is_err() || height.is_err() {
            return Err(());
        }
        let parsed_size = Size { width: width.unwrap(), height: height.unwrap() };
        Ok(ElementBase {
            position: Vector2::new_zero(),
            size: Vector2::new_zero(),
            parsed_size,
        })
    }
    fn try_parse_width_height<T: Element>(value: &Value, field: &'static str, parse_response: &mut ParseResultBuilder) -> Result<Unit, ()> {
        match value.get_unit(field) {
            Ok(unit) => Ok(unit),
            _ => match value.get_str(field) {
                Ok(width_str) => {
                    Parser::try_parse_unit(width_str, parse_response)
                }
                _ => {
                    if T::allow_unsized() {
                        Ok(Unit::Undecided)
                    } else {
                        parse_response.with_error(ValidationError::NotFound(field));
                        Err(())
                    }
                }
            }
        }
    }
    fn try_parse_unit(value: &str, parse_response: &mut ParseResultBuilder) -> Result<Unit, ()> {
        match value.chars().last() {
            Some('%') => {
                let val = f32::from_str(&value[..value.len() - 1]);
                if let Ok(val) = val {
                    Ok(Unit::Percent(val))
                } else {
                    parse_response.with_error(ValidationError::DynamicMessage(format!(
                        "Couldn't convert {} to a percentage unit",
                        value
                    )));
                    Err(())
                }
            }
            Some(_) => {
                let val = f32::from_str(value);
                if let Ok(val) = val {
                    Ok(Unit::Units(val))
                } else {
                    parse_response.with_error(ValidationError::DynamicMessage(format!(
                        "Couldn't convert {} to a unit",
                        value
                    )));
                    Err(())
                }
            }
            None => {
                    parse_response.with_error(ValidationError::DynamicMessage(format!(
                        "Couldn't convert {} to a unit",
                        value
                    )));
                Err(())
            }
        }
    }
}
