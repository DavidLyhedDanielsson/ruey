#[derive(Debug, PartialEq, Eq)]
pub enum ValidationError {
    NotFound(&'static str),
    NotFoundMultiple(&'static str),
    NotFoundEither(&'static str),
    WrongType(&'static str, &'static str),
    InvalidValue(&'static str, &'static str),
    MutuallyExclusive(&'static str),
    Message(&'static str),
    DynamicMessage(String),
}

impl std::fmt::Display for ValidationError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ValidationError::NotFound(field) => write!(f, "Could not find mandatory field \"{}\"", field),
            ValidationError::NotFoundMultiple(fields) => write!(f, "Could not find mandatory fields \"{}\"", fields),
            ValidationError::NotFoundEither(fields) => write!(f, "Could not find one of either mandatory fields \"{}\"", fields),
            ValidationError::WrongType(field, expected) => write!(f, "Field \"{}\" should be {}", field, expected),
            ValidationError::InvalidValue(field, expected) => write!(f, "Invalid value in field \"{}\", expected {}", field, expected),
            ValidationError::MutuallyExclusive(fields) => write!(f, "Fields {:?} are mutually exclusive", fields),
            ValidationError::Message(message) => write!(f, "{}", message),
            ValidationError::DynamicMessage(message) => write!(f, "{}", message),
        }
    }
}