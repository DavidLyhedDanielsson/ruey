use parsing::validationerror::ValidationError;
use ron::Value;
use parsing::provider::Provider;
use parsing::ronparser::*;

pub fn parse_provider(text: &str) -> Option<Provider> {
    if text.matches(':').count() > 1 {
        return None;
    }
    let mut iter = text.split(':');
    let name = iter.next();
    let index = iter.next();
    if let Some(index) = index {
        Some(Provider::Index {
            name: name.unwrap().to_string(),
            index: index.to_string(),
        })
    } else if let Some(name) = name {
        Some(Provider::List(name.to_string()))
    } else {
        None
    }
}

pub type ValidatorFunction = fn(&Value, &'static str) -> Result<(), ValidationError>;
pub type CustomValidatorFunction = fn(&Value) -> Result<(), ValidationError>;

pub struct FieldValidator<'a> {
    value: &'a Value,
    validators: Vec<(&'static str, ValidatorFunction)>,
    custom_validators: Vec<CustomValidatorFunction>,
}

#[allow(clippy::wrong_self_convention)]
impl<'a> FieldValidator<'a> {
    pub fn new(value: &'a Value) -> Self {
        FieldValidator {
            value,
            validators: Vec::new(),
            custom_validators: Vec::new(),
        }
    }

    pub fn validate(self) -> Vec<ValidationError> {
        let mut validation_errors = Vec::<ValidationError>::new();
        for validator in self.validators {
            match validator.1(self.value, validator.0) {
                Ok(_) => {/*Horray*/},
                Err(err) => validation_errors.push(err),
            }
        }
        for validator in self.custom_validators {
            match validator(self.value) {
                Ok(_) => {/*Yay*/},
                Err(err) => validation_errors.push(err),
            }
        }
        validation_errors
    }

    pub fn is_class_fun() -> ValidatorFunction {
        |value: &Value, field: &'static str| {
            match value.get(field) {
                Ok(value) => {
                    if let Value::Map(_) = value {
                        Ok(())
                    } else {
                        Err(ValidationError::WrongType(field, "class"))
                    }
                }
                Err(_) => Err(ValidationError::NotFound(field))
            }
        }
    }
    pub fn is_i32_fun() -> ValidatorFunction {
        |value: &Value, field: &'static str| {
             match value.get_i32(field) {
                Ok(_) => Ok(()),
                Err(error) => Err(error),
            }
        }
    }
    pub fn is_u32_fun() -> ValidatorFunction {
        |value: &Value, field: &'static str| {
             match value.get_u32(field) {
                Ok(_) => Ok(()),
                Err(error) => Err(error),
            }
        }
    }
    pub fn is_f32_fun() -> ValidatorFunction {
        |value: &Value, field: &'static str| {
             match value.get_f32(field) {
                Ok(_) => Ok(()),
                Err(error) => Err(error),
            }
        }
    }
    pub fn is_bool_fun() -> ValidatorFunction {
        |value: &Value, field: &'static str| {
             match value.get_bool(field) {
                Ok(_) => Ok(()),
                Err(error) => Err(error),
            }
        }
    }
    pub fn is_str_fun() -> ValidatorFunction {
        |value: &Value, field: &'static str| {
             match value.get_str(field) {
                Ok(_) => Ok(()),
                Err(error) => Err(error),
            }
        }
    }
    pub fn is_array_fun() -> ValidatorFunction {
        |value: &Value, field: &'static str| {
             match value.get_array(field) {
                Ok(_) => Ok(()),
                Err(error) => Err(error),
            }
        }
    }
    pub fn is_vector2_fun() -> ValidatorFunction {
        |value: &Value, field: &'static str| {
             match value.get_vector2(field) {
                Ok(_) => Ok(()),
                Err(error) => Err(error),
            }
        }
    }
    pub fn is_unit_fun() -> ValidatorFunction {
        |value: &Value, field: &'static str| {
             match value.get_unit(field) {
                Ok(_) => Ok(()),
                Err(error) => Err(error),
            }
        }
    }
    pub fn is_provider_fun() -> ValidatorFunction {
        |value: &Value, field: &'static str| {
            match value.get_str(field) {
                Ok(value) => {
                    match parse_provider(value) {
                        Some(_) => Ok(()),
                        None => Err(ValidationError::WrongType(field, "a provider")),
                    }
                },
                Err(err) => Err(err)
            }
        }
    }
    pub fn is_list_provider_fun() -> ValidatorFunction {
        |value: &Value, field: &'static str| {
            match value.get_str(field) {
                Ok(value) => {
                    match parse_provider(value) {
                        Some(Provider::List(_)) => Ok(()),
                        _ => Err(ValidationError::WrongType(field, "a list provider")),
                    }
                },
                Err(err) => Err(err)
            }
        }
    }
    pub fn is_index_provider_fun() -> ValidatorFunction {
        |value: &Value, field: &'static str| {
            match value.get_str(field) {
                Ok(value) => {
                    match parse_provider(value) {
                        Some(Provider::Index{..}) => Ok(()),
                        _ => Err(ValidationError::WrongType(field, "a index provider")),
                    }
                },
                Err(err) => Err(err)
            }
        }
    }
    pub fn is_alignment_fun() -> ValidatorFunction {
        |value: &Value, field: &'static str| {
             match value.get_alignment(field) {
                Ok(_) => Ok(()),
                Err(error) => Err(error),
            }
        }
    }

    pub fn is_valid(mut self, validator: CustomValidatorFunction) -> FieldValidator<'a> {
        self.custom_validators.push(validator);
        self
    }
    pub fn is_class(mut self, field: &'static str) -> FieldValidator<'a> {
        self.validators.push((field, FieldValidator::is_class_fun()));
        self
    }
    pub fn is_i32(mut self, field: &'static str) -> FieldValidator<'a> {
        self.validators.push((field, FieldValidator::is_i32_fun()));
        self
    }
    pub fn is_u32(mut self, field: &'static str) -> FieldValidator<'a> {
        self.validators.push((field, FieldValidator::is_u32_fun()));
        self
    }
    pub fn is_f32(mut self, field: &'static str) -> FieldValidator<'a> {
        self.validators.push((field, FieldValidator::is_f32_fun()));
        self
    }
    pub fn is_bool(mut self, field: &'static str) -> FieldValidator<'a> {
        self.validators.push((field, FieldValidator::is_bool_fun()));
        self
    }
    pub fn is_str(mut self, field: &'static str) -> FieldValidator<'a> {
        self.validators.push((field, FieldValidator::is_str_fun()));
        self
    }
    pub fn is_array(mut self, field: &'static str) -> FieldValidator<'a> {
        self.validators.push((field, FieldValidator::is_array_fun()));
        self
    }
    pub fn is_vector2(mut self, field: &'static str) -> FieldValidator<'a> {
        self.validators.push((field, FieldValidator::is_vector2_fun()));
        self
    }
    pub fn is_unit(mut self, field: &'static str) -> FieldValidator<'a> {
        self.validators.push((field, FieldValidator::is_unit_fun()));
        self
    }
    pub fn is_provider(mut self, field: &'static str) -> FieldValidator<'a> {
        self.validators.push((field, FieldValidator::is_provider_fun()));
        self
    }
    pub fn is_list_provider(mut self, field: &'static str) -> FieldValidator<'a> {
        self.validators.push((field, FieldValidator::is_list_provider_fun()));
        self
    }
    pub fn is_index_provider(mut self, field: &'static str) -> FieldValidator<'a> {
        self.validators.push((field, FieldValidator::is_index_provider_fun()));
        self
    }
    pub fn is_alignment(mut self, field: &'static str) -> FieldValidator<'a> {
        self.validators.push((field, FieldValidator::is_alignment_fun()));
        self
    }
}

#[cfg(test)]
mod test {
    use parsing::parsehelper::*;
    use lazy_static::*;

    lazy_static! {
        static ref TEST_RON_VALUE: Value = Value::from_str(r#"
        (
            some_class: (
                some_member: "member",
            ),
            some_i32: -123,
            some_u32: 123,
            some_f32: 654.321,
            some_bool: true,
            some_str: "Some string",
            some_array: [1, 2, 3],
            some_vector2: (1.0, 2.0),
            percent_unit: "12.3%",
            some_unit: 12,
            some_list_provider: "some_provider",
            some_index_provider: "some_provider:some_index",
        )
        "#).unwrap();
    }

    mod test_parse_provider {
        use super::*;

        #[test]
        fn test_parse_list_provider() {
            let provider = parse_provider("provider_name").unwrap();
            match provider {
                Provider::List(name) => assert_eq!("provider_name", &name),
                _ => panic!("\"provider_name\" was not a list provider"),
            }
        }
        #[test]
        fn test_parse_index_provider() {
            let provider = parse_provider("provider_name:provider_index").unwrap();
            match provider {
                Provider::Index { name, index } => {
                    assert_eq!("provider_name", &name);
                    assert_eq!("provider_index", &index);
                }
                _ => panic!("\"provider_name\" was not a list provider"),
            }
        }
        #[test]
        fn test_parse_multiple_colons() {
            assert!(parse_provider("a:b:c").is_none());
        }
    }

    mod test_field_validator {
        use super::*;
        
        #[test]
        fn test_is_class() {
            assert!(
                FieldValidator::new(&TEST_RON_VALUE)
                .is_class("some_class")
                .validate().is_empty(), "is_class does not work");
        }
        #[test]
        fn test_is_not_class() {
            let errors = FieldValidator::new(&TEST_RON_VALUE)
                .is_class("some_i32")
                .validate();
            assert!(!errors.is_empty(), "No errors after validation");
            assert_eq!(errors.len(), 1, "Multiple errors after validation");
            match errors.get(0).unwrap() {
                ValidationError::WrongType(name, expected) => {
                    assert_eq!(name, &"some_i32", "invalid name");
                    assert_eq!(expected, &"class", "invalid expected type");
                }
                _ => panic!("Error is not \"WrongType\"")
            }
        }

        #[test]
        fn test_is_i32() {
            assert!(
                FieldValidator::new(&TEST_RON_VALUE)
                .is_i32("some_i32")
                .validate().is_empty(), "is_i32 does not work");
        }
        #[test]
        fn test_is_not_i32() {
            let errors = FieldValidator::new(&TEST_RON_VALUE)
                .is_i32("some_class")
                .validate();
            assert!(!errors.is_empty(), "No errors after validation");
            assert_eq!(errors.len(), 1, "Multiple errors after validation");
            match errors.get(0).unwrap() {
                ValidationError::WrongType(name, expected) => {
                    assert_eq!(name, &"some_class", "invalid name");
                    assert_eq!(expected, &"an i32", "invalid expected type");
                }
                _ => panic!("Error is not \"WrongType\"")
            }
        }

        #[test]
        fn test_is_u32() {
            assert!(
                FieldValidator::new(&TEST_RON_VALUE)
                .is_u32("some_u32")
                .validate().is_empty(), "is_u32 does not work");
        }
        #[test]
        fn test_is_not_u32() {
            let errors = FieldValidator::new(&TEST_RON_VALUE)
                .is_u32("some_class")
                .validate();
            assert!(!errors.is_empty(), "No errors after validation");
            assert_eq!(errors.len(), 1, "Multiple errors after validation");
            match errors.get(0).unwrap() {
                ValidationError::WrongType(name, expected) => {
                    assert_eq!(name, &"some_class", "invalid name");
                    assert_eq!(expected, &"a u32", "invalid expected type");
                }
                _ => panic!("Error is not \"WrongType\"")
            }
        }

        #[test]
        fn test_is_f32() {
            assert!(
                FieldValidator::new(&TEST_RON_VALUE)
                .is_f32("some_f32")
                .validate().is_empty(), "is_f32 does not work");
        }
        #[test]
        fn test_is_not_f32() {
            let errors = FieldValidator::new(&TEST_RON_VALUE)
                .is_f32("some_class")
                .validate();
            assert!(!errors.is_empty(), "No errors after validation");
            assert_eq!(errors.len(), 1, "Multiple errors after validation");
            match errors.get(0).unwrap() {
                ValidationError::WrongType(name, expected) => {
                    assert_eq!(name, &"some_class", "invalid name");
                    assert_eq!(expected, &"an f32", "invalid expected type");
                }
                _ => panic!("Error is not \"WrongType\"")
            }
        }

        #[test]
        fn test_is_bool() {
            assert!(
                FieldValidator::new(&TEST_RON_VALUE)
                .is_bool("some_bool")
                .validate().is_empty(), "is_bool does not work");
        }
        #[test]
        fn test_is_not_bool() {
            let errors = FieldValidator::new(&TEST_RON_VALUE)
                .is_bool("some_class")
                .validate();
            assert!(!errors.is_empty(), "No errors after validation");
            assert_eq!(errors.len(), 1, "Multiple errors after validation");
            match errors.get(0).unwrap() {
                ValidationError::WrongType(name, expected) => {
                    assert_eq!(name, &"some_class", "invalid name");
                    assert_eq!(expected, &"a bool", "invalid expected type");
                }
                _ => panic!("Error is not \"WrongType\"")
            }
        }

        #[test]
        fn test_is_str() {
            assert!(
                FieldValidator::new(&TEST_RON_VALUE)
                .is_str("some_str")
                .validate().is_empty(), "is_str does not work");
        }
        #[test]
        fn test_is_not_str() {
            let errors = FieldValidator::new(&TEST_RON_VALUE)
                .is_str("some_class")
                .validate();
            assert!(!errors.is_empty(), "No errors after validation");
            assert_eq!(errors.len(), 1, "Multiple errors after validation");
            match errors.get(0).unwrap() {
                ValidationError::WrongType(name, expected) => {
                    assert_eq!(name, &"some_class", "invalid name");
                    assert_eq!(expected, &"a string", "invalid expected type");
                }
                _ => panic!("Error is not \"WrongType\"")
            }
        }

        #[test]
        fn test_is_array() {
            assert!(
                FieldValidator::new(&TEST_RON_VALUE)
                .is_array("some_array")
                .validate().is_empty(), "is_array does not work");
        }
        #[test]
        fn test_is_not_array() {
            let errors = FieldValidator::new(&TEST_RON_VALUE)
                .is_array("some_class")
                .validate();
            assert!(!errors.is_empty(), "No errors after validation");
            assert_eq!(errors.len(), 1, "Multiple errors after validation");
            match errors.get(0).unwrap() {
                ValidationError::WrongType(name, expected) => {
                    assert_eq!(name, &"some_class", "invalid name");
                    assert_eq!(expected, &"an array", "invalid expected type");
                }
                _ => panic!("Error is not \"WrongType\"")
            }
        }

        #[test]
        fn test_is_vector2() {
            assert!(
                FieldValidator::new(&TEST_RON_VALUE)
                .is_vector2("some_vector2")
                .validate().is_empty(), "is_vector2 does not work");
        }
        #[test]
        fn test_is_not_vector2() {
            let errors = FieldValidator::new(&TEST_RON_VALUE)
                .is_vector2("some_class")
                .validate();
            assert!(!errors.is_empty(), "No errors after validation");
            assert_eq!(errors.len(), 1, "Multiple errors after validation");
            match errors.get(0).unwrap() {
                ValidationError::WrongType(name, expected) => {
                    assert_eq!(name, &"some_class", "invalid name");
                    assert_eq!(expected, &"an array with two f32s", "invalid expected type");
                }
                _ => panic!("Error is not \"WrongType\"")
            }
        }

        #[test]
        fn test_is_unit() {
            assert!(
                FieldValidator::new(&TEST_RON_VALUE)
                .is_unit("some_unit")
                .validate().is_empty(), "is_unit does not work");
        }
        #[test]
        fn test_is_not_unit() {
            let errors = FieldValidator::new(&TEST_RON_VALUE)
                .is_unit("some_class")
                .validate();
            assert!(!errors.is_empty(), "No errors after validation");
            assert_eq!(errors.len(), 1, "Multiple errors after validation");
            match errors.get(0).unwrap() {
                ValidationError::WrongType(name, expected) => {
                    assert_eq!(name, &"some_class", "invalid name");
                    assert_eq!(expected, &"either a unit (number) or a percentage (string)", "invalid expected type");
                }
                _ => panic!("Error is not \"WrongType\"")
            }
        }

        #[test]
        fn test_is_provider() {
            assert!(
                FieldValidator::new(&TEST_RON_VALUE)
                .is_provider("some_index_provider")
                .is_provider("some_list_provider")
                .validate().is_empty(), "is_provider does not work");
        }
        #[test]
        fn test_is_not_provider() {
            let errors = FieldValidator::new(&TEST_RON_VALUE)
                .is_provider("some_class")
                .validate();
            assert!(!errors.is_empty(), "No errors after validation");
            assert_eq!(errors.len(), 1, "Multiple errors after validation");
            match errors.get(0).unwrap() {
                ValidationError::WrongType(name, expected) => {
                    assert_eq!(name, &"some_class", "invalid name");
                    assert_eq!(expected, &"a string", "invalid expected type");
                }
                _ => panic!("Error is not \"WrongType\"")
            }
        }

        #[test]
        fn test_is_list_provider() {
            assert!(
                FieldValidator::new(&TEST_RON_VALUE)
                .is_list_provider("some_list_provider")
                .validate().is_empty(), "is_list_provider does not work");
        }
        #[test]
        fn test_is_not_list_provider() {
            let errors = FieldValidator::new(&TEST_RON_VALUE)
                .is_list_provider("some_class")
                .validate();
            assert!(!errors.is_empty(), "No errors after validation");
            assert_eq!(errors.len(), 1, "Multiple errors after validation");
            match errors.get(0).unwrap() {
                ValidationError::WrongType(name, expected) => {
                    assert_eq!(name, &"some_class", "invalid name");
                    assert_eq!(expected, &"a string", "invalid expected type");
                }
                _ => panic!("Error is not \"WrongType\"")
            }
        }

        #[test]
        fn test_is_index_provider() {
            assert!(
                FieldValidator::new(&TEST_RON_VALUE)
                .is_index_provider("some_index_provider")
                .validate().is_empty(), "is_index_provider does not work");
        }
        #[test]
        fn test_is_not_index_provider() {
            let errors = FieldValidator::new(&TEST_RON_VALUE)
                .is_index_provider("some_class")
                .validate();
            assert!(!errors.is_empty(), "No errors after validation");
            assert_eq!(errors.len(), 1, "Multiple errors after validation");
            match errors.get(0).unwrap() {
                ValidationError::WrongType(name, expected) => {
                    assert_eq!(name, &"some_class", "invalid name");
                    assert_eq!(expected, &"a string", "invalid expected type");
                }
                _ => panic!("Error is not \"WrongType\"")
            }
        }
    }
}