use parsing::validationerror::ValidationError;
use ron::Value;
use vector2::Vector2;
use element::Unit;
use parsing::parsehelper::parse_provider;
use parsing::provider::Provider;
use parsing::alignment::*;

pub trait GetValue {
    fn get(&self, field: &'static str) -> Result<&Value, ValidationError>;
    fn get_i32(&self, field: &'static str) -> Result<i32, ValidationError>;
    fn get_u32(&self, field: &'static str) -> Result<u32, ValidationError>;
    fn get_f32(&self, field: &'static str) -> Result<f32, ValidationError>;
    fn get_bool(&self, field: &'static str) -> Result<bool, ValidationError>;
    fn get_str(&self, field: &'static str) -> Result<&str, ValidationError>;
    fn get_array(&self, field: &'static str) -> Result<&Vec<Value>, ValidationError>;
    fn get_vector2(&self, field: &'static str) -> Result<Vector2, ValidationError>;
    fn get_unit(&self, field: &'static str) -> Result<Unit, ValidationError>;
    fn get_provider(&self, field: &'static str) -> Result<Provider, ValidationError>;
    fn get_list_provider(&self, field: &'static str) -> Result<String, ValidationError>;
    fn get_index_provider(&self, field: &'static str) -> Result<(String, String), ValidationError>;
    fn get_alignment(&self, field: &'static str) -> Result<(VerticalAlignment, HorizontalAlignment), ValidationError>;
}

pub trait ExpectValue {
    fn expect(&self, field: &'static str) -> &Value;
    fn expect_i32(&self, field: &'static str) -> i32;
    fn expect_u32(&self, field: &'static str) -> u32;
    fn expect_f32(&self, field: &'static str) -> f32;
    fn expect_bool(&self, field: &'static str) -> bool;
    fn expect_str(&self, field: &'static str) -> &str;
    fn expect_array(&self, field: &'static str) -> &Vec<Value>;
    fn expect_vector2(&self, field: &'static str) -> Vector2;
    fn expect_unit(&self, field: &'static str) -> Unit;
    fn expect_provider(&self, field: &'static str) -> Provider;
    fn expect_list_provider(&self, field: &'static str) -> String;
    fn expect_index_provider(&self, field: &'static str) -> (String, String);
    fn expect_alignment(&self, field: &'static str) -> (VerticalAlignment, HorizontalAlignment);
}
pub trait OptionalValue {
    fn optional_str(&self, field: &'static str, default: &str) -> String;
    fn optional_i32(&self, field: &'static str, default: i32) -> i32;
    fn optional_u32(&self, field: &'static str, default: u32) -> u32;
    fn optional_f32(&self, field: &'static str, default: f32) -> f32;
    fn optional_bool(&self, field: &'static str, default: bool) -> bool;
    fn optional_array<'a>(&'a self, field: &'static str, default: &'a [Value]) -> &'a [Value];
    fn optional_vector2(&self, field: &'static str, default: Vector2) -> Vector2;
    fn optional_unit(&self, field: &'static str, default: Unit) -> Unit;
    fn optional_provider(&self, field: &'static str) -> Option<Provider>;
    fn optional_list_provider(&self, field: &'static str) -> Option<String>;
    fn optional_index_provider(&self, field: &'static str) -> Option<(String, String)>;
    fn optional_alignment(&self, field: &'static str, default_vertical: VerticalAlignment, default_horizontal: HorizontalAlignment) -> (VerticalAlignment, HorizontalAlignment);
}

impl GetValue for Value {
    fn get(&self, field: &'static str) -> Result<&Value, ValidationError> {
        match self {
            Value::Map(map) => {
                if let Some(field) = map.get(&Value::String(field.to_string())) {
                    Ok(field)
                } else {
                    Err(ValidationError::NotFound(field))
                }
            }
            _ => Err(ValidationError::WrongType(field, "a class"))
        }
    }

    fn get_i32(&self, field: &'static str) -> Result<i32, ValidationError> {
        match self.get(field)? {
            Value::Number(number) => Ok(number.get() as i32),
            _ => Err(ValidationError::WrongType(field, "an i32"))
        }
    }

    fn get_u32(&self, field: &'static str) -> Result<u32, ValidationError> {
        match self.get(field)? {
            Value::Number(number) => Ok(number.get() as u32),
            _ => Err(ValidationError::WrongType(field, "a u32"))
        }
    }

    fn get_f32(&self, field: &'static str) -> Result<f32, ValidationError> {
        match self.get(field)? {
            Value::Number(number) => Ok(number.get() as f32),
            _ => Err(ValidationError::WrongType(field, "an f32"))
        }
    }

    fn get_bool(&self, field: &'static str) -> Result<bool, ValidationError> {
        match self.get(field)? {
            Value::Bool(boolean) => Ok(*boolean),
            _ => Err(ValidationError::WrongType(field, "a bool"))
        }
    }

    fn get_str(&self, field: &'static str) -> Result<&str, ValidationError> {
        match self.get(field)? {
            Value::String(string) => Ok(string),
            _ => Err(ValidationError::WrongType(field, "a string"))
        }
    }

    fn get_array(&self, field: &'static str) -> Result<&Vec<Value>, ValidationError> {
        match self.get(field)? {
            Value::Seq(array) => Ok(array),
            _ => Err(ValidationError::WrongType(field, "an array"))
        }
    }

    fn get_vector2(&self, field: &'static str) -> Result<Vector2, ValidationError> {
        match self.get(field)? {
            Value::Seq(array) => {
                if array.len() != 2 {
                    return Err(ValidationError::WrongType(field, "an array with two f32s"));
                }
                let x = match array.get(0).unwrap() {
                    Value::Number(number) => number.get(),
                    _ => return Err(ValidationError::WrongType(field, "an array with two f32s")),
                };
                let y = match array.get(1).unwrap() {
                    Value::Number(number) => number.get(),
                    _ => return Err(ValidationError::WrongType(field, "an array with two f32s")),
                };
                Ok(Vector2::new(x as f32, y as f32))
            }
            _ => Err(ValidationError::WrongType(field, "an array with two f32s"))
        }
    }

    fn get_unit(&self, field: &'static str) -> Result<Unit, ValidationError> {
        match self.get(field)? {
            Value::Number(number) => {
                let number = number.get();
                if number.fract() == 0.0 {
                    Ok(Unit::Units(number as f32))
                } else {
                    Ok(Unit::Percent(number as f32))
                }
            }
            _ => Err(ValidationError::WrongType(field, "either a unit (number) or a percentage (string)"))
        }
    }

    fn get_provider(&self, field: &'static str) -> Result<Provider, ValidationError> {
        match self.get(field)? {
            Value::String(provider_text) => {
                match parse_provider(provider_text) {
                    Some(provider) => Ok(provider),
                    None => Err(ValidationError::InvalidValue(field, "a provider"))
                }
            }
            _ => Err(ValidationError::WrongType(field, "a provider (string)"))
        }
    }

    fn get_list_provider(&self, field: &'static str) -> Result<String, ValidationError> {
        match self.get_provider(field)? {
            Provider::List(provider) => Ok(provider),
            Provider::Index{..} => Err(ValidationError::WrongType(field, "a list provider")),
        }
    }

    fn get_index_provider(&self, field: &'static str) -> Result<(String, String), ValidationError> {
        match self.get_provider(field)? {
            Provider::List(_) => Err(ValidationError::WrongType(field, "a list provider")),
            Provider::Index{name, index} => Ok((name, index))
        }
    }

    fn get_alignment(&self, field: &'static str) -> Result<(VerticalAlignment, HorizontalAlignment), ValidationError> {
        match self.get(field)? {
            Value::String(align_str) => {
                let split_index = match align_str.find('|') {
                    Some(split_index) => split_index,
                    None => return Err(ValidationError::InvalidValue(field, "\"<horizontal origin>|<vertical_origin>\" (string)")),
                };
                let (mut horizontal, mut vertical) = align_str.split_at(split_index);
                vertical = &vertical[1..];
                if horizontal.contains("top")
                    || horizontal.contains("bottom")
                    || vertical.contains("left")
                    || vertical.contains("right")
                {
                    std::mem::swap(&mut horizontal, &mut vertical);
                }
                let horizontal_origin = match horizontal {
                    "left" => HorizontalAlignment::Left,
                    "center" => HorizontalAlignment::Center,
                    "right" => HorizontalAlignment::Right,
                    _ => return Err(ValidationError::InvalidValue("align", "Could not parse horizontal origin; only left, center, or right is allowed")),
                };
                let vertical_origin = match vertical {
                    "top" => VerticalAlignment::Top,
                    "center" => VerticalAlignment::Center,
                    "bottom" => VerticalAlignment::Bottom,
                    _ => return Err(ValidationError::InvalidValue("align", "Could not parse vertical origin; only top, center, or buttom is allowed")),
                };
                Ok((vertical_origin, horizontal_origin))
            }
            _ => Err(ValidationError::WrongType(field, "<vertical alignment>|<horizontal alignment> (string)"))
        }
    }
}

#[allow(clippy::match_wild_err_arm)]
impl ExpectValue for Value {
    fn expect(&self, field: &'static str) -> &Value {
        match self.get(field) {
            Ok(field) => field,
            Err(_) => panic!("Could not find {}", field),
        }
    }

    fn expect_i32(&self, field: &'static str) -> i32 {
        match self.get_i32(field) {
            Ok(field) => field,
            Err(_) => panic!("Could not find {}", field),
        }
    }

    fn expect_u32(&self, field: &'static str) -> u32 {
        match self.get_u32(field) {
            Ok(field) => field,
            Err(_) => panic!("Could not find {}", field),
        }
    }

    fn expect_f32(&self, field: &'static str) -> f32 {
        match self.get_f32(field) {
            Ok(field) => field,
            Err(_) => panic!("Could not find {}", field),
        }
    }

    fn expect_bool(&self, field: &'static str) -> bool {
        match self.get_bool(field) {
            Ok(field) => field,
            Err(_) => panic!("Could not find {}", field),
        }
    }

    fn expect_str(&self, field: &'static str) -> &str {
        match self.get_str(field) {
            Ok(field) => field,
            Err(_) => panic!("Could not find {}", field),
        }
    }

    fn expect_array(&self, field: &'static str) -> &Vec<Value> {
        match self.get_array(field) {
            Ok(field) => field,
            Err(_) => panic!("Could not find {}", field),
        }
    }

    fn expect_vector2(&self, field: &'static str) -> Vector2 {
        match self.get_vector2(field) {
            Ok(field) => field,
            Err(_) => panic!("Could not find {}", field),
        }
    }

    fn expect_unit(&self, field: &'static str) -> Unit {
        match self.get_unit(field) {
            Ok(field) => field,
            Err(_) => panic!("Could not find {}", field),
        }
    }

    fn expect_provider(&self, field: &'static str) -> Provider {
        match self.get_provider(field) {
            Ok(field) => field,
            Err(_) => panic!("Could not find {}", field),
        }
    }

    fn expect_list_provider(&self, field: &'static str) -> String {
        match self.get_list_provider(field) {
            Ok(provider) => provider,
            Err(_) => panic!("Could not find {}", field),
        }
    }

    fn expect_index_provider(&self, field: &'static str) -> (String, String) {
        match self.get_index_provider(field) {
            Ok(pair) => pair,
            Err(_) => panic!("Could not find {}", field),
        }
    }

    fn expect_alignment(&self, field: &'static str) -> (VerticalAlignment, HorizontalAlignment) {
        match self.get_alignment(field) {
            Ok(pair) => pair,
            Err(_) => panic!("Could not find {}", field),
        }
    }
}

impl OptionalValue for Value {
    fn optional_str(&self, field: &'static str, default: &str) -> String {
        match self.get_str(field) {
            Ok(field) => field.to_string(),
            _ => default.to_string(),
        }
    }

    fn optional_i32(&self, field: &'static str, default: i32) -> i32 {
        match self.get_i32(field) {
            Ok(field) => field,
            _ => default,
        }
    }

    fn optional_u32(&self, field: &'static str, default: u32) -> u32 {
        match self.get_u32(field) {
            Ok(field) => field,
            _ => default,
        }
    }

    fn optional_f32(&self, field: &'static str, default: f32) -> f32 {
        match self.get_f32(field) {
            Ok(field) => field,
            _ => default,
        }
    }

    fn optional_bool(&self, field: &'static str, default: bool) -> bool {
        match self.get_bool(field) {
            Ok(field) => field,
            _ => default,
        }
    }

    fn optional_array<'a>(&'a self, field: &'static str, default: &'a [Value]) -> &'a [Value] {
        match self.get_array(field) {
            Ok(field) => field,
            _ => default,
        }
    }

    fn optional_vector2(&self, field: &'static str, default: Vector2) -> Vector2 {
        match self.get_vector2(field) {
            Ok(field) => field,
            _ => default,
        }
    }

    fn optional_unit(&self, field: &'static str, default: Unit) -> Unit {
        match self.get_unit(field) {
            Ok(field) => field,
            _ => default,
        }
    }

    fn optional_provider(&self, field: &'static str) -> Option<Provider> {
        match self.get_provider(field) {
            Ok(provider) => Some(provider),
            _ => None,
        }
    }

    fn optional_list_provider(&self, field: &'static str) -> Option<String> {
        match self.get_list_provider(field) {
            Ok(provider) => Some(provider),
            _ => None,
        }
    }

    fn optional_index_provider(&self, field: &'static str) -> Option<(String, String)> {
        match self.get_index_provider(field) {
            Ok(pair) => Some(pair),
            _ => None,
        }
    }

    fn optional_alignment(&self, field: &'static str, default_vertical: VerticalAlignment, default_horizontal: HorizontalAlignment) -> (VerticalAlignment, HorizontalAlignment) {
        match self.get_alignment(field) {
            Ok(pair) => pair,
            _ => (default_vertical, default_horizontal),
        }
    }
}
