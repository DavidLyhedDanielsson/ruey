#[derive(Hash, PartialEq, Eq, Clone, Copy, Debug)]
pub enum GuiKey {
    UP,
    DOWN,
    LEFT,
    RIGHT,
    ENTER,
    BACKSPACE,
    DELETE,
}
